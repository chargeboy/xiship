<?php
/*require_once('../include.php');
connect();
$sql = "select * from shop_admin";
$totalRows = getResultNum($sql);
// echo $totalRows;
$pageSize = 2;
//得到总页码数
$totalPage = ceil($totalRows/$pageSize);
//当前的页码
$page = $_REQUEST['page']?(int)$_REQUEST['page']:1;

if ($page<1 || $page==null || !is_numeric($page)) {
	$page= 1;
}
if ($page>=$totalPage) $page = $totalPage;

//偏移量(算法)
//取出前5条（第一页）：select * from table limit 0,5
//取出第5到第10条（第二页）：select * from table limit 5,5
$offset = ($page - 1)*$pageSize;

$sql = "select * from shop_admin limit {$offset}, {$pageSize}";
// echo "$sql";
$rows = fetchAll($sql);
// print_r($rows);
foreach ($rows as $row) {
	echo "编号：".$row['id'], "<br />";
	echo "管理员名称：".$row['username'], "<hr />";
}

echo showPage($page, $totalPage, "cid=5");
*/

function showPage($page, $totalPage, $where=null, $sep="&nbsp;") {
//传条件的时候要加 $where
$where = ($where==null)?null:"&".$where;

//全局变量$_SERVER['PHP_SELF']是获取脚本变量的绝对路径
$url = $_SERVER['PHP_SELF'];

//首页、尾页
$index = ($page == 1)?"首页":"<a href='{$url}?page=1{$where}'>首页</a>";
$last = ($page == $totalPage)?"尾页":"<a href='{$url}?page={$totalPage}{$where}'>尾页</a>";

//判断是否超过了
$prevPage = ($page >=1)?$page-1: 1;
$nextPage = ($page >=$totalPage)?$totalPage:$page+1;
//上一页，下一页
$prev = ($page == 1)?"上一页":"<a href='{$url}?page={$prevPage}{$where}'>上一页</a>";
$next = ($page == $totalPage)?"下一页":"<a href='{$url}?page={$nextPage}{$where}'>下一页</a>";

//总共
$str = "当前第{$page}页/总共{$totalPage}页";

/*for ($i = 1; $i <= $totalPage; $i ++) {
	//当前无连接
	if ($page == $i) {
		$p .= "[{$i}]";
	} else {
		$p .= "<a href='{$url}?page={$i}{$where}'> [{$i}]</a>";
	}
}*/

$pageStr = $str.$sep .$index.$sep .$prev.$sep .$next.$sep .$last;
return $pageStr;
}