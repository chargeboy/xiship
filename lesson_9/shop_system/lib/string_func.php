<?php

/**
 * 生成验证码（目前没有用到，没有有这个方法来创建验证码）
 * @param  integer $type   $type是？
 * @param  integer $length $length是验证码的长度
 * @return string          返回这个验证码
 */
function build_random_string($type = 1, $length = 4) {
	if ($type == 1) {
		$chars = join("", range(0, 9));
	} elseif ($type == 2) {
		$chars = join("", array_merge(range("a", "z"), range("A", "Z")));
	} elseif ($type == 3) {
		$chars = join("", array_merge(range("a", "z"), range("A", "Z"), range(0, 9)));
	}
	if ($length > strlen($chars)) {
		exit("字符串输出不够！");
	}
	$chars = str_shuffle($chars);
	return substr($chars, 0, $length);
}

/**
 * 生成唯一字符串
 * @return string
 */
function getUniName(){
	return md5(uniqid(microtime(true),true));
}

/**
 * 得到文件的扩展名
 * @param $filename
 * @return string
 */
function getExt($filename){
	return strtolower(end(explode(".",$filename)));
}