<?php
//实现数据库函数的封装，无非是进行数据的增、删、改、查
 error_reporting(0);
/**
 * 连接数据库的操作
 * 如果不指定这个操作，可以在配置文件configs中定义
 * 设置数据库编码 mysql_query('set names utf8');
 * @return [数据库连接结果的标识符]
 */
function connect() {
	$conn = mysql_connect("localhost", "root", "localhost") or die("数据库连接失败Erorr:".mysql_errno().":".mysql_error());
//	mysql_set_charset('utf-8');
//	mysql_query("set names 'utf8'");
	mysql_query('SET NAMES UTF8');
	mysql_select_db('shop_system') or die("指定数据库打开失败！");
	return $conn;
}

/**
 * 完成记录的插入操作
 * @param  [type] $table [description]
 * @param  [type] $array 插入的数组
 * @return [type]        [description]
 */
/*function insert($table, $array) {
	$keys = join(",", array_keys($array));
	$vals = "'".join(",", array_values($array))."'";
	$sql = "insert into {$table}($keys) values({$vals})";
	mysql_query($sql);
	// var_dump($sql);
	return mysql_insert_id();
}*/

//以后就用这个插入语句
function insert($table, $arr){
	//键名 对应 键值
	foreach ($arr as $key => $value) {
		$value = mysql_real_escape_string($value);
		$keyArr[] = "`".$key."`";//把数组$arr中的键名保存到$keyArr数组当中
		$valueArr[] = "'".$value."'";//把数组当中的键值保存到$valueArr当中
	}
		$keys = implode(',', $keyArr);
		//implode分割数组成字符串
		$values = implode(',', $valueArr);
		$sql = "insert into " .$table. "(" .$keys. ") values(" .$values. ")";
		mysql_query($sql);
		// var_dump($sql);
		return mysql_insert_id();
}

/**
 * 完成记录的更新操作。记住这个操作的步骤。
 * 更新语句 update shop_admin set username='panchaozhi' where id=1;去配成这个语句
 * @param  [type] $table [description]
 * @param  [type] $array [description]
 * @param  [type] $where [description]
 * @return [type]        [description]
 */
function update($table, $array, $where=null) {
	foreach ($array as $key => $value) {
		$value = mysql_real_escape_string($value);
		$key_value_arr[] = "`".$key."` = '".$value."'";
	}
	$key_values = implode(',', $key_value_arr);
	$sql = "update ".$table." set ".$key_values." where ".$where;
	mysql_query($sql);
	return mysql_affected_rows();
}

/**
 * 删除记录的操作
 * @param  [type] $table [description]
 * @param  [type] $where [description]
 * @return [type]        [description]
 */
function delete($table, $where=null) {
	$where = $where==null?null:" where ".$where;
	$sql = "delete from {$table} {$where}";
	mysql_query($sql);
	return mysql_affected_rows();
}

/**
 * 得到一条指定的记录
 * @param  [type] $sql         [description]
 * @param  [type] $result_type [description]
 * @return [type]              [description]
 */
function fetchOne($sql, $result_type=MYSQL_ASSOC) {
	$result = mysql_query($sql);
	$row = mysql_fetch_array($result, $result_type);
	return $row;
}

/**
 * 得到结果集中的所有记录
 * @param  string $sql         [description]
 * @param  string $result_type [description]
 * @return multitype              [description]
 */
function fetchAll($sql, $result_type = MYSQL_ASSOC) {
//	$conn = connect();
	$result = mysql_query($sql);
	$rows = array();
	while ($row = mysql_fetch_array($result, $result_type)){
		$rows[] = $row;
  }
	return $rows;
}
/*function fetchAll($sql){
	//mysql_fetch_array()函数把资源转换成数组，一次转换出一行出来
	$rows = array();
	while($row = mysql_fetch_array($sql, MYSQL_ASSOC)){
		$rows[]=$row;
	}
	return isset($rows)?$rows:"";
}*/

/**
 * 得到结果集中的记录条数
 * @param  [type] $sql [description]
 * @return [type]      [description]
 */
function getResultNum($sql) {
	$result = mysql_query($sql);
	return mysql_num_rows($result);
}

/**
 * 得到上一步插入记录的Id号
 * @return number [description]
 */
function getInsertId() {
	connect();
	return mysql_insert_id();
}

/**
 * 根据cid得到4条产品的信息
 * @param $cid
 * @return array
 */
function getProsByCid($cid) {
	connect();
	$sql = "select p.id,p.pName,p.pSn,p.pNum,p.mPrice,p.iPrice,p.pDes,p.pubTime,p.isShow,p.isHot,c.cName,p.cId from shop_pro as p join shop_cate c on p.cId=c.id where p.cId={$cid} limit 4";
	$rows = fetchAll($sql);
	return $rows;
}

/**
 * 得到下4条书籍的记录，只不过是将图片缩小了而已，而不是大图分类下的小图片
 * @param $cid
 * @return array
 */
function getSmallProsBycId($cid) {
	connect();
	$sql = "select p.id,p.pName,p.pSn,p.pNum,p.mPrice,p.iPrice,p.pDes,p.pubTime,p.isShow,p.isHot,c.cName,p.cId from shop_pro as p join shop_cate c on p.cId=c.id where p.cId={$cid} limit 4,4";
	$rows = fetchAll($sql);
	return $rows;
}