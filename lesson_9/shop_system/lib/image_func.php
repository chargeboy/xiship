<?php
require_once('string_func.php');
//session_start();
/**
 * 通过GD库做图片验证码
 */
function verify_image(){


	$image = imagecreatetruecolor(80, 30);//新建一个真彩色图像
	$bgcolor = imagecolorallocate($image, 255, 255, 255);//为图像分配颜色
	imagefill($image, 0, 0, $bgcolor);//为图像填充颜色

	/*for ($i=0; $i < 4; $i++) {
        $fontsize = 6;
        $fontcolor = imagecolorallocate($image, rand(0,120), rand(0,120), rand(0,120));
        $fontcontent = rand(0,9);
        $x = ($i*100/5 + rand(5,10));
        $y = rand(5,10);
        imagestring($image, $fontsize, $x, $y, $fontcontent, $fontcolor);//数字验证码的位置
    }*/
	$captcha_code = "";


	for ($i=0; $i < 4; $i++) {
		$fontsize = 6;
		$fontcolor = imagecolorallocate($image, rand(0,120), rand(0,120), rand(0,120));
		$data = 'abcdefghijkmnpqrstuvwxyz23456789';//ABCDEFGHIJKLMNOPQRSTUVWXYZ
		$fontcontent = substr($data, rand(0,strlen($data)),1);//4次，所以取一个就行
		$captcha_code .= $fontcontent;
		//substr — 返回字符串的子串 string substr  ( string $string  , int $start  [, int $length  ] )
		//rand — 产生一个随机整数
		$x = ($i*100/5 + rand(5,10));
		$y = rand(5,10);
		imagestring($image, $fontsize, $x, $y, $fontcontent, $fontcolor);//数字加字母验证码的位置
	}

	$sess_name = 'verify';
	$_SESSION[$sess_name] = $captcha_code;



	for ($i=0; $i < 200; $i++) {
		$pointcolor = imagecolorallocate($image, rand(50,200), rand(50,200), rand(50,200));
		imagesetpixel($image, rand(1,79), rand(1,29), $pointcolor);//点的干扰
	}

	for ($i=0; $i < 3; $i++) {
		$linecolor = imagecolorallocate($image, rand(50,99), rand(50,99), rand(50,99));
		imageline($image, rand(1,79), rand(1,29), rand(1,79), rand(1,29), $linecolor);//线的干扰
	}
	header('Content-type:image/png');
	imagepng($image);//以 PNG 格式将图像输出到浏览器或文件
	imagedestroy($image);
}

/**
 * 生成缩略图
 * @param string $filename
 * @param string $destination
 * @param int $dst_w
 * @param int $dst_h
 * @param bool $isReservedSource
 * @param float $scale
 * @return string
 */
/*function thumb($filename,$destination=null,$dst_w=null,$dst_h=null,$isReservedSource=true,$scale=0.5){
	//list — 把数组中的值赋给一些变量,这里是列出所有变量
	//getimagesize — 取得图像大小
	list($src_w,$src_h,$imagetype)=getimagesize($filename);
	//检测图像的宽和高是否为空
	if(is_null($dst_w)||is_null($dst_h)){
		//得到目标的图像，其宽和高是原来图像的$scale倍
		$dst_w=ceil($src_w*$scale);
		$dst_h=ceil($src_h*$scale);
	}
	//image_type_to_mime_type — 取得 getimagesize
	$mime=image_type_to_mime_type($imagetype);

	//将imagecreatefromjpeg中的 / 替换为createfrom，然后图像类型就是image/jpeg，即$mime的值
	$createFun=str_replace("/", "createfrom", $mime);

	//有时候有imagejpeg()函数这样子的，将 / 替换成 null
	$outFun=str_replace("/", null, $mime);

	//创建一个画布
	$src_image=$createFun($filename);

	//根据目标图像的宽的高，创建一个画布, imagecreatetruecolor — 新建一个真彩色图像
	$dst_image=imagecreatetruecolor($dst_w, $dst_h);

	//imagecopyresampled — 重采样拷贝部分图像并调整大小
	///$dst_image 目标图像, $src_image 原图像，目标x y坐标，原图像x y坐标.......
	imagecopyresampled($dst_image, $src_image, 0,0,0,0, $dst_w, $dst_h, $src_w, $src_h);

	//传过来的$destination有值，但是这个目录不存在的话，创建它
	if($destination&&!file_exists(dirname($destination))){
		mkdir(dirname($destination),0777,true);
	}
	//目标文件取唯一文件名
	$dstFilename=$destination==null?get_unique_name().".".get_extend($filename):$destination;
	//生成的目标图像保存在磁盘中，文件名是$datFilename
	//即输出图像，$dstfFilename是可选参数
	$outFun($dst_image,$dstFilename);
	imagedestroy($src_image);
	imagedestroy($dst_image);
	//是否保存原图像
	if(!$isReservedSource){
		unlink($filename);
	}
	//返回目标的文件名
	return $dstFilename;
}*/
function thumb($filename,$destination=null,$dst_w=null,$dst_h=null,$isReservedSource=true,$scale=0.5){
	list($src_w,$src_h,$imagetype)=getimagesize($filename);
	if(is_null($dst_w)||is_null($dst_h)){
		$dst_w=ceil($src_w*$scale);
		$dst_h=ceil($src_h*$scale);
	}
	$mime=image_type_to_mime_type($imagetype);
	$createFun=str_replace("/", "createfrom", $mime);
	$outFun=str_replace("/", null, $mime);
	$src_image=$createFun($filename);
	$dst_image=imagecreatetruecolor($dst_w, $dst_h);
	imagecopyresampled($dst_image, $src_image, 0,0,0,0, $dst_w, $dst_h, $src_w, $src_h);
	if($destination&&!file_exists(dirname($destination))){
		mkdir(dirname($destination),0777,true);
	}
	$dstFilename=$destination==null?getUniName().".".getExt($filename):$destination;
	$outFun($dst_image,$dstFilename);
	imagedestroy($src_image);
	imagedestroy($dst_image);
	if(!$isReservedSource){
		unlink($filename);
	}
	return $dstFilename;
}
