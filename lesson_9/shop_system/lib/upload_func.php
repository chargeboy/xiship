<?php
require_once('string_func.php');
//文件上传的函数

/**
 * 构造上传文件的信息
 * @return array
 */
function buildInfo(){
    if(!$_FILES){
        return ;
    }
    $i=0;$files = array();
    foreach($_FILES as $v){
        //单文件，直接循环输出
        if(is_string($v['name'])){
            $files[$i]=$v;
            $i++;
        }else{
            //多文件，转换成二维数组
            foreach($v['name'] as $key=>$val){
                $files[$i]['name']=$val;
                $files[$i]['size']=$v['size'][$key];
                $files[$i]['tmp_name']=$v['tmp_name'][$key];
                $files[$i]['error']=$v['error'][$key];
                $files[$i]['type']=$v['type'][$key];
                $i++;
            }
        }
    }
    return $files;
}

/**
 * 文件上传函数的封装
 * @param string $path是上传文件要保存的路径
 * @param array $allowExt
 * @param string $path
 * @param int $maxSize
 * @param bool $imgFlag
 * @return array() 返回的是文件上传信息的数组
 */
/*function uploadFile($path="uploads",$allowExt=array("gif","jpeg","png","jpg","wbmp"),$maxSize=2097152,$imgFlag=true){
    //如果这个路径不存在，创建它
    if(!file_exists($path)){
        mkdir($path,0777,true);
    }
    //初始化
    $i=0;$uploadedFiles = array();
    //获取文件上传的信息
    $files=buildInfo();
    //如果上传的文件没有信息或不是一个数组
    if(!($files&&is_array($files))){
        return ;
    }
    //把文件上传信息遍历出来
    foreach($files as $file){
        //文件上传成功
        if($file['error']===UPLOAD_ERR_OK){
            $ext=get_extend($file['name']);
            //检测文件的扩展名
            if(!in_array($ext,$allowExt)){
                exit("非法文件类型");
            }
            //校验是否是一个真正的图片类型
            if($imgFlag){
                if(!getimagesize($file['tmp_name'])){
                    exit("不是真正的图片类型");
                }
            }
            //检测上传文件的大小
            if($file['size']>$maxSize){
                exit("上传文件过大");
            }
            //检测是否是通过Post方式上传的
            if(!is_uploaded_file($file['tmp_name'])){
                exit("不是通过HTTP POST方式上传上来的");
            }
            //文件名要唯一，$filename是加密过的
            $filename=get_unique_name().".".$ext;
            //要保存的路径，如 uploads/php_big.jpg，不过php_big是加密过的
            $destination=$path."/".$filename;
            //将在服务端的文件名移到到$destination这个路径
            if(move_uploaded_file($file['name'], $destination)){
                //文件名
                $file['name']=$filename;
                //清除一些上传文件的信息
                unset($file['tmp_name'],$file['size'],$file['type']);
                //将这个数组的内容循环出来，从0开始
                $uploadedFiles[$i]=$file;
                $i++;
            }
        }else{
            //文件上传不成功
            switch($file['error']){
                case 1:
                    $mes="超过了配置文件上传文件的大小";//UPLOAD_ERR_INI_SIZE
                    break;
                case 2:
                    $mes="超过了表单设置上传文件的大小";			//UPLOAD_ERR_FORM_SIZE
                    break;
                case 3:
                    $mes="文件部分被上传";//UPLOAD_ERR_PARTIAL
                    break;
                case 4:
                    $mes="没有文件被上传1111";//UPLOAD_ERR_NO_FILE
                    break;
                case 6:
                    $mes="没有找到临时目录";//UPLOAD_ERR_NO_TMP_DIR
                    break;
                case 7:
                    $mes="文件不可写";//UPLOAD_ERR_CANT_WRITE;
                    break;
                case 8:
                    $mes="由于PHP的扩展程序中断了文件上传";//UPLOAD_ERR_EXTENSION
                    break;
            }
            echo $mes;
        }
    }
    return $uploadedFiles;
}*/
function uploadFile($path="uploads",$allowExt=array("gif","jpeg","png","jpg","wbmp"),$maxSize=2097152,$imgFlag=true){
    //如果这个路径不存在，创建它
    if(!file_exists($path)){
        mkdir($path,0777,true);
    }
    //初始化
    $i=0;$uploadedFiles = array();
    //获取文件上传的信息
    $files=buildInfo();
    //如果上传的文件没有信息或不是一个数组
    if(!($files&&is_array($files))){
        return ;
    }
    //把文件上传信息遍历出来
    foreach($files as $file){
        //文件上传成功
        if($file['error']===UPLOAD_ERR_OK){
            $ext=getExt($file['name']);
            //检测文件的扩展名
            if(!in_array($ext,$allowExt)){
                exit("非法文件类型");
            }
            //校验是否是一个真正的图片类型
            if($imgFlag){
                if(!getimagesize($file['tmp_name'])){
                    exit("不是真正的图片类型");
                }
            }
            //检测上传文件的大小
            if($file['size']>$maxSize){
                exit("上传文件过大");
            }
            //检测是否是通过Post方式上传的
            if(!is_uploaded_file($file['tmp_name'])){
                exit("不是通过HTTP POST方式上传上来的");
            }
            //文件名要唯一，$filename是加密过的
            $filename=getUniName().".".$ext;
            //要保存的路径，如 uploads/php_big.jpg，不过php_big是加密过的
            $destination=$path."/".$filename;
            //将在服务端的文件名移到到$destination这个路径
            if(move_uploaded_file($file['name'], $destination)){
                //文件名
                $file['name']=$filename;
                //清除一些上传文件的信息
                unset($file['tmp_name'],$file['size'],$file['type']);
                //将这个数组的内容循环出来，从0开始
                $uploadedFiles[$i]=$file;
                $i++;
            }
        }else{
            //文件上传不成功
            switch($file['error']){
                case 1:
                    $mes="超过了配置文件上传文件的大小";//UPLOAD_ERR_INI_SIZE
                    break;
                case 2:
                    $mes="超过了表单设置上传文件的大小";			//UPLOAD_ERR_FORM_SIZE
                    break;
                case 3:
                    $mes="文件部分被上传";//UPLOAD_ERR_PARTIAL
                    break;
                case 4:
                    $mes="没有文件被上传1111";//UPLOAD_ERR_NO_FILE
                    break;
                case 6:
                    $mes="没有找到临时目录";//UPLOAD_ERR_NO_TMP_DIR
                    break;
                case 7:
                    $mes="文件不可写";//UPLOAD_ERR_CANT_WRITE;
                    break;
                case 8:
                    $mes="由于PHP的扩展程序中断了文件上传";//UPLOAD_ERR_EXTENSION
                    break;
            }
            echo $mes;
        }
    }
    return $uploadedFiles;
}