<?php
require_once('lib/mysql_func.php');
session_start();
function getAllCate() {
  connect();
  $sql = "select id, cName from shop_cate";
  $rows = fetchAll($sql);
  return $rows;
}
$cates = getAllCate();

if (!($cates && is_array($cates))) {
  alertMese("网站维护中...", "http://www.baidu.com");
}
?>

<!doctype html>
<!--这是前台显示的界面-->
<html>
<head>
  <meta charset="utf-8">
  <title>首页</title>
  <link type="text/css" rel="stylesheet" href="styles/reset.css">
  <link type="text/css" rel="stylesheet" href="styles/main.css">
  <!--[if IE 6]>
  <script type="text/javascript" src="js/DD_belatedPNG_0.0.8a-min.js"></script>
  <script type="text/javascript" src="js/ie6Fixpng.js"></script>
  <![endif]-->
</head>
<body>
<div class="headerBar">
  <div class="topBar">
    <div class="comWidth">
      <div class="leftArea">
        <a href="#" class="collection">收藏本站</a>
      </div>
      <div class="rightArea">
        欢迎来到我们的书籍系统！
        <?php if($_SESSION['loginFlag']):?>
          <span>欢迎您</span><?php echo $_SESSION['username'];?>
          <a href="doAction.php?act=userOut">[退出]</a>
        <?php else:?>
          <a href="login.php">[登录]</a><a href="register.php">[免费注册]</a>
        <?php endif;?>
      </div>
    </div>
  </div>
  <div class="logoBar">
    <div class="comWidth">
      <div class="logo fl">
        <a href="#"><img src="images/logo.jpg" alt="php&zhi"></a>
      </div>
      <div class="search_box fl">
        <input type="text" class="search_text fl">
        <input type="button" value="搜 索" class="search_btn fr">
      </div>
      <div class="shopCar fr">
        <span class="shopText fl">购物车</span>
        <span class="shopNum fl">0</span>
      </div>
    </div>
  </div>
  <div class="navBox">
    <div class="comWidth clearfix">
      <div class="shopClass fl">
        <h3>全部书籍分类<i class="shopClass_icon"></i></h3>
        <div class="shopClass_show">
          <dl class="shopClass_item">
            <dt><a href="#" class="b">PHP书籍</a> <a href="#" class="b">PHP基础入门</a> </dt>
<!--            class="aLink"-->
            <dd><a href="#">简介</a> <a href="#">价格</a> <a href="#">编号</a> <a href="#">实战</a></dd>
          </dl>
          <dl class="shopClass_item">
            <dt><a href="#" class="b">Java书籍</a> <a href="#" class="b">Java实例教程</a> </dt>
            <dd><a href="#">简介</a> <a href="#">价格</a> <a href="#">编号</a> <a href="#">实战</a></dd>
          </dl>
          <dl class="shopClass_item">
            <dt><a href="#" class="b">MySQL书籍</a> <a href="#" class="b">数据库分类</a> </dt>
            <dd><a href="#">简介</a> <a href="#">价格</a> <a href="#">编号</a> <a href="#">其他数据库</a></dd>
          </dl>
          <dl class="shopClass_item">
            <dt><a href="#" class="b">Web书籍</a> <a href="#" class="b">Web项目开发</a> </dt>
            <dd><a href="#">简介</a> <a href="#">价格</a> <a href="#">编号</a> <a href="#">Web走向</a></dd>
          </dl>
          <dl class="shopClass_item">
            <dt><a href="#" class="b">C++书籍</a> <a href="#" class="b">C++游戏开发</a> </dt>
            <dd><a href="#">简介</a> <a href="#">价格</a> <a href="#">编号</a> <a href="#">体验</a></dd>
          </dl>
        </div>
        <div class="shopClass_list hide">
          <div class="shopClass_cont">
            <dl class="shopList_item">
              <dt>111111111</dt>
              <dd>
                <a href="#">xxxxx</a><a href="#">xxxxxxxxxx</a><a href="#">xxxxx字xxxxx</a><a href="#">xxxxx</a><a href="#">xxxxx</a><a href="#">xxxxx</a>
              </dd>
            </dl>
            <dl class="shopList_item">
              <dt>xxxxxxxxxxxxx</dt>
              <dd>
                <a href="#">xxxxx</a><a href="#">xxxxxxxxxx</a><a href="#">xxxxx字xxxxx</a><a href="#">xxxxx</a><a href="#">xxxxx</a><a href="#">xxxxx</a><a href="#">xxxxx</a><a href="#">xxxxxxxxxx</a><a href="#">xxxxx字xxxxx</a><a href="#">xxxxx</a><a href="#">xxxxx</a><a href="#">xxxxx</a><a href="#">xxxxx</a>
              </dd>
            </dl>
            <dl class="shopList_item">
              <dt>ttttttttttttt</dt>
              <dd>
                <a href="#">xxxxx</a><a href="#">xxxxxxxxxx</a><a href="#">xxxxx字xxxxx</a><a href="#">xxxxx</a><a href="#">xxxxx</a><a href="#">xxxxx</a><a href="#">xxxxx</a><a href="#">xxxxxxxxxx</a><a href="#">xxxxx字xxxxx</a><a href="#">xxxxx</a><a href="#">xxxxx</a><a href="#">xxxxx</a><a href="#">xxxxx</a>
              </dd>
            </dl>
            <dl class="shopList_item">
              <dt>ooooooooooo</dt>
              <dd>
                <a href="#">xxxxx</a><a href="#">xxxxxxxxxx</a><a href="#">xxxxx字xxxxx</a><a href="#">xxxxx</a><a href="#">xxxxx</a><a href="#">xxxxx</a><a href="#">xxxxx</a><a href="#">xxxxxxxxxx</a><a href="#">xxxxx字xxxxx</a><a href="#">xxxxx</a><a href="#">xxxxx</a><a href="#">xxxxx</a><a href="#">xxxxx</a>
              </dd>
            </dl>
            <dl class="shopList_item">
              <dt>xxxxxxxxxxxxx</dt>
              <dd>
                <a href="#">xxxxx</a><a href="#">xxxxxxxxxx</a><a href="#">xxxxx字xxxxx</a><a href="#">xxxxx</a><a href="#">xxxxx</a><a href="#">xxxxx</a><a href="#">xxxxx</a><a href="#">xxxxxxxxxx</a><a href="#">xxxxx字xxxxx</a><a href="#">xxxxx</a><a href="#">xxxxx</a><a href="#">xxxxx</a><a href="#">xxxxx</a>
              </dd>
            </dl>
            <div class="shopList_links">
              <a href="#">xxxxxxxxxxxxxxx<span></span></a><a href="#">xxxxx容等等<span></span></a>
            </div>
          </div>
        </div>
      </div>
      <ul class="nav fl">
        <li><a href="#" class="active">系统首页</a></li>
        <li><a href="#">系统简介</a></li>
        <li><a href="#">系统中心</a></li>
        <li><a href="#">系统导航</a></li>
        <li><a href="#">问题反馈</a></li>
        <li><a href="#">联系我们</a></li>
      </ul>
    </div>
  </div>
</div>
<div class="banner comWidth clearfix">
  <div class="banner_bar banner_big">
    <ul class="imgBox">
      <li><a href="#"><img src="images/banner/banner_01.jpg" alt="banner"></a></li>
      <li><a href="#"><img src="images/banner/banner_02.jpg" alt="banner"></a></li>
    </ul>
    <div class="imgNum">
      <a href="#" class="active"></a><a href="#"></a><a href="#"></a><a href="#"></a>
    </div>
  </div>
</div>

<!--分类列表的循环-->
<?php foreach ($cates as $cate):?>
  <div class="shopTit comWidth">
    <span class="icon"></span><h3><?php echo $cate['cName'];?></h3>
    <a href="#" class="more">更多&gt;&gt;</a>
  </div>

  <!--书籍列表的实现-->
  <div class="shopList comWidth clearfix">
    <div class="leftArea">
      <!--左边的版块和图片循环-->
      <div class="banner_bar banner_sm">
        <ul class="imgBox">
          <li><a href="#"><img src="images/banner/banner_sm_01.jpg" alt="banner"></a></li>
          <li><a href="#"><img src="images/banner/banner_sm_02.jpg" alt="banner"></a></li>
        </ul>
        <!--左版块的图片-->
        <div class="imgNum">
          <a href="#" class="active"></a><a href="#"></a><a href="#"></a><a href="#"></a>
        </div>
      </div>
    </div>
    <!--    书籍信息的版块-->
    <div class="rightArea">
      <div class="shopList_top clearfix">
        <!--        根据书籍分类得到书籍的信息-->
        <!--            循环遍历输出书籍信息-->
        <?php
        $pros = getProsByCid($cate['id']);
        if ($pros && is_array($pros)):
          foreach ($pros as $pro):
            ?>
            <div class="shop_item">
              <div class="shop_img">
                <a href="proDetails.php" target="_blank"><img height="200" width="187" src="" alt=""></a>
              </div>
              <h6><?php echo $pro['pName'];?></h6>
              <p>￥<?php echo $pro['iPrice'];?>元</p>
            </div>
            <?php
          endforeach;
        endif;
        ?>
      </div>
      <!--      小图片下的图片文字循环-->
      <div class="shopList_sm clearfix">
        <?php
        $prosSmall = getSmallProsBycId($cate['id']);
        if ($prosSmall && is_array($prosSmall)):
          foreach ($prosSmall as $proSmall):
            ?>
            <div class="shopItem_sm">
              <div class="shopItem_smImg">
                <a href="proDetails.php" target="_blank"><img width="95" height="95" src="" alt=""></a>
              </div>
              <div class="shopItem_text">
                <p><?php echo $proSmall['pName'];?></p>
                <h3>￥<?php echo $proSmall['iPrice'];?>元</h3>
              </div>
            </div>
            <?php
          endforeach;
        endif;
        ?>
      </div>
    </div>
  </div>
    <?php
  endforeach;
  ?>
<div class="hr_25"></div>
<div class="footer">
  <p><a href="#">网站简介</a><i>|</i><a href="#">网站公告</a><i>|</i> <a href="#">招纳贤士</a><i>|</i><a href="#">联系我们</a><i>|</i>客服热线：18269179369</p>
  <p>Copyright &copy; 2015 - 2016 ChaoZhi版权所有&nbsp;&nbsp;&nbsp;桂ICP备xxxxxxx号&nbsp;&nbsp;&nbsp;桂ICP证xxxxxxxxx号&nbsp;&nbsp;&nbsp;</p>
  <!-- <p class="web"><a href="#"><img src="images/webLogo.jpg" alt="logo"></a><a href="#"><img src="images/webLogo.jpg" alt="logo"></a><a href="#"><img src="images/webLogo.jpg" alt="logo"></a><a href="#"><img src="images/webLogo.jpg" alt="logo"></a></p> -->
</body>
</html>