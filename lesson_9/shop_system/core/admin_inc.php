<?php
require_once('../include.php');
session_start();
/**
 * 检查管理员是否存在
 * @param $sql
 * @return arra
 */
    function checkAdmin($sql) {
        return fetchOne($sql);
    }

/**
 * 检查是否有管理员登录
 */
    function checkLogined() {
        if ($_SESSION['adminId'] == "" && $_COOKIE['adminId'] == "") {
            alertMese('请使用管理账号登录！', 'login.php');
        }
    }

/**
 * 添加管理员
 * @return string
 */
    function addAdmin() {
        connect();//记得要连接数据库
        $arr = $_POST;
        $arr['password'] = md5($_POST['password']);
        if (insert("shop_admin", $arr)) {
            $mes = "添加成功！<br /><a href='addAdmin.php'>继续添加</a>|<a href='listAdmin.php'>查看管理员列表</a> ";
        } else {
            $mes = "添加失败！<br /><a href='addAdmin.php'>重新添加</a> ";
        }
        return $mes;
    }

/**
 * 编辑管理员的操作
 * @param  number $id 管理员的id
 * @return string     操作成功的信息
 */
    function editAdmin($id) {
        connect();
        $arr = $_POST;
        $arr['password'] = md5($_POST['password']);
        if (update("shop_admin", $arr, "id = {$id}")) {
            $mes = "编辑成功！<br /><a href='listAdmin.php'>查看管理员列表</a>";
        } else {
            $mes = "编辑失败！<br /><a href='editAdmin.php'>返回继续编辑</a>";
        }
        return $mes;
    }

/**
 * 删除管理员用户的操作
 * @param  [type] $id [description]
 * @return [type]     [description]
 */
    function delAdmin($id) {
        connect();
        if (delete("shop_admin", "id = {$id}")) {
            $mes = "删除成功！<br /><a href='listAdmin.php'>返回管理员列表</a>";
        } else {
            $mes = "删除失败！<br /><a href='listAdmin.php'>重新删除</a>";
        }
        return $mes;
    }

/**
 * 等到所有管理员的操作
 * @return [type] [description]
 */
    function getAllAdmin() {
        connect();
        $sql = "select id, username, email from shop_admin";
        $rows = fetchAll($sql);
        return $rows;

    }

/**
 * 管理员退出
 */
    function logout() {
        $_SESSION = array();
        if (isset($_COOKIE[session_name()])) {
            setcookie(session_name(), "", time()-1);
        }
        session_destroy();
        alertMese('退出成功！', 'login.php');
    }
?>