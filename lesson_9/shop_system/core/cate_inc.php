<?php
require_once('../include.php');

/**
 * 添加分类的操作
 * @return string
 */
function addCate() {
    connect();
    $arr = $_POST;
    if (insert("shop_cate", $arr)) {
        $mes = "添加成功！<br /><a href='addCate.php'>继续添加</a> | <a href='listCate.php'>查看分类列表</a>";
    } else {
        $mes = "添加失败！<br /><a href='addCate.php'>重新添加</a> | <a href='listCate.php'>查看分类列表</a>";
    }
    return $mes;
}

/**
 * 修改分类的操作
 * @param $id
 * @return string
 */
function editCate($id) {
    connect();
    $arr = $_POST;
    if (update("shop_cate", $arr, "id={$id}")) {
        $mes = "修改分类成功！<br /><a href='listCate.php'>查看分类列表</a>";
    } else {
        $mes = "修改分类失败！<br /><a href='listCate.php'>返回分类列表继续修改</a>";
    }
    return $mes;
}

/**
 * 删除分类
 * 注意级联的操作，删除的时候要考虑完整性，以后做修改
 * @param $id
 * @return string
 */
function delCate($id) {
    connect();
    if (delete("shop_cate", "id={$id}")) {
        $mes = "删除成功！<br /><a href='listCate.php'>返回分类列表</a> ";
    } else {
        $mes = "删除失败！<br /><a href='listCate.php'>重新删除</a> ";
    }
    return $mes;
}

/**
 * 得到所有的分类
 * @return multitype
 */
function getAllCate() {
    connect();
    $sql = "select id, cName from shop_cate";
    $rows = fetchAll($sql);
    return $rows;
}