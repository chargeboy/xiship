<?php
require_once('../include.php');

/**
 * 添加商品函数
 */
function addPro(){
  connect();
  $arr=$_POST;
	$arr['pubTime']=time();
  $path="./uploads";
  //获取上传文件的信息，得到一个数组
	$uploadFiles=uploadFile($path);
  //判断存在这个数组和数组有值
	if(is_array($uploadFiles)&&$uploadFiles){
    //将其遍历出来，对其进行压缩，产生缩略图
    foreach($uploadFiles as $key=>$uploadFile){
      //$path."/".$uploadFile['name']是原文件路径和文件名（也就是上传的图像?把它保存在$path下？文件名就是上传的文件名？)
      //../image_50/".$uploadFile['name']是目标文件的路径+文件名，文件名是加密过的，唯一的
      //50,50是宽和高
//      thumb($path."/".$uploadFile['name'],"../image_50/".$uploadFile['name'],10,10);
      thumb($path."/".$uploadFile['name'],"../image_100/".$uploadFile['name'],30,30);
//      thumb($path."/".$uploadFile['name'],"../image_350/".$uploadFile['name'],50,50);
//      thumb($path."/".$uploadFile['name'],"../image_500/".$uploadFile['name'],100,100);
    }
  }
  //将数组插入数据库中的结果
	$res=insert("shop_pro",$arr);
//  $rows = fetchAll($res);
//  print_r($rows);
  //得到上一步插入记录的id
	$pid=getInsertId();
	if($res&&$pid){
    foreach($uploadFiles as $key => $uploadFile){
      //表shop_album里的pid 和 alubmPath
      //pid=shop_pro里的id,albumPath=加密后的文件的文件名
      $arr1['pid']=$pid;
      $arr1['albumPath']=$uploadFile['name'];
      //将这个数组插入到表shop_album中，执行这个添加操作
      addAlbum($arr1);
    }
    $mes="<p>添加成功!</p><a href='addPro.php' target='mainFrame'>继续添加</a>|<a href='listPro.php' target='mainFrame'>查看书籍列表</a>";
  }else{
    //添加不成功则将座上传产生的文件删掉
    foreach($uploadFiles as $key => $uploadFile){
      /*if(file_exists("../image_10/".$uploadFile['name'])){
        unlink("../image_10/".$uploadFile['name']);
      }*/
      if(file_exists("../image_30/".$uploadFile['name'])){
        unlink("../image_30/".$uploadFile['name']);
      }
      /*if(file_exists("../image_50/".$uploadFile['name'])){
        unlink("../image_50/".$uploadFile['name']);
      }*/
      /*if(file_exists("../image_100/".$uploadFile['name'])){
        unlink("../image_100/".$uploadFile['name']);
      }*/
    }
    $mes="<p>添加失败!</p><a href='addPro.php' target='mainFrame'>返回重新添加</a>";

  }
	return $mes;
}

/**
 * 暂时不做。图片功能没有实现
 * @param $id
 * @return string
 */
function editPro($id) {
  $arr = $_POST;
  if (update("shop_pro", $arr, "id = {$id}")) {
    $mes = "<p>修改成功！</p><a href='listPro.php'>查看书籍列表</a>";
  } else {
    $mes = "<p>修改失败！</p><a href='listPro.php' target='mainFrame'>返回重新修改</a>";
  }
  return $mes;
}

/**
 * 这里涉及到两个表的删除问题，也是在图片成功上传的情况下执行的，目前图片未上传成功，无法执行此操作
 * @param $id
 */
function delPro($id) {
  if (delete("shop_pro", "id = {$id}")) {

  };

}

/**
 * 根据id得到商品的详细信息
 * @param $id
 * @return array
 */
function getProById($id) {
  connect();
  $sql = "select p.id,p.pName,p.pSn,p.mPrice,p.pNum,p.iPrice,p.pDes,p.pubTime,p.isShow,p.isHot,p.cId,c.cName from shop_pro as p join shop_cate c on p.cId=c.id where p.id={$id}";
  $row = fetchOne($sql);
  return $row;
}