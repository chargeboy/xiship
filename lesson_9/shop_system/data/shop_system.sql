CREATE DATABASE shop_system DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE shop_system;


CREATE TABLE shop_admin(
id tinyint unsigned auto_increment primary key,
username varchar(20) not null unique,
password char(32) not null,
email varchar(50) not null
);


CREATE TABLE shop_cate(
id smallint unsigned auto_increment primary key,
cName varchar(50) unique
);


CREATE TABLE shop_pro(
id int unsigned auto_increment primary key,
pName varchar(50) not null unique,
pSn varchar(50) not null,
pNum int unsigned default 1,
mPrice decimal(10, 2) not null,
iPrice decimal(10, 2) not null,
pDes text,
pImg varchar(50) not null,
pubTime int unsigned not null,
isShow tinyint(1) default 1,
isHot tinyint(1) default 0,
cId smallint unsigned not null
);


CREATE TABLE shop_user(
id int unsigned auto_increment primary key,
username varchar(20) not null unique,
password char(32) not null,
sex enum("男", "女", "保密"),
face varchar(50) not null,
regTime int unsigned not null
);


CREATE TABLE shop_album(
id  int unsigned auto_increment primary key,
pid int unsigned not null,
albumPath varchar(50) not null
);

