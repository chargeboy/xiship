<?php
require_once('../include.php');
error_reporting(0);
connect();
checkLogined();
//$order = $_REQUEST['order']?$_REQUEST['order']:null;
//$order_By = $order? "order by p.".$order:null;
$sql = "select p.id,p.pName,p.pSn,p.pNum,p.mPrice,p.iPrice,p.pDes,p.isShow,p.isHot,p.pubTime, c.cName from shop_pro as p JOIN shop_cate c on p.cId=c.id";
$totalRows = getResultNum($sql);
$pageSize = 2;
$totalPage = ceil($totalRows/$pageSize);
$page = $_REQUEST['page']?(int)$_REQUEST['page']:1;

if ($page<1 || $page==null || !is_numeric($page)) {
  $page= 1;
}
if ($page>=$totalPage) $page = $totalPage;

$offset = ($page - 1)*$pageSize;

$sql = "select p.id,p.pName,p.pSn,p.pNum,p.mPrice,p.iPrice,p.pDes,p.isShow,p.isHot,p.pubTime, c.cName from shop_pro as p JOIN shop_cate c on p.cId=c.id limit {$offset}, {$pageSize}";
$rows = fetchAll($sql);

//print_r($rows);

if (!$rows) {
  alertMese('没有书籍信息，请添加！', 'addCate.php');
  exit();
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <title>书籍列表信息</title>
  <link rel="stylesheet" href="styles/backstage.css">
  <link rel="stylesheet" href="scripts/jquery-ui/css/ui-lightness/jquery-ui-1.10.4.custom.css" />
  <script src="scripts/jquery-ui/js/jquery-1.10.2.js"></script>
  <script src="scripts/jquery-ui/js/jquery-ui-1.10.4.custom.js"></script>
  <script src="scripts/jquery-ui/js/jquery-ui-1.10.4.custom.min.js"></script>
</head>
<body>
<div class="details">
  <div class="details_operation clearfix">
    <div class="bui_select">
      <input type="button" value="添&nbsp;&nbsp;加" class="add"  onclick="addPro()">
    </div>

  </div>
  <!--表格-->
  <table class="table" cellspacing="0" cellpadding="0">
    <thead>
    <tr>
      <th width="10%">编号</th>
      <th width="25%">书籍名称</th>
      <th width="10%">书籍分类</th>
      <th width="10%">上架时间</th>
      <th width="10%">优惠价格</th>
      <th width="10%">是否上架</th>
      <th>操作</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($rows as $row):?>
      <tr>
        <!--这里的id和for里面的c1 需要循环出来-->
        <td><input type="checkbox" id="c1" class="check"><label for="c1" class="label"><?php echo $row['id'];?></label></td>

        <td><?php echo $row['pName'];?></td>
        <td><?php echo $row['cName'];?></td>
        <td><?php echo date("Y-m-d H:i:s", $row['pubTime']);?></td>
        <td><?php echo $row['iPrice'];?>元</td>
        <td><?php echo $row['isShow'] == 1?"上架":"下架";?></td>

        <td align="center">
          <input type="button" value="详情" class="btn" onclick="showDetail(<?php echo $row['id'];?>, '<?php echo $row['pName'];?>')">
          <input type="button" value="修改" class="btn" onclick="editPro(<?php echo $row['id'];?>)">
          <input type="button" value="删除" class="btn" onclick="delPro(<?php echo $row['id'];?>)">

          <div id="showDetail<?php echo $row['id'];?>" style="display:none;">
            <table class="table" cellspacing="0" cellpadding="0">
              <tr>
                <td width="20%" align="right">书籍名称</td>
                <td><?php echo $row['pName'];?></td>
              </tr>
              <tr>
                <td width="20%"  align="right">书籍类别</td>
                <td><?php echo $row['cName'];?></td>
              </tr>
              <tr>
                <td width="20%"  align="right">书籍编号</td>
                <td><?php echo $row['pSn'];?></td>
              </tr>
              <tr>
                <td width="20%"  align="right">书籍数量</td>
                <td><?php echo $row['pNum'];?>本</td>
              </tr>
              <tr>
                <td  width="20%"  align="right">书籍价格</td>
                <td><?php echo $row['mPrice'];?>元</td>
              </tr>
              <tr>
                <td  width="20%"  align="right">优惠价格</td>
                <td><?php echo $row['iPrice'];?>元</td>
              </tr>


              <!--<tr>
                <td width="20%"  align="right">书籍图片</td>
                <td>
                  <?php
/*                  $proImgs=getAllImgByProId($row['id']);
                  foreach($proImgs as $img):
                    */?>
                    <img width="100" height="100" src="uploads/<?php /*echo $img['albumPath'];*/?>" alt=""/> &nbsp;&nbsp;
                  <?php /*endforeach;*/?>
                </td>
              </tr>-->


              <tr>
                <td width="20%"  align="right">是否上架</td>
                <td>
                  <?php echo $row['isShow']==1?"上架":"下架";?>
                </td>
              </tr>
              <tr>
                <td width="20%"  align="right">是否热卖</td>
                <td>
                  <?php echo $row['isHot']==1?"热卖":"正常";?>
                </td>
              </tr>
            </table>

            <span style="display:block;width:80%; ">书籍描述<br/><?php echo $row['pDes'];?>
            </span>
          </div>


        </td>

      </tr>
    <?php endforeach;?>

    <?php if ($totalRows > $pageSize):?>
      <tr>
        <td colspan="7" align="center"><?php echo showPage($page, $totalPage);?></td>
      </tr>
    <?php endif;?>
    </tbody>
  </table>
</div>
</body>
<script type="text/javascript">
  function addPro() {
    window.location = "addPro.php";
  }
  function editPro(id) {
    window.location = "editPro.php?id="+id;
  }
  function delPro(id) {
    if (window.confirm("您确定要删除吗？删除后不可以恢复哦！！！")) {
      window.location = "doAdminAction.php?act=delPro&id=" + id;
    }
  }
  function showDetail(id,t){
    $("#showDetail"+id).dialog({
      height:"auto",
      width: "auto",
      position: {my: "center", at: "center",  collision:"fit"},
      modal:false,//是否模式对话框
      draggable:true,//是否允许拖拽
      resizable:true,//是否允许拖动
      title:"书籍名称："+t,//对话框标题
      show:"slide",
      hide:"explode"
    });
  }

</script>
</html>