<?php
require_once('../include.php');
checkLogined();
//connect();
$rows = getAllCate();
//print_r($rows);
if (!$rows) {
  alertMese("没有分类，请前往添加！", 'addCate.php');
}
$id = $_REQUEST['id'];
$proInfo = getProById($id);
?>
<!doctype html>
<html>
<head>
  <meta charset="utf-8">
  <title>添加书籍</title>
  <link href="./styles/global.css"  rel="stylesheet"  type="text/css" media="all" />
  <script type="text/javascript" charset="utf-8" src="../plugins/kindeditor/kindeditor.js"></script>
  <script type="text/javascript" charset="utf-8" src="../plugins/kindeditor/lang/zh_CN.js"></script>
  <script type="text/javascript" src="./scripts/jquery-1.6.4.js"></script>
  <script>
KindEditor.ready(function(K) {
  window.editor = K.create('#editor_id');
});
    $(document).ready(function(){
      $("#selectFileBtn").click(function(){
        $fileField = $('<input type="file" name="thumbs[]"/>');
        $fileField.hide();
        $("#attachList").append($fileField);
        $fileField.trigger("click");
        $fileField.change(function(){
          $path = $(this).val();
          $filename = $path.substring($path.lastIndexOf("\\")+1);
          $attachItem = $('<div class="attachItem"><div class="left">a.gif</div><div class="right"><a href="#" title="删除附件">删除</a></div></div>');
          $attachItem.find(".left").html($filename);
          $("#attachList").append($attachItem);
        });
      });
      $("#attachList>.attachItem").find('a').live('click',function(obj,i){
        $(this).parents('.attachItem').prev('input').remove();
        $(this).parents('.attachItem').remove();
      });
    });
  </script>

</head>
<body>
<h3>添加书籍</h3>
<form action="doAdminAction.php?act=editPro&id=<?php echo $id;?>" method="post" enctype="multipart/form-data">
  <table width="70%"  border="1" cellpadding="5" cellspacing="0" bgcolor="#cccccc">
    <tr>
      <td align="center">书籍名称</td>
      <td align="center"><input type="text" name="pName"  value="<?php echo $proInfo['pName'];?>"/></td>
    </tr>
    <tr>
      <td align="center">书籍分类</td>
      <td align="center">
      <select name="cId">
        <?php foreach($rows as $row):?>
        <option value="<?php echo $row['id'];?>" <?php echo $row['id'] == $proInfo['cId']?"'selected=selected'":null;?>><?php echo $row['cName'];?>
        </option>
        <?php endforeach;?>
      </select>
      </td>
    </tr>
    <tr>
      <td align="center">书籍编号</td>
      <td align="center"><input type="text" name="pSn"  value="<?php echo $proInfo['pSn'];?>"/></td>
    </tr>
    <tr>
      <td align="center">书籍数量</td>
      <td align="center"><input type="text" name="pNum"  value="<?php echo $proInfo['pNum'];?>"/></td>
    </tr>
    <tr>
      <td align="center">书籍市场价</td>
      <td align="center"><input type="text" name="mPrice"  value="<?php echo $proInfo['mPrice'];?>"/></td>
    </tr>
    <tr>
      <td align="center">书籍优惠价</td>
      <td align="center"><input type="text" name="iPrice"  value="<?php echo $proInfo['iPrice'];?>"/></td>
    </tr>
    <tr>
      <td align="center">书籍描述</td>
      <td align="center">
        <textarea name="pDes" id="editor_id" style="width:100%;height:150px;" value="<?php echo $proInfo['pDes'];?>"></textarea>
      </td>
    </tr>
    <tr>
      <td align="center">书籍图像</td>
      <td>
        <a href="javascript:void(0)" id="selectFileBtn">添加附件</a>
        <div id="attachList" class="clear"></div>
      </td>
    </tr>
    <tr>
      <td colspan="2" align="center"><input type="submit"  value="提交修改"/></td>
    </tr>
  </table>
</form>
</body>
</html>