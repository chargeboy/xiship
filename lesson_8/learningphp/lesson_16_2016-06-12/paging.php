<!DOCTYPE html>
<html>
<head>
	<title>数据分页</title>
	<meta http-equiv="Content-type" content="text/html; charset=utf-8">
</head>
<style>
	body{
		font-size: 12px;
		font-family: verdana;
		width: 100%;
	}
	div.content{
		height: 300px;
	}
	div.page{
		text-align: center;
	}
	div.page a{
		border: #aaaddd 1px solid;
		text-decoration: none;
		padding: 2px 5px 2px 5px;
		margin: 2px;
	}
	div.page span.current{
		border: #000099 1px solid;
		background-color: #000099;
		padding: 4px 6px 4px 6px; 
		margin: 2px;
		color: #fff;
		font-weight: bold;
	}
	div.page span.disable{
		border: #eee 1px solid;
		padding: 2px 5px 2px 5px;
		margin: 2px;
		color: #ddd;
	}
	div.page form{
		display: inline;
	}
</style>
<body>
<?php  
	// header("Content-type: text/html; charset=utf-8");

	//技术依赖  主要是使用 mysql 的 limit 关键字

	//1、传入页码
	 $page = $_GET['page'];

	// if (!$_GET['page']) {
	//       $page = 1;
	// }

	//2、根据页码取出数据：php+mysql
	$host = 'localhost';
	$username = 'root';
	$password = '123456';
	$dbname = 'test';
	$pagesize = 10;
	$showPage = 5; //显示页码的个数


	// 连接数据库
	$link = mysql_connect($host, $username, $password);
	if (!$link) {
		echo "数据库连接失败！<br />";
		exit;
	}else{
		echo "数据库连接成功！<br />";
	};

	//选择所要操作的数据库
	mysql_select_db($dbname);

	// 设置数据库编码格式
	mysql_query('SET NAMES UTF8');

	// 填写sql获取分页数据 SELECT * FROM 表名 LIMIT 起始位置, 显示条数;
	$sql = 'SELECT * FROM user LIMIT ' . ($page - 1) * $pagesize . ', 10';

	//把sql语句传送到数据中
	$result = mysql_query($sql);

	echo "<div class='content'>";
	// 处理我们的数据
	echo "<table border=1 cellspacing=0 width=50% align=center>";
	echo "<tr><td>ID</td><td>Name</td></tr>";
	while ($row = mysql_fetch_assoc($result)) {
		echo "<tr>";
		echo "<td>" . $row['Id'] . "</td>";
		echo "<td>" . $row['name'] . "</td>";
		echo "</tr>";
	}
	echo "</table>";
	echo "</div>";

	//关闭链接
	mysql_free_result($result);


	//获取数据总数
	$total_sql = 'SELECT COUNT(*) FROM  user';
	$total_result = mysql_fetch_array(mysql_query($total_sql));
	
	$total = $total_result[0];

	$page_number = ceil($total/$pagesize); //ceil是向上取 1 的意思

	//关闭链接
	mysql_close($link);

	//3、显示数据 + 分页条
	$page_banner = "<div class='page'>";

	//计算偏移量
	$pageOffset = ($showPage - 1) / 2;

	//初始化数据
	$start = 0;
	$end = $page_number;

	if ($page > 1){
		$page_banner .= "<a href='" . $_SERVER['PHP_SELF'] . "?page=1'>首页  </a>";
		$page_banner .= "<a href='" . $_SERVER['PHP_SELF'] . "?page=". ($page - 1) . "'><上一页</a>";
	}else{
		$page_banner .="<span class='disable'>首页 </span>";
		$page_banner .="<span class='disable'><上一页</span>";
	}


	//首部省略
	if ($page_number > $showPage) {
		if ($page > $pageOffset + 1) {
			$page_banner .= "...";
		}
		if ($page > $pageOffset) {
			$start = $page - $pageOffset;
			$end = $page_number > $page + $pageOffset ? $page + $pageOffset : $page_number; 
		}else{
			$start = 1;
			$end = $page_number > $showPage ? $showPage : $page_number;
		}

		if ($page + $pageOffset  > $page_number) {
			$start = $start - ($page + $pageOffset - $end);
		}
	}

	for ($i = $start ; $i <= $end ; $i ++) { 
		if ($page == $i) {
			$page_banner .= "<span class='current'>{$i}</span>";
		}else{
			$page_banner .= "<a href='". $_SERVER['PHP_SELF'] ."?page=".$i ."'> {$i} </a>";
		}
	}

	//尾部省略
	if ($page_number > $showPage && $page_number > $page + $pageOffset) {
		$page_banner .= "...";
	}

	if ($page < $page_number) {
	           $page_banner .= "<a href='" . $_SERVER['PHP_SELF'] ."?page=" . ($page + 1) . "'>下一页></a>";
	           $page_banner .= "<a href='" . $_SERVER['PHP_SELF'] ."?page={$page_number}'>  尾页</a>";
	}else{
		$page_banner .="<span class='disable'>下一页></span>";
		$page_banner .="<span class='disable'>  尾页</span>";
	}
	
	$page_banner .= "共{$page_number}页 - 当前第{$page}页";
	$page_banner .= "<form action='paging.php'>";
	$page_banner .= "到第<input type='text' size='2' name = 'page' />页";
	$page_banner .= "<input type='submit' value='确定'>";
	$page_banner .= "</form></div>";

	echo $page_banner;
?>
</body>
</html>