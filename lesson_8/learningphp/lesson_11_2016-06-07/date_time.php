<?php 	
	//获取当前的UNIX时间戳， UNIX 时间戳（英文叫做：timestamp）是 PHP 中关于时间与日期的一个很重要的概念，它表示从 1970年1月1日 00:00:00 到当前时间的秒数之和。

	$time = time(); //这个函数就获得了当前的时间戳
	echo $time; 

	echo '<br />'; 

	// php内置了date()函数，来取得当前的日期。date(时间戳的格式, 规定时间戳【默认是当前的日期和时间，可选】)

	//设置默认的时区
	date_default_timezone_set('Asia/Shanghai');

	//date函数，第二个参数取默认值的情况
	echo date("Y-m-d");

	echo '<br />'; 

	//date函数，第二个参数有值的情况
	echo date("Y-m-d",'1396193923');

	echo '<br />'; 

	// PHP提供了内置函数strtotime实现功能：获取某个日期的时间戳，或获取某个时间的时间戳。


	echo strtotime('2016-06-06');

	echo '<br />'; 

	echo strtotime('2016-06-06 00:00:01');

	echo '<br />'; 

	// 其实strtotime('2016-06-06')相当于strtotime('2016-06-06 00:00:00')


	// strtotime函数预期接受一个包含美国英语日期格式的字符串并尝试将其解析为 Unix 时间戳。

	// 函数说明：strtotime(要解析的时间字符串, 计算返回值的时间戳【默认是当前的时间，可选】)


	echo strtotime("now");//相当于将英文单词now直接等于现在的日期和时间，并把这个日期时间转化为unix时间戳。这个效果跟echo time();一样。

	echo '<br />'; 

	echo strtotime("+1 seconds");//相当于将现在的日期和时间加上了1秒，并把这个日期时间转化为unix时间戳。这个效果跟echo time()+1;一样。

	echo '<br />'; 

	echo strtotime("+1 day");//相当于将现在的日期和时间加上了1天。

	echo '<br />'; 

	echo strtotime("+1 week");//相当于将现在的日期和时间加上了1周。

	echo '<br />'; 

	echo strtotime("+1 week 3 days 7 hours 5 seconds");//相当于将现在的日期和时间加上了1周3天7小时5秒。

	echo '<br />'; 

	// gmdate 函数能格式化一个GMT的日期和时间，返回的是格林威治标准时（GMT）。

	// 举个例子，我们现在所在的中国时区是东八区，领先格林威治时间8个小时，有时候也叫GMT+8，那么服务器运行以下脚本返回的时间应该是这样的：
	// 当前时间假定是2016-06-06 23:12:00
	echo date('Y-m-d H:i:s', time()); 

	echo '<br />'; 

	echo gmdate('Y-m-d H:i:s', time()); 