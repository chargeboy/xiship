<?php 
	/**
	  * 一看文件名，那这次学的必须是数组呀，往下看
	  * 数组就是一个键值对组成的语言结构，键类似于酒店的房间号，值类似于酒店房间里存储的东
	  * 西。如果你去某个酒店住店，服务员会告诉你房间号是多少，具体房间里有存储了什么，那就
	  * 需要根据房间号进到房间里才知道了。
	  *
	  */

	$arr = array(); //表示创建一个空数组，并把创建的空数组赋值给变量$arr。


	/**
	  * PHP有两种数组：索引数组、关联数组。索引和关联两个词都是针对数组的键而言的。先介绍
	  *下索引数组，索引数组是指数组的键是整数的数组，并且键的整数顺序是从0开始，依次类推。
	  *
	  */

	$fruit = array("苹果","香蕉","菠萝");

	print_r($fruit);
	echo "<br />";

	// 索引数组赋值有三种方式:
	// 第一种：用数组变量的名字后面跟一个中括号的方式赋值，当然，索引数组中，中括号内的键一定是整数。比如，
		$arr[0]='苹果';

	// 第二种：用array()创建一个空数组，使用=>符号来分隔键和值，左侧表示键，右侧表示值。当然，索引数组中，键一定是整数。比如，
		array('0'=>'苹果');

	// 第三种：用array()创建一个空数组，直接在数组里用英文的单引号'或者英文的双引号"赋值，数组会默认建立从0开始的整数的键。比如
		
		array('苹果');

	// 这个数组相当于

		array('0'=>'苹果');

	//访问索引数组里的值
		$fruit = array('苹果','香蕉');
		$fruit0 = $fruit['0'];
		//如果数组已经设值
		if(isset($fruit)){
			print_r($fruit0);//结果为苹果
			echo "<br />";
		}


	//用for循环来访问数组里的值
	$fruit=array('苹果','香蕉','菠萝');

	for($i=0; $i<3; $i++){
	    echo '<br>数组第'.$i.'值是：'.$fruit[$i];
	}


	//用foreach循环来访问数组里的值
	$fruit=array('苹果','香蕉','菠萝');
	foreach($fruit as $k=>$v){
	    echo '<br>第'.$k.'值是：'.$v;
	}



	// 该说关联数组了，关联数组是指数组的键是字符串的数组。
	$fruit = array(
	    'apple'=>"苹果",
	    'banana'=>"香蕉",
	    'pineapple'=>"菠萝"
	); 

	print_r($fruit);
	echo "<br />";


	//关联数组赋值有两种方式:
	//第一种：用数组变量的名字后面跟一个中括号的方式赋值，当然，关联数组中，中括号内的键一定是字符串。比如，

		$arr['apple']='苹果';

	//第二种：用array()创建一个空数组，使用=>符号来分隔键和值，左侧表示键，右侧表示值。当然，关联数组中，键一定是字符串。比如，

		array('apple'=>'苹果');

	//访问关联数组
	$fruit = array(
		'apple'=>"苹果",
		'banana'=>"香蕉",
		'pineapple'=>"菠萝"
		); 
	$fruit0 = $fruit['banana'];
	print_r($fruit0);	

	echo "<br />";

	//foreach循环访问数组
	$fruit=array(
		'apple'=>"苹果",
		'banana'=>"香蕉",
		'pineapple'=>"菠萝"
		);
	
	foreach($fruit as $k=>$v){
	    echo '<br>水果的英文键名：'.$k.'，对应的值是：'.$v;
	}

