<?php  
	header("Content-type: text/html; charset=utf-8");

	//PHP通过安装相应的扩展驱动的方式实现数据库的操作，当前主流的数据库有MsSQL、MySQL、Sybase、Db2、Oracle、PostgreSQL、Access等，这些数据库PHP都能够安装扩展来支持，但是我们常用的却是MySQL，当今最流行的LAMP或者LNMP架构就是Linux、Apache/nginx、MySQL、PHP，所以我们也使用MySQL.

	//PHP中一个数据库可能有一个或者多个扩展，其中既有官方的，也有第三方提供的。像MySQL常用的扩展有原生的mysql库，也可以使用增强版的mysqli扩展，还可以使用PDO进行连接与操作。

	// 原生mysql扩展进行数据库连接的方法：
	// $link = mysql_connect('mysql_host', 'mysql_user', 'mysql_password');
	// 
	// 这是面向过程的就去，建议使用面向对象的方法
	$link = mysql_connect('localhost', 'root', '123456');
	if ($link) {
		echo "mysql 连接成功 <br />";
	}else{
		echo "mysql 连接失败 <br />";
	}
	mysql_close($link);

	// mysqli扩展
	// $link = mysqli_connect('mysql_host', 'mysql_user', 'mysql_password');
	$link = mysqli_connect('localhost', 'root', '1234567');
	if ($link) {
		echo "mysqli 连接成功 <br />";
	}else{
		echo "mysqli 连接失败 <br />";
	}



	// PDO扩展
	// $dsn = 'mysql:dbname=testdb;host=127.0.0.1';
	// $user = 'dbuser';
	// $password = 'dbpass';
	// $dbh = new PDO($dsn, $user, $password);

	$dsn = 'mysql:dbname=test;host=127.0.0.1';
	$user = 'root';
	$password = '123456';
	$dbh = new PDO($dsn, $user, $password);

	if ($dbh) {
		echo "PDO 连接成功 <br />";
	}else{
		echo "PDO 连接失败 <br />";
	}



	$link = mysql_connect('127.0.0.1', 'root', '123456') or die('数据库连接失败');
	mysql_select_db('mysql');   //选择一个数据库
	mysql_query("set names 'utf8'");  //设置一下当前连接使用的字符编码，一般我们会使用utf8编码
	$result = mysql_query('select * from user');   //执行查询语句

	$row = mysql_fetch_assoc($result);  //获取查询结果

	$rows = mysql_fetch_array($result);  //获取查询结果

	var_dump($row);

	echo "<br />";

	var_dump($rows);

	echo "<br />";
	mysql_close($link);

	// 当我们了解了如何使用mysql_query进行数据查询以后，插入数据其实也是类似的
	$link = mysql_connect('127.0.0.1', 'root', '123456') or die('数据库连接失败');
	mysql_select_db('test');   //选择一个数据库

	mysql_query("set names 'utf8'");  //设置一下当前连接使用的字符编码，一般我们会使用utf8编码

	$sql = "insert into user(name, age, class) values('李四', 18, '高三一班')";
	mysql_query($sql); //执行插入语句

	echo "<br />";

	$uid = mysql_insert_id();

	echo $uid;

	echo "<br />";

	// $name = '李四';
	// $age = 18;
	// $class = '高三一班';
	// $sql = "insert into user(name, age, class) values('$name', '$age', '$class')"; //或者这样将变量拼接到sql语句中
	// mysql_query($sql); //执行插入语句

	mysql_close($link);

	// 在mysql中，执行插入语句以后，可以得到自增的主键id,通过PHP的mysql_insert_id函数可以获取该id。这个id的作用非常大，通常可以用来判断是否插入成功，或者作为关联ID进行其他的数据操作。


	// PHP有多个函数可以获取数据集中的一行数据，最常用的是mysql_fetch_array，可以通过设定参数来更改行数据的下标，默认的会包含数字索引的下标以及字段名的关联索引下标。

	$link = mysql_connect('127.0.0.1', 'root', '123456') or die('数据库连接失败');
	mysql_select_db('test');   //选择一个数据库
	mysql_query("set names 'utf8'");  //
	$sql = "select * from user";
	$result = mysql_query($sql);
	$row = mysql_fetch_array($result);

	// 可以通过设定参数MYSQL_NUM只获取数字索引数组，等同于mysql_fetch_row函数，如果设定参数为MYSQL_ASSOC则只获取关联索引数组，等同于mysql_fetch_assoc函数。


	$row = mysql_fetch_row($result);
	$row = mysql_fetch_array($result, MYSQL_NUM); //这两个方法获取的数据是一样的

	$row = mysql_fetch_assoc($result);
	$row = mysql_fetch_array($result, MYSQL_ASSOC);


	// 如果要获取数据集中的所有数据，我们通过循环来遍历整个结果集。

	$data = array();
	while ($row = mysql_fetch_array($result)) {
	    $data[] = $row;
	}

	var_dump($data);

	echo "<br />";

	mysql_close($link);

	// 查询数据分页
	// 通过mysql的limit可以很容易的实现分页，limit m,n表示从m行后取n行数据，在PHP中我们需要构造m与n来实现获取某一页的所有数据。

	// 假定当前页为$page，每页显示$n条数据，那么m为当前页前面所有的数据，既$m = ($page-1) * $n，在知道了翻页原理以后，那么我们很容易通过构造SQL语句在PHP中实现数据翻页。


	$link = mysql_connect('127.0.0.1', 'root', '123456') or die('数据库连接失败');
	mysql_select_db('test');   //选择一个数据库
	mysql_query("set names 'utf8'");  //

	$page = 1;
	$pagesize = 10;
	$offset = ($page - 1) * $pagesize;
	$sql = "select * from user limit $offset, $pagesize";
	$result = mysql_query($sql);
	//循环获取当前页的数据
	$data = array();
	while ($row = mysql_fetch_assoc($result)) {
	    $data[] = $row;
	}

	echo '<pre>';
	var_dump($data);
	echo '</pre>';
	echo "<br />";

	mysql_close($link);

	// 更新数据
	// 数据的更新与删除相对比较简单，只需要构建好相应的sql语句，然后调用mysql_query执行就能完成相应的更新与删除操作。

	$link = mysql_connect('127.0.0.1', 'root', '123456') or die('数据库连接失败');
	mysql_select_db('test');   //选择一个数据库
	mysql_query("set names 'utf8'");  //
	//limit 1 是为了优化数据库查询，只要查到了就不会再继续往下查了
	$sql = "update user set name = '曹操' where id=10 limit 1";
	if (mysql_query($sql)) {
	    echo '更新成功 <br />';
	}

	mysql_close($link);


	// 删除数据
	$link = mysql_connect('127.0.0.1', 'root', '123456') or die('数据库连接失败');
	mysql_select_db('test');   //选择一个数据库
	mysql_query("set names 'utf8'");  //

	$sql = "delete from user where id=1 limit 1";
	if (mysql_query($sql)) {
	    echo '删除成功 <br />';
	}

	mysql_close($link);


	// 对于删除与更新操作，可以通过mysql_affected_rows函数来获取更新过的数据行数，如果数据没有变化，则结果为0。
	$link = mysql_connect('127.0.0.1', 'root', '123456') or die('数据库连接失败');
	mysql_select_db('test');   //选择一个数据库
	mysql_query("set names 'utf8'");  //

	$sql = "update user set name = '曹操' where id=4 limit 1";
	if (mysql_query($sql)) {
	    echo mysql_affected_rows() . '<br />';
	}

	mysql_close($link);