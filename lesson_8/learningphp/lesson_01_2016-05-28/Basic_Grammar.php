<?php 
/**
  *   PHP是一种运行在服务器端的脚本语言，学习成本低，学习也快捷，而且功能与效率都不差；
  *   PHP是跨平台的语言，是完全免费开源的，特别是跟Linux、MySql的结合非常优秀；在世界上
  *   许许多多的Web服务器上都运行着PHP写的程序。
  *   
  *   PHP脚本都是以 .php后缀的文件，可以嵌套html，css，js等前端代码，虽然PHP脚本有很多种标
  *   示，但是一般我们都选择<?php 开头 ?>结尾的写法，而如果整个文件都是PHP脚本程序的话，
  *   我们可以省略掉 ?> 的结尾。
  *   PHP开发环境的配置请参考这里 http://blog.duicode.com/431.html
  *   废话不多说，开始学习。
  *   alter:panchaozhi
  *   desc:不错，写得不错！
  */
 ?>

<?php 
	//变量声明
	$number = 1;
	$string = 'string';
	//这里要说一下字符串的声明，可以使用 '单引号' ，也可以使用 "双引号"，
	//如果遇到字符串中半酣引号了怎么办呢？请看 
	$stringTem = '这是一个包含"双引号"的字符串';  //无情嵌套
	$stringTemp = "这是一个包含'双引号'的字符串"; //同上
	$stringTempe = "这是一个包含\"双引号\"的字符串"; //疯狂转义，输出带双引号
	//当双引号遇上变量时，变量就是变量，当单引号遇见变量时，变量名就变成一个字符串
	$stringTest = "变量 $stringTemp " . '变量 $stringTemp';
	$isTrue = true;
	$float = 1.5;
	$array=array('name' => 'duicode.com');

	//变量命名规则：驼峰命名法
	$helloWorld = 'Hello World!';

	// 输出一句话或者输出变量
	echo "Hello World!";
	echo '<br />'; //这可是HTML的换行哦
	print('Hello World!');
	echo '<br />';
	printf('Hello World!');
	echo '<br />';
	print_r('Hello World!');
	echo '<br />';

	//这里用到了神奇的 . 点连接符，可以将字符串和计算公式或者变量连接起来拼接成字符串
	echo('13 * 2 = ' .  13 * 2);
	echo '<br />';
	echo($number);
	echo '<br />';
	echo($stringTest);
	echo '<br />';

	//但是数组不能这样输出了，有专门的方法
	var_dump($array);
	var_dump($number);//这里直接把变量类型都打印出来了~

	//不要忘了每句代码后面的结束符号 ; 哦！

	//也看到注释方法咯，//单行注释；   /*  */ 多行注释；
	

	//有一种类型叫资源类型，虽不常用，但也要知道呀。
	$file = fopen("f.txt","r");//r是对文件仅进行只读
	$chars = fgets($file);//fgets是对文件指针读取一行
	echo($chars);
	echo '<br />';

	//l另一种类型叫NULL类型

	error_reporting(0); //禁止显示PHP警告
	$var;
	var_dump($var);
	echo '<br />';
	$var1 = null;
	var_dump($var1);
	echo '<br />';
	$var2 = NULL;
	var_dump(var2);
	echo '<br />';
	$var3 = "helloWorld";
	unset($var3); //释放 $var3
	var_dump($var3);
	echo '<br />';