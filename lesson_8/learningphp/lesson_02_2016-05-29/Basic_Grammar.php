<?php 
	
	//常量就是不变的量，使用常量可以避免重复定义，还可以提高项目的可维护性，来定义一个常量玩玩
	$p = 'PII';
	define('PI', 3.14);
	define($p, 3.14);
	echo(PI);
	echo '<br />';
	echo(PII);
	echo '<br />';


	// 系统常量是PHP已经定义好的常量，我们可以直接拿来使用，来看看你常见的系统常量
	// __FILE__ :php程序文件名。它可以帮助我们获取当前文件在服务器的物理位置。
	echo __FILE__;
	echo '<br />';

	// __LINE__ :PHP程序文件行数。它可以告诉我们，当前代码在第几行。
	echo __LINE__;
	echo '<br />';

	// PHP_VERSION:当前解析器的版本号。它可以告诉我们当前PHP解析器的版本号，我们可以提前知道我们的PHP代码是否可被该PHP解析器解析。
	echo PHP_VERSION;
	echo '<br />';

	// PHP_OS：执行当前PHP版本的操作系统名称。它可以告诉我们服务器所用的操作系统名称，我们可以根据该操作系统优化我们的代码。
	echo PHP_OS;
	echo '<br />';

	// 获取常量值的有两种方法取值。第一种是使用常量名直接获取值
	$r=10;
	$area = PI * $r * $r; //计算圆的面积
	echo $area;
	echo '<br />';

	// 第二种是使用constant()函数。它和直接使用常量名输出的效果是一样的，但函数可以动态的输出不同的常量，在使用上要灵活、方便
	
	$p;
	//定义圆周率的两种取值
	define("PI1",3.14);
	define("PI2",3.142);
	//定义值的精度
	$height = "中";
	//根据精度返回常量名，将常量变成了一个可变的常量
	if($height == "中"){
	    	$p = "PI2";
	}else if($height == "低"){
		$p = "PI1";
	}
	$r=100;

	$area1 = constant($p) *$r *$r;
	echo $area1;

	//判断常量是否被定义使用defined函数，若存在则返回布尔类型true，
	//否则返回布尔类型false; 
	define("PI3",3.14);
	$p = "PI3";

	$is1 = defined($p);
	$is2 = defined("PI4");

	var_dump($is1);
	var_dump($is2);

	  // 常用的运算符有
	     	// 算术运算符 +、 -、 *、 /、%
	 

	     	 // 赋值运算符 =、&（引用赋值，两个变量都指向同一个数据存储地址）
	 

	      	 //比较运算符 ==、===（全等，值等且类型等）、!=（<>）、!==（非全等，值不等或者类型不等）、>、<、<=、>=
	 	

	      	 // 三目运算符  
		$a =78;
		 $b = $a >= 60 ? "及格" : "不及格";
		 var_dump($b);
		 echo '<br />';


	 	 //逻辑运算符
		 $a = TRUE;
		 $b = TRUE;
		 var_dump($a and $b); //逻辑与
		 echo '<br />';
		 var_dump($a && $b); //逻辑与
		 echo '<br />';
		 var_dump($a or $b); //逻辑或
		 echo '<br />';
		 var_dump($a || $b); //逻辑或
		 echo '<br />';
		 var_dump($a xor $b);//逻辑异或
		 echo '<br />';
		 var_dump(!$a); //逻辑非
		 echo '<br />';


		 //字符串连接运算符  . 点连接符 和 .=


		 //错误控制运算符。
		 // PHP中提供了一个错误控制运算符“@”，对于一些可能会在运行过程中出错的表达式时，我们不希望出错的时候给客户显示错误信息，这样对用户不友好。于是，可以将@放置在一个PHP表达式之前，该表达式可能产生的任何错误信息都被忽略掉；
		// 如果激活了track_error（这个玩意在php.ini中设置）特性，表达式所产生的任何错误信息都被存放在变量$php_errormsg中，此变量在每次出错时都会被覆盖，所以如果想用它的话必须尽早检查。
		// 需要注意的是：错误控制前缀“@”不会屏蔽解析错误的信息，不能把它放在函数或类的定义之前，也不能用于条件结构例如if和foreach等。

		 $conn = @mysql_connect("localhost","username","password");
		 echo "出错了，错误原因是：".$php_errormsg;