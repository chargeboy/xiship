<?php  
	header("Content-type: text/html; charset=utf-8");

	//技术依赖  主要是使用 mysql 的 limit 关键字

	//1、传入页码
	 $page = $_GET['page'];

	// if (!$_GET['page']) {
	//       $page = 1;
	// }

	//2、根据页码取出数据：php+mysql
	$host = 'localhost';
	$username = 'root';
	$password = '123456';
	$dbname = 'test';
	$pagesize = 10;

	// 连接数据库
	$link = mysql_connect($host, $username, $password);
	if (!$link) {
		echo "数据库连接失败！<br />";
		exit;
	}else{
		echo "数据库连接成功！<br />";
	};

	//选择所要操作的数据库
	mysql_select_db($dbname);

	// 设置数据库编码格式
	mysql_query('SET NAMES UTF8');

	// 填写sql获取分页数据 SELECT * FROM 表名 LIMIT 起始位置, 显示条数;
	$sql = 'SELECT * FROM user LIMIT ' . ($page - 1) * $pagesize . ', 10';

	//把sql语句传送到数据中
	$result = mysql_query($sql);

	// 处理我们的数据
	echo "<table border=1 cellspacing=0 width=40%>";
	echo "<tr><td>ID</td><td>Name</td></tr>";
	while ($row = mysql_fetch_assoc($result)) {
		echo "<tr>";
		echo "<td>" . $row['Id'] . "</td>";
		echo "<td>" . $row['name'] . "</td>";
		echo "</tr>";
	}
	echo "</table>";

	//关闭链接
	mysql_free_result($result);


	//获取数据总数
	$total_sql = 'SELECT COUNT(*) FROM  user';
	$total_result = mysql_fetch_array(mysql_query($total_sql));
	
	$total = $total_result[0];

	$page_number = ceil($total/$pagesize); //ceil是向上取 1 的意思

	//关闭链接
	mysql_close($link);

	//3、显示数据 + 分页条

	$page_banner = "<a href='" . $_SERVER['PHP_SELF'] . "?page=". ($page - 1) . "'>上一页</a>";
	$page_banner .= "  <a href='" . $_SERVER['PHP_SELF'] .	 "?page=" . ($page + 1) . "'>下一页</a>";
	$page_banner .= "  共{$page_number}页 - 当前第{$page}页";
	echo $page_banner;
