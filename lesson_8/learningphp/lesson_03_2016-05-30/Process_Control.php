<?php 
/**
   *     一个问题肯定有解决方法吧？那还用说，那在编程语言中也是这样，这就涉及到了程
   *     序语言中的流程控制， 今天搞一搞PHP中的流程控制。
   *   
   */

	//第一种：顺序结构，顾名思义，当然是按照顺序来执行了
	$price = 100;
	$number = 10;

	$result = $price * $number;

	echo "最终的加个为" . $result;
	echo '<br />';

	//第二种：条件结构，条件结构分好几种，下面就介绍一下
	$sex = '男' ;
	if ($sex == '男') {
		echo "是男的";
		echo '<br />';
	}else{
		echo "是女的";
		echo '<br />';
	}


	if ($sex == '男') {
		echo "是男的";
		echo '<br />';
	}elseif ($sex == '女') {
		echo "是女的";
		echo '<br />';
	}


	if ($sex == '男') {
		echo "是男的";
		echo '<br />';
	}elseif ($sex == '女') {
		echo "是女的";
		echo '<br />';
	}else{
		echo "未知性别";
		echo '<br />';
	}


	//其中break显而易见，如果没有就不跳出循环，那多不好
	$value = 3;
	switch ($value) {
		case 1:
			echo '没奖<br />';
			break;

		case 2:
			echo '没奖<br />';
			break;
		
		case 3:
			echo '没奖<br />';
			break;
		
		case 4:
			echo '恭喜你，中奖啦<br />';
			break;
		
		default:
			echo '没奖<br />';
			break;
	}

	//下面两个的区别就是一个先算后比，一个先比后算，所以do...while至少执行一次
	while ( $value <= 10) {
		$value ++;
		echo "value的值为" . $value;
		echo '<br />';
	}


	do{
		$value ++;
		echo "value的值为" . $value;
		echo '<br />';
	} while ( $value <= 20);


	//for(初始化;循环条件;递增项){执行任务}
	for ($i=0; $i < 10 ; $i++) { 
		echo "i 的 值 为" . $i;
		echo '<br />';
	}


	//PHP中用的很多的循环  foreach (数组 as 值){执行的任务}

	$students = array(
		'2010'=>'令狐冲',
		'2011'=>'林平之',
		'2012'=>'曲洋',
		'2013'=>'任盈盈',
		'2014'=>'向问天',
		'2015'=>'任我行',
		'2016'=>'冲虚',
		'2017'=>'方正',
		'2018'=>'岳不群',
		'2019'=>'宁中则',
		);//10个学生的学号和姓名，用数组存储


	foreach ($students as $v) {
		echo "v 为 " . $v;
		echo '<br />';
	}


	foreach ($students as $key => $value) {
		echo "key 为 " . $key . "，value 为 " . $value;
		echo '<br />';
	}


	//循环的多层嵌套1
	$totalMoney = 0;//总工资
	$basicMoney =  300;//基本工资
	$sex = "男";
	$noHouse = TRUE; //是否有房
	$houseMoney =  150;//住房补贴
	$isPregnancy = TRUE; //是否怀孕
	$pregnancyMoney =  100;//怀孕补贴
	
	if($sex == '男'){
		$totalMoney = $basicMoney  + 0;// 男的没奖金
		if($noHouse){
			$totalMoney = $totalMoney  + $houseMoney;
		} 
	}else if($sex == '女'){
		$totalMoney = $basicMoney  + 300;// 女的有奖金300元
		if($isPregnancy){
			$totalMoney = $totalMoney  + $pregnancyMoney;
		} 
	}
	echo $totalMoney;
	echo "<br />";



	//循环的多层嵌套2
	$students = array(
		'2010'=>array('令狐冲',"59"),
		'2011'=>array('林平之',"44"),
		'2012'=>array('曲洋',"89"),
		'2013'=>array('任盈盈',"92"),
		'2014'=>array('向问天',"93"),
		'2015'=>array('任我行',"87"),
		'2016'=>array('冲虚',"58"),
		'2017'=>array('方正',"74"),
		'2018'=>array('岳不群',"91"),
		'2019'=>array('宁中则',"90"),
		);//10个学生的学号、姓名、分数，用数组存储
		 

		foreach ($students as $key => $value) { 
		//使用循环结构遍历数组,获取学号 
		     	echo $key; //输出学号
			 echo ":";
			 //循环输出姓名和分数
			 foreach ($value as $v){
				echo $v; 
			 }
			 echo "<br />";
		}




	//循环的多层嵌套3
	$students = array(
		'2010'=>'令狐冲',
		'2011'=>'林平之',
		'2012'=>'曲洋',
		'2013'=>'任盈盈',
		'2014'=>'向问天',
		'2015'=>'任我行',
		'2016'=>'冲虚',
		'2017'=>'方正',
		'2018'=>'岳不群',
		'2019'=>'宁中则',
		);//10个学生的学号和姓名，用数组存储
		$query = '2014';
		//使用循环结构遍历数组,获取学号和姓名

		foreach ($students as $key => $value) { 
		    //使用条件结构，判断是否为该学号
			if ($key == $query) { 
				echo $value;//输出（打印）姓名
				echo "<br />";
				break;//结束循环（跳出循环）
			}
		}