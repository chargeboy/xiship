<?php 
	/**
	  *
	  *  PHP内置了超过1000个函数，因此PHP是一门非常强大的语言。尽管PHP中有这么多函数了，
	  *  但是我们也很需要自定义函数通过将一组代码封装起来，使语言真正的为我们所用。
	  *
	  */

	    // PHP函数的定义方式：
	    // 1.使用关键字“function”开始
	    // 2.函数名可以是字母或下划线开头：function name()
	    // 3.在大括号中编写函数体：
	function helloWorld(){
		echo "Hello  World";
		echo "<br />";
	}

	//调用函数
	helloWorld();


	// 函数可以没有参数，也可以有多个参数，这是必须的嘛，来试试
	function sum($a, $b){
		return $a + $b;
	}

	$sum = sum(1, 2);
	echo $sum;
	echo "<br />";

	//这里也看到了，函数是有返回值的，在没有return的情况下返回值为null，但是函数没有多个返回值，所以有多个返回值的时候就可以组成一个数组返回，遇到return函数就结束了。
	function numbers() {
	   	 return array(1, 2, 3);
	}
	list ($one, $two, $three) = numbers();

	echo $one . '<br />' . $two . '<br />' . $three . '<br />';

	//可变函数就是使用变量的值来调用函数
	function name(){
		echo "sunny <br />";
	}

	$func = 'name';

	$func();

	//在类中也可以这样调用
	class book{
		function name(){
			echo "funny <br />";
		}
	}

	$book = new book();

	$book -> $func();

	//PHP中有超过1000个的内置函数，内置函数就是PHP系统自带的函数
	$str = '我是个大香蕉';
	$str = str_replace('香蕉', '苹果', $str); //替换函数
	echo $str; 
	echo "<br />";

	//当我们创建了自定义函数，并且了解了可变函数的用法，为了确保程序调用的函数是存在的，经常会先使用function_exists判断一下函数是否存在。同样的method_exists可以用来检测类的方法是否存在，类是否定义可以使用class_exists，PHP中有很多这类的检查方法，例如文件是否存在file_exists。

	function func() {
	}

	if (function_exists('func')){
	    echo '存在';
	    echo "<br />";
	}

	class MyClass{
	}
	// 使用前检查类是否存在
	if (class_exists('MyClass')) {
	    $myclass = new MyClass();
	    echo '也存在';
	    echo "<br />";
	}


	$filename = 'test.txt';
	if (!file_exists($filename)) {
	    echo $filename . '不存在';
	    echo "<br />";
	}
	