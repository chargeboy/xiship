#LearningPHP

**主要是回顾一下以前学习的PHP基础知识**

- 2016-05-28 PHP基本语法和变量
- 2016-05-29 PHP中的常量
- 2016-05-30 PHP中的流程控制
- 2016-05-31 PHP中的数组
- 2016-06-01 PHP中的函数
- 2016-06-02 PHP中的类和对象
- 2016-06-03 PHP中的字符串
- 2016-06-04 PHP中的正则表达式
- 2016-06-05 PHP中的cookie && session
- 2016-06-06 PHP中的 file 操作
- 2016-06-07 PHP中的日期和时间
- 2016-06-08 PHP中的图像操作
- 2016-06-09 PHP中的异常处理
- 2016-06-10 PHP连接MySQL
- 2016-06-11 PHP+MySQL分页
- 2016-06-12 PHP+MySQL分页收尾