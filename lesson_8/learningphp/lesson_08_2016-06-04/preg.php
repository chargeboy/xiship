<?php 
	//又是正则表达式呀，在博客上都介绍过了，这次直接走起
	//   正则表达式基础: http://blog.duicode.com/453.html
	//   PHP中常用的正则表达式函数: http://blog.duicode.com/451.html
	
	$p = '/apple/';  //这个就是正则表达式
	$str = "apple banna";
	if (preg_match($p, $str)) {  //这个是PHP中的函数，来判断一类字符模式是否存在
	    echo '匹配成功 <br />';
	}

	/*
	\ 一般用于转义字符
	^ 断言目标的开始位置(或在多行模式下是行首)
	$ 断言目标的结束位置(或在多行模式下是行尾)
	. 匹配除换行符外的任何字符(默认)
	[ 开始字符类定义
	] 结束字符类定义
	| 开始一个可选分支
	( 子组的开始标记
	) 子组的结束标记
	? 作为量词，表示 0 次或 1 次匹配。位于量词后面用于改变量词的贪婪特性。 (查阅量词)
	* 量词，0 次或多次匹配
	+ 量词，1 次或多次匹配
	{ 自定义量词开始标记
	} 自定义量词结束标记


	\s匹配任意的空白符，包括空格，制表符，换行符。[^\s]代表非空白符。[^\s]+ 表示一次或多次匹配非空白符。
	*/

	$p = '/^我[^\s]+(苹果|香蕉)$/';
	$str = "我喜欢吃苹果";
	if (preg_match($p, $str)) {
	    echo '匹配成功  <br />';
	}

	/*
	元字符具有两种使用场景，一种是可以在任何地方都能使用，另一种是只能在方括号内使用，在方括号内使用的有：

	\ 转义字符
	^ 仅在作为第一个字符(方括号内)时，表明字符类取反
	- 标记字符范围

	其中^在反括号外面，表示断言目标的开始位置，但在方括号内部则代表字符类取反，方括号内的减号-可以标记字符范围，例如0-9表示0到9之间的所有数字。
	*/


	// 贪婪模式：在可匹配与可不匹配的时候，优先匹配
	//   \d表示匹配数字
	$p = '/\d+\-\d+/';
	$str = "我的电话是010-12345678";
	preg_match($p, $str, $match);
	echo $match[0]; 
	echo "<br />";


	// 懒惰模式：在可匹配与可不匹配的时候，优先不匹配

	$p = '/\d?\-\d?/';
	$str = "我的电话是010-12345678";
	preg_match($p, $str, $match);
	echo $match[0];  
	echo "<br />";

	// 当我们确切的知道所匹配的字符长度的时候，可以使用{}指定匹配字符数
	$p = '/\d{3}\-\d{8}/';
	$str = "我的电话是010-12345678";
	preg_match($p, $str, $match);
	echo $match[0]; 
	echo "<br />";


	/*
	为什么要使用正则表达式呢？
	使用正则表达式的目的是为了实现比字符串处理函数更加灵活的处理方式，因此跟字符串处理函数一样，其主要用来判断子字符串是否存在、字符串替换、分割字符串、获取模式子串等。
	*/

	$subject = "abcdef";
	$pattern = '/def/';
	preg_match($pattern, $subject, $matches);
	var_dump($matches); 
	echo "<br />";

	// 上面的代码简单的执行了一个匹配，简单的判断def是否能匹配成功，但是正则表达式的强大的地方是进行模式匹配，因此更多的时候，会使用模式：

	$subject = "abcdef";
	$pattern = '/a(.*?)d/';
	preg_match($pattern, $subject, $matches);
	var_dump($matches); 
	echo "<br />";

	// preg_match只能匹配一次结果，但很多时候我们需要匹配所有的结果，preg_match_all可以循环获取一个列表的匹配结果数组。


	$p = "|<[^>]+>(.*?)</[^>]+>|i";
	$str = "<b>example: </b><div align=left>this is a test</div>";
	preg_match_all($p, $str, $matches);
	var_dump($matches);

	echo "<br />";

	// 可以使用preg_match_all匹配一个表格中的数据：

	$p = "/<tr><td>(.*?)<\/td>\s*<td>(.*?)<\/td>\s*<\/tr>/i";
	$str = "<table> <tr><td>Eric</td><td>25</td></tr> <tr><td>John</td><td>26</td></tr> </table>";
	preg_match_all($p, $str, $matches);
	var_dump($matches);

	echo "<br />";

	// $matches结果排序为$matches[0]保存完整模式的所有匹配, $matches[1] 保存第一个子组的所有匹配，以此类推。



	// 正则表达式的搜索和替换
	$string = 'April 15, 2014';
	$pattern = '/(\w+) (\d+), (\d+)/i';
	$replacement = '$3, ${1} $2'; //还不是很懂啊
	echo preg_replace($pattern, $replacement, $string);
	// 其中${1}与$1的写法是等效的，表示第一个匹配的字串，$2代表第二个匹配的。
	echo "<br />";


	// 通过复杂的模式，我们可以更加精确的替换目标字符串的内容。
	$patterns = array ('/(19|20)(\d{2})-(\d{1,2})-(\d{1,2})/',
                   '/^\s*{(\w+)}\s*=/');
	$replace = array ('\3/\4/\1\2', '$\1 =');//\3等效于$3,\4等效于$4，依次类推
	echo preg_replace($patterns, $replace, '{startDate} = 1999-5-27'); 
	//详细解释下结果：(19|20)表示取19或者20中任意一个数字，(\d{2})表示两个数字，(\d{1,2})表示1个或2个数字，(\d{1,2})表示1个或2个数字。^\s*{(\w+)\s*=}表示以任意空格开头的，并且包含在{}中的字符，并且以任意空格结尾的，最后有个=号的。

	echo "<br />";


	// 用正则替换来去掉多余的空格与字符：
	$str = 'one     two';
	$str = preg_replace('/\s+/', ' ', $str);
	echo $str; // 结果改变为'one two'

	echo "<br />";


	// 常用案例
	$user = array(
	    'name' => 'spark1985',
	    'email' => 'spark@imooc.com',
	    'mobile' => '13312345678'
	);
	//进行一般性验证
	if (empty($user)) {
	    die('用户信息不能为空');
	}
	if (strlen($user['name']) < 6) {
	    die('用户名长度最少为6位');
	}
	//用户名必须为字母、数字与下划线
	if (!preg_match('/^\w+$/i', $user['name'])) {
	    die('用户名不合法');
	}
	//验证邮箱格式是否正确
	if (!preg_match('/^[\w\.]+@\w+\.\w+$/i', $user['email'])) {
	    die('邮箱不合法');
	}
	//手机号必须为11位数字，且为1开头
	if (!preg_match('/^1\d{10}$/i', $user['mobile'])) {
	    die('手机号不合法');
	}
	echo '用户信息验证成功';











