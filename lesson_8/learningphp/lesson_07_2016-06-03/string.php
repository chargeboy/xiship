<?php 
	//不管是什么开发，我们遇到最多的可能就是字符串，PHP也不例外呀，一个字符串 通过下面的3种方法来定义：
		// 1、单引号
		// 2、双引号
		// 3、heredoc语法结构

	// 单引号定义的字符串：
	$hello = 'hello world';

	// 双引号定义的字符串：
	$hello = "hello world";

	// heredoc语法结构定义的字符串：
	//然而并不好用
	// $hello = <<<TAG
	// hello world
	// TAG;
	
	//单双引号有啥区别呢？PHP允许我们在双引号串中直接包含字串变量。而单引号串中的内容总被认为是普通字符。

	$str='hello';
	echo "str is $str"; //运行结果: str is hello
	echo '<br />';
	echo 'str is $str'; //运行结果: str is $str
	echo '<br />';

	//PHP中用英文的点号 . 来连接两个字符串。

	$hello='hello';
	$world=' world';
	$hi = $hello.$world;
	echo $hi;//我们可以用echo函数输出一下这个字符串连接。
	echo '<br />';


	//PHP中有三个函数可以去掉字符串的空格
	// trim去除一个字符串两端空格。
	// rtrim是去除一个字符串右部空格，其中的r是right的缩写。
	// ltrim是去除一个字符串左部空格，其中的l是left的缩写。
	echo trim(" 空格 ")."<br>";
	echo rtrim(" 空格 ")."<br>";
	echo ltrim(" 空格 ")."<br>";

	//获取字符串的长度  英文字符就用 strlen()。
	$str = 'hello';
	$len = strlen($str);
	echo $len;//输出结果是5
	echo "<br />";

	//有中文字符的话用 mb_strlen()
	$str = "我爱你";
	echo mb_strlen($str,"UTF8");
	echo "<br />";


	//截取字符串  英文substr(字符串变量,开始截取的位置，截取个数），中文 mb_substr(字符串变量,开始截取的位置，截取个数, 网页编码）
	$str='i love you';
	//截取love这几个字母
	echo substr($str, 2, 4);//为什么开始位置是2呢，因为substr函数计算字符串位置是从0开始的，也就是0的位置是i,1的位置是空格，l的位置是2。从位置2开始取4个字符，就是love。

	$str='我爱你，中国';
	//截取中国两个字
	echo mb_substr($str, 4, 2, 'utf8');//为什么开始位置是4呢，和上一个例子一样，因为mb_substr函数计算汉字位置是从0开始的，也就是0的位置是我,1的位置是爱，4的位置是中。从位置4开始取2个汉字，就是中国。中文编码一般是utf8格式


	//查找字符串  strpos(要处理的字符串, 要定位的字符串, 定位的起始位置[可选])

	$str = 'I want to study at duicode';
	$pos = strpos($str, 'duicode');
	echo $pos;

	echo "<br />";


	//替换字符串  str_replace(要查找的字符串, 要替换的字符串, 被搜索的字符串, 替换进行计数[可选])

	$str = 'I want to learn js';
	$replace = str_replace('js', 'php', $str);
	echo $replace;

	echo "<br />";


	//格式化字符串 sprintf(格式, 要转化的字符串)

	$str = '99.9';
	$result = sprintf('%01.2f', $str);
	echo $result;

	echo "<br />";

	//解释  这个 %01.2f 是什么意思呢？
	// 1、这个 % 符号是开始的意思，写在最前面表示指定格式开始了。 也就是 "起始字符", 直到出现 "转换字符" 为止，就算格式终止。
	// 2、跟在 % 符号后面的是 0， 是 "填空字元" ，表示如果位置空着就用0来填满。
	// 3、在 0 后面的是1，这个 1 是规定整个所有的字符串占位要有1位以上(小数点也算一个占位)。
	//     如果把 1 改成 6，则 $result的值将为 099.90
	//     因为，在小数点后面必须是两位，99.90一共5个占位，现在需要6个占位，所以用0来填满。
	// 4、在 %01 后面的 .2 （点2） 就很好理解了，它的意思是，小数点后的数字必须占2位。 如果这时候，$str 的值为9.234,则 $result的值将为9.23.
	//     为什么4 不见了呢？ 因为在小数点后面，按照上面的规定，必须且仅能占2位。 可是 $str 的值中，小数点后面占了3位，所以，尾数4被去掉了，只剩下 23。
	// 5、最后，以 f "转换字符" 结尾。


	//php字符串合并函数implode(分隔符[可选], 数组)

	$arr = array('Hello', 'World!');
	$result = implode('', $arr);
	print_r($result);

	echo "<br />";


	// php字符串分隔函数explode(分隔符[可选], 字符串)
	$str = 'apple,banana';
	$result = explode(',', $str);
	print_r($result);

	echo "<br />";


	// php字符串转义函数addslashes()
	$str = "what's your name?";
	echo addslashes($str);

