<?php 
	// PHP的GD库是用来处理图形的扩展库，通过GD库提供的一系列API，可以对图像进行处理或者直接生成新的图片。

	// PHP除了能进行文本处理以外，通过GD库，可以对JPG、PNG、GIF、SWF等图片进行处理。GD库常用在图片加水印，验证码生成等方面。

	header("content-type: image/png");
	$img=imagecreatetruecolor(100, 100);
	$red=imagecolorallocate($img, 0xFF, 0x00, 0x00);
	imagefill($img, 0, 0, $red);
	// imagepng($img);
	// imagedestroy($img);


	// 要对图形进行操作，首先要新建一个画布，通过imagecreatetruecolor函数可以创建一个真彩色的空白图片：

	$img = imagecreatetruecolor(500, 500);

	// GD库中对于画笔所用的颜色，需要通过imagecolorallocate函数进行分配，通过参数设定RGB的颜色值来确定画笔的颜色：

	$red = imagecolorallocate($img, 0xFF, 0x00, 0x00);

	// 然后我们通过调用绘制线段函数imageline进行线条的绘制，通过指定起点跟终点来最终得到线条。

	imageline($img, 0, 0, 200, 200, $red);

	// 线条绘制好以后，通过header与imagepng进行图像的输出。

	header("content-type: image/png");
	// imagepng($img);

	// 通过上面的步骤，可以发现PHP绘制图形非常的简单，但很多时候我们不只是需要输出图片，可能我们还需要得到一个图片文件，可以通过imagepng函数指定文件名将绘制后的图像保存到文件中。

	// imagepng($img, './img.png');

	// 最后可以调用imagedestroy释放该图片占用的内存。

	imagedestroy($img);


	// GD库可以进行多种图形的基本操作，常用的有绘制线条，背景填充，画矩形，绘制文字等。跟绘制线条类似，首先需要新建一个图片与初始化颜色。

	$img = imagecreatetruecolor(100, 100);
	$red = imagecolorallocate($img, 0xFF, 0x00, 0x00);

	// 然后使用imagestring函数来进行文字的绘制，这个函数的参数很多：imagestring ( resource $image , int $font , int $x , int $y , string $s , int $col )，可以通过$font来设置字体的大小，x,y设置文字显示的位置，$s是要绘制的文字,$col是文字的颜色。


	imagestring($img, 5, 0, 0, "Hello world", $red);
	header("content-type: image/png");
	// imagepng($img);
	imagedestroy($img);

	// 前面我们知道通过imagepng可以直接输出图像到浏览器，但是很多时候，我们希望将处理好的图像保存到文件，以便可以多次使用。通过指定路径参数将图像保存到文件中。


	$filename = 'img.png';
	// imagepng($img, $filename);

	// 使用imagepng可以将图像保存成png格式，如果要保存成其他格式需要使用不同的函数，使用imagejpeg将图片保存成jpeg格式，imagegif将图片保存成gif格式，需要说明的是，imagejpeg会对图片进行压缩，因此还可以设置一个质量参数。

	$filename = 'img.jpg';
// ​	imagejpeg($img, $filename, 80);

	$img = imagecreatetruecolor(100, 100);
	$red = imagecolorallocate($img, 0xFF, 0x00, 0x00);
	// imagestring($img, 5, 0, 0, "Hello world", $red);
	$filename = 'img1.png';
	//在这里将图片保存到$filename文件中
	// imagejpeg($img, $filename, 80);

	imagedestroy($img);
	if (file_exists($filename)) {
	    // echo '文件保存成功 <br />';
	}


	// 简单的验证码其实就是在图片中输出了几个字符，通过我们前面章节讲到的imagestring函数就能实现。
	// 但是在处理上，为了使验证码更加的安全，防止其他程序自动识别，因此常常需要对验证码进行一些干扰处理，通常会采用绘制一些噪点，干扰线段，对输出的字符进行倾斜、扭曲等操作。
	// 可以使用imagesetpixel绘制点来实现噪点干扰，但是只绘制一个点的作用不大，因此这里常常会使用循环进行随机绘制。
	for($i=0;$i<50;$i++) {
	  // imagesetpixel($im, rand(0, 100) , rand(0, 100) , $black); 
	  // imagesetpixel($im, rand(0, 100) , rand(0, 100) , $green);
	} 



	$img = imagecreatetruecolor(100, 40);
	$black = imagecolorallocate($img, 0x00, 0x00, 0x00);
	$green = imagecolorallocate($img, 0x00, 0xFF, 0x00);
	$white = imagecolorallocate($img, 0xFF, 0xFF, 0xFF);
	imagefill($img,0,0,$white);
	//生成随机的验证码
	$code = '';
	for($i = 0; $i < 4; $i++) {
	    $code .= rand(0, 9);
	}
	imagestring($img, 5, 10, 10, $code, $black);
	//加入噪点干扰
	for($i=0;$i<50;$i++) {
	  // imagesetpixel($img, rand(0, 100) , rand(0, 100) , $black); 
	  // imagesetpixel($img, rand(0, 100) , rand(0, 100) , $green);
	}
	//输出验证码
	// header("content-type: image/png");
	// imagepng($img);
	// imagedestroy($img);

	// 给图片添加水印的方法一般有两种，一种是在图片上面加上一个字符串，另一种是在图片上加上一个logo或者其他的图片。
	// 因为这里处理的是已经存在的图片，所以可以直接从已存在的图片建立画布，通过imagecreatefromjpeg可以直接从图片文件创建图像。

	$filename = 'img1.png';

	$im = imagecreatefrompng($filename);

	// 创建图像对象以后，我们就可以通过前面的GD函数，绘制字符串到图像上。如果要加的水印是一个logo图片，那么就需要再建立一个图像对象，然后通过GD函数imagecopy将logo的图像复制到源图像中。

	// $logo = imagecreatefrompng('img.png');
	// imagecopy($im, $logo, 15, 15, 0, 0, $width, $height);

	// 当将logo图片复制到原图片上以后，将加水印后的图片输出保存就完成了加水印处理。

	// imagejpeg($im, $filename);


	//这里仅仅是为了案例需要准备一些素材图片
	$url = 'http://www.iyi8.com/uploadfile/2014/0521/20140521105216901.jpg';
	$content = file_get_contents($url);
	$filename = 'tmp.jpg';
	file_put_contents($filename, $content);


	$url = 'http://wiki.ubuntu.org.cn/images/3/3b/Qref_Edubuntu_Logo.png';
	file_put_contents('logo.png', file_get_contents($url));


	//开始添加水印操作
	$im = imagecreatefromjpeg($filename);
	$logo = imagecreatefrompng('logo.png');
	$size = getimagesize('logo.png');
	imagecopy($im, $logo, 15, 15, 0, 0, $size[0], $size[1]); 
	 
	header("content-type: image/jpeg");
	imagejpeg($im, 'img2.jpg');

