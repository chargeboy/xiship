<?php 
	//今天该文件的操作了~
	// PHP具有丰富的文件操作函数，最简单的读取文件的函数为file_get_contents，可以将整个文件全部读取到一个字符串中。

	$content = file_get_contents('./test.txt');

	// file_get_contents也可以通过参数控制读取内容的开始点以及长度。

	$content = file_get_contents('./test.txt', null, null, 100, 500);

	// PHP也提供类似于C语言操作文件的方法，使用fopen，fgets，fread等方法，fgets可以从文件指针中读取一行，freads可以读取指定长度的字符串。

	// $fp = fopen('./text.txt', 'rb');
	// while(!feof($fp)) {
	//     echo fgets($fp); //读取一行
	// }
	// fclose($fp);

	// echo "<br />";

	// $fp = fopen('./text.txt', 'rb');
	// $contents = '';
	// while(!feof($fp)) {
	//     $contents .= fread($fp, 4096); //一次读取4096个字符
	// }
	// fclose($fp);

	// 使用fopen打开的文件，最好使用fclose关闭文件指针，以避免文件句柄被占用。

	// 一般情况下在对文件进行操作的时候需要先判断文件是否存在，PHP中常用来判断文件存在的函数有两个is_file与file_exists.


	$filename = './test.txt';
	if (file_exists($filename)) {
	    echo file_get_contents($filename);
	}

	echo "<br />";

	// 如果只是判断文件存在，使用file_exists就行，file_exists不仅可以判断文件是否存在，同时也可以判断目录是否存在，从函数名可以看出，is_file是确切的判断给定的路径是否是一个文件。

	$filename = './test.txt';
	if (is_file($filename)) {
	    echo file_get_contents($filename);
	}

	echo "<br />";

	// 更加精确的可以使用is_readable与is_writeable在文件是否存在的基础上，判断文件是否可读与可写。

	$filename = './test.txt';
	if (is_writeable($filename)) {
	    file_put_contents($filename, '我是老头啊');
	}
	if (is_readable($filename)) {
	    echo file_get_contents($filename);
	}

	echo "<br />";

	// 与读取文件对应，PHP写文件也具有两种方式，最简单的方式是采用file_put_contents。

	// $data参数可以是一个一维数组，当$data是数组的时候，会自动的将数组连接起来，相当于$data=implode('', $data);

	$filename = './test.txt';
	$data = 'test';
	file_put_contents($filename, $data);

	$fp = fopen('./test.txt', 'rb');
	echo fgets($fp);
	fclose($fp);

	echo "<br />";

	// 同样的，PHP也支持类似C语言风格的操作方式，采用fwrite进行文件写入。

	$fp = fopen('./test.txt', 'w');
	fwrite($fp, 'hello');
	fwrite($fp, 'world');
	fclose($fp);


	$fp = fopen('./test.txt', 'rb');
	echo fread($fp, 200);
	fclose($fp);

	echo "<br />";

	// 文件有很多元属性，包括：文件的所有者、创建时间、修改时间、最后的访问时间等。
	// fileowner：获得文件的所有者
	// filectime：获取文件的创建时间
	// filemtime：获取文件的修改时间
	// fileatime：获取文件的访问时间


	// 其中最常用的是文件的修改时间，通过文件的修改时间，可以判断文件的时效性，经常用在静态文件或者缓存数据的更新。

	$mtime = filemtime($filename);
	echo '修改时间：'.date('Y-m-d H:i:s', filemtime($filename));

	echo "<br />";

	$filename = './test.txt';
	echo '所有者：'.fileowner($filename).'<br>';
	echo '创建时间：'.filectime($filename).'<br>';
	echo '修改时间：'.filemtime($filename).'<br>';
	echo '最后访问时间：'.fileatime($filename).'<br>';

	//给$mtime赋值为文件的修改时间
	$mtime = time(); 
	//通过计算时间差 来判断文件内容是否有效
	if (time() - $mtime > 3600) {
	    echo '<br>缓存已过期';
	} else {
	    echo file_get_contents($filename);
	}

	echo "<br />";

	// 通过filesize函数可以取得文件的大小，文件大小是以字节数表示的。


	$filename = './test.txt';
	$size = filesize($filename);

	// 如果要转换文件大小的单位，可以自己定义函数来实现。

	function getsize($size, $format = 'kb') {
	    $p = 0;
	    if ($format == 'kb') {
	        $p = 1;
	    } elseif ($format == 'mb') {
	        $p = 2;
	    } elseif ($format == 'gb') {
	        $p = 3;
	    }
	    $size /= pow(1024, $p);   //pow() 函数返回 x 的 y 次方。
	    return number_format($size, 3);
	}


	$filename = './test.txt';
	$size = filesize($filename);

	$size = getsize($size, 'kb'); //进行单位转换
	echo $size.'kb';

	echo "<br />";

	// 值得注意的是，没法通过简单的函数来取得目录的大小，目录的大小是该目录下所有子目录以及文件大小的总和，因此需要通过递归的方法来循环计算目录的大小。


	// 跟Unix系统命令类似，PHP使用unlink函数进行文件删除。

	// unlink($filename);

	// 删除文件夹使用rmdir函数，文件夹必须为空，如果不为空或者没有权限则会提示失败。

	// rmdir($dir);

	// 如果文件夹中存在文件，可以先循环删除目录中的所有文件，然后再删除该目录，循环删除可以使用glob函数遍历所有文件。

	// foreach (glob("*") as $filename) {
	//    unlink($filename);
	// }

