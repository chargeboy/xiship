<?php  
	//php中的异常处理
	// PHP支持异常处理，异常处理是面向对象一个重要特性，PHP代码中的异常通过throw抛出，异常抛出之后，后面的代码将不会再被执行。
	// 既然抛出异常会中断程序执行，那么为什么还需要使用异常处理？
	// 异常抛出被用于在遇到未知错误，或者不符合预先设定的条件时，通知客户程序，以便进行其他相关处理，不至于使程序直接报错中断。
	// 当代码中使用了try catch的时候，抛出的异常会在catch中捕获，否则会直接中断。
	// 既然抛出异常会中断程序执行，那么为什么还需要使用异常处理？
	// 异常抛出被用于在遇到未知错误，或者不符合预先设定的条件时，通知客户程序，以便进行其他相关处理，不至于使程序直接报错中断。
	// 当代码中使用了try catch的时候，抛出的异常会在catch中捕获，否则会直接中断。
	// 1、基本语法
	        try{
	            //可能出现错误或异常的代码
	            //catch表示捕获，Exception是php已定义好的异常类
	        } catch(Exception $e){
	            //对异常处理，方法：
	                //1、自己处理
	                //2、不处理，将其再次抛出
	        }

	// 2、处理处理程序应当包括：
	// Try - 使用异常的函数应该位于 "try"  代码块内。如果没有触发异常，则代码将照常继续执行。但是如果异常被触发，会抛出一个异常。
	// Throw - 这里规定如何触发异常。注意：每一个 "throw" 必须对应至少一个 "catch"，当然可以对应多个"catch"
	// Catch - "catch" 代码块会捕获异常，并创建一个包含异常信息的对象。



	//创建可抛出一个异常的函数
	function checkNum($number){
	     if($number>1){
	         throw new Exception("异常提示-数字必须小于等于1");
	     }
	     return true;
	 }
	 
	//在 "try" 代码块中触发异常
	 try{
	     checkNum(2);
	     //如果异常被抛出，那么下面一行代码将不会被输出
	     echo '如果能看到这个提示，说明你的数字小于等于1';
	 }catch(Exception $e){
	     //捕获异常
	     echo '捕获异常: ' .$e->getMessage();
	 }


	 echo "<br />";

	//  PHP具有很多异常处理类，其中Exception是所有异常处理的基类。

	// Exception具有几个基本属性与方法，其中包括了：

	// message 异常消息内容
	// code 异常代码
	// file 抛出异常的文件名
	// line 抛出异常在该文件的行数

	// 其中常用的方法有：

	// getTrace 获取异常追踪信息
	// getTraceAsString 获取异常追踪信息的字符串
	// getMessage 获取出错信息

	// 如果必要的话，可以通过继承Exception类来建立自定义的异常处理类。

	 //自定义的异常类，继承了PHP的异常基类Exception
	class MyException extends Exception {
	    function getInfo() {
	        return '自定义错误信息';
	    }
	}

	try {
	    //使用异常的函数应该位于 "try"  代码块内。如果没有触发异常，则代码将照常继续执行。但是如果异常被触发，会抛出一个异常。
	    throw new MyException('error');//这里规定如何触发异常。注意：每一个 "throw" 必须对应至少一个 "catch"，当然可以对应多个"catch"
	} catch(Exception $e) {//"catch" 代码块会捕获异常，并创建一个包含异常信息的对象
	    echo $e->getInfo();//获取自定义的异常信息
	    echo $e->getMessage();//获取继承自基类的getMessage信息
	}


	echo "<br />";


	// 在了解了异常处理的基本原理之后，我们可以通过try catch来捕获异常，我们将执行的代码放在try代码块中，一旦其中的代码抛出异常，就能在catch中捕获。

	// 这里我们只是通过案例来了解try catch的机制以及异常捕获的方法，在实际应用中，不会轻易的抛出异常，只有在极端情况或者非常重要的情况下，才会抛出异常，抛出异常，可以保障程序的正确性与安全，避免导致不可预知的bug。

	// 一般的异常处理流程代码为：


	try {
	    throw new Exception('wrong');
	} catch(Exception $ex) {
	    echo 'Error:'.$ex->getMessage().'<br>';
	    echo $ex->getTraceAsString().'<br>';
	}
	echo '异常处理后，继续执行其他代码 <br />';



	// 在异常被捕获之后，我们可以通过异常处理对象获取其中的异常信息，前面我们已经了解捕获方式，以及获取基本的错误信息。

	// 在实际应用中，我们通常会获取足够多的异常信息，然后写入到错误日志中。

	// 通过我们需要将报错的文件名、行号、错误信息、异常追踪信息等记录到日志中，以便调试与修复问题。

	try {
	    throw new Exception('wrong');
	} catch(Exception $ex) {
	    $msg = 'Error:'.$ex->getMessage()."\n";
	    $msg.= $ex->getTraceAsString()."\n";
	    $msg.= '异常行号：'.$ex->getLine()."\n";
	    $msg.= '所在文件：'.$ex->getFile()."\n";
	    //将异常信息记录到日志中
	    file_put_contents('error.log', $msg);
	}