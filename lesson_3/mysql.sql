<?php

--复习 Mysql 的知识点，熟悉并掌握它--
	
/*创建数据库*/
	create database dbname;

	/*update和 alter的区别：
		update 是修改现存表里行中的值
		alter 是修改表的结构，如添加列，修改列的数据类型等
	*/
	
/*创建一个用户*/
	grant select, insert, update, delete, index, alter, create, drop
	on 数据库.*
	to 用户名 identified by '密码'

/*加载SQL文件*/
	>mysql -h 主机名 -u 用户名 -D 数据库 -p < xx.sql
	//当然也可以直接在 phpMyadmin 中直接导入文件
	
/*例子*/
	create table customers
	( customerid int unsigned not null auto-increment primary key,
		name char(10) not mull,
		address char(100) not null,
		city char(20) not null
	);
	//unsigned意思是只能是0或者一个正数。id是无符号的
	
/*使用数据库*/

	/*连接数据库*/
	function connect(){
		$link = mysql_connect('dbhost', 'user', 'password')
		or die("Error:".mysql_errno().":".mysql_error());
		mysql_set_charset(charset);
		mysql_select_db(database_name) or die("error massege");
		return $link;
	}
	/*插入数据*/
	function insert($keys, $array) {
		//array_keys返回数组中的所有键名
		$keys = implode(",", array_keys($array));
		$vals = implode(",", array_values($array))
		$sql = "insert into ".$table."(".$keys.") values(".$vals.")";
	}

	/*更新数据*/
	function update($table, $array, $where=null) {
		foreach ($array as $key => $value) {
			$value = mysql_real_escape_string($value);
			$keyAndvalueArr[] = "`".$key."`='".$value."'";
			$keyAndvalues = implode(",", $keyAndvalueArr);
			$sql = "update ".$table." set ".$keyAndvalues." where ".$where;
			$this->query($sql);
			//$result = mysql_query($sql);
		}
	}

	/*删除操作*/
	function delete($table, $where) {
		$where = $where==null ? null : "where ".$where;
		$sql = "delete from ".$table." where ".$where;
		$this->query($sql);
	}