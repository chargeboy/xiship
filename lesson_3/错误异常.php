<?php
	//了解php对错误异常的处理机制
	//使用如下方式处理异常
	try {
		//code go here
	throw new Exception('message', code);
	}
	catch (Exception $e) {
		//handle exception
	}
?>
	
<?php
	/**示例**/
	try {
		throw new Exception("A terrible error has occirred", 42);
		
	}
	catch (Exception $e) {
		echo "Exception". $e->getCode().":".$e->getMessage()."<br />".
		"in" .$e->getFile()." on line ". $e->getLine(). "<br />";
	}

	//getCode()---------返回错误信息的代码
	//getMessage()--------返回错误的消息
	//getFile()------返回错误的文件路径
	//getLine()-------错误在第几行
	//__toString()-------允许简单的显示一个Exception对象，并且给出以上所有方法可以提供的信息
?>


<!--用户自定义异常-->
<?php
	class fileOpenException extends Exception {
		function __toString(){
			return "fileOpenException". $this->getCode().":".$this->getMessage()."<br />".
		"in" .$this->getFile()." on line ". $this->getLine(). "<br />";
		}
	}

	class fileWriteException extends Exception {
		function __toString(){
			return "fileOpenException". $this->getCode().":".$this->getMessage()."<br />".
		"in" .$this->getFile()." on line ". $this->getLine(). "<br />";
		}
	}
?>
<!--如何处理-->
<?php
	try {
		if (!($fp = @fopen("$DOCUMENT_ROOT/../orders/orders.txt", 'ab'))) {
			throw new fileOpenException();
			
		}
		if (!fwrite($fp, string)) {
			throw new fileWriteException();
			
		}
	}
	catch (fileOpenException $feo) {
		echo "<p>******************</p>";
	}
	catch (Exception $e) {
		echo "<p>******************</p>";
	}
?>