<!--一个简单的 html 搜索页面-->
<!DOCTYPE html>
<html>
<head>
	<title>简单的对数据库进行搜索</title>
</head>
<body>
	<h1>简单的对数据库进行搜索</h1>

	<form action="results.php" method="post">
		选择搜索的类型：
		<select name="searchtype">
			<option value="author">作者</option>
			<option value="title">书名</option>
			<option value="isbn">书号</option>
		</select>
		<br />
		请输入搜索内容：
		<input type="text" name="searchterm" size="40" />
		<br />
		<input type="submit" name="submit" value="搜索" />
	</form>
</body>
</html>