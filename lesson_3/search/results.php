<!--搜索结果的页面-->
<!DOCTYPE html>
<html>
<head>
	<title>结果搜索</title>
</head>
<body>
	<h1>你的搜索结果如下：</h1>
<?php
	$searchtype = $_POST['searchtype'];
	//用 trim（）过滤掉开始或结束位置的空白字符
	$searchterm = trim($_POST['searchterm']);

	if (empty($searchtype) || empty($searchterm)) {
		echo "请选择搜索类型和搜索内容！";
		exit;
	}

	if (!get_magic_quotes_gpc()) {
		$searchtype = addslashes($searchtype);
		$searchterm = addslashes($searchterm);
	}

	// @ $db = new mysqli('localhost', 'bookorama', 'bookorama123', 'books');
	@ $db = mysqli_connect('localhost', 'bookorama', 'bookorama123', 'books');

	if (mysqli_connect_errno()) {
		echo "错误：无法连接数据库！";
	}

	$query = "select * from books where ".$searchtype."like '%".$searchterm."%'";

	// $result = $db->query($query);
	$result = mysqli_query($db, $query); 

	// $num_results = $result->num_rows;
	$num_results = mysqli_num_rows($result); //返回查询结果的行数

	echo "<p>共找到".$num_results."本书.</p>";

	for ($i=0; $i < $num_results; $i++) { 
		// $row = $result->fetch_assoc();
		$row = mysqli_fetch_assoc($result); //获取每一行并以数组返回
		echo "<p><strong>".($i+1).".书名: ";
		echo htmlspecialchars(stripslashes($row['title']));
		// htmlspecialchars, 特殊字符转换为HTML实体
		echo "</strong><br />作者：";
		echo stripslashes($row['author']);
		echo "<br />书号: ";
		echo stripslashes($row['isbn']);
		echo "<br />价格：";
		echo stripslashes($row['price']);
		echo "</p>";
		//使用 stripslashes 去掉转义字符的反斜杠
	}

	// $result->free();
	mysqli_free_result($result);

	// $db->close();
	mysqli_close($db);

?>
</body>
</html>