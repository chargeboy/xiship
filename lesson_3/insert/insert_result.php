<!--对 insert.php 的处理程序-->
<!DOCTYPE html>
<html>
<head>
	<title>添加书籍</title>
</head>
<body>
<h1>添加结果</h1>

<?php
	$isbn = $_POST['isbn'];
	$author = $_POST['author'];
	$title = $_POST['title'];
	$price = $_POST['price'];

	if (!$isbn || !$author || !$title || !$price) {
		echo "请填写完整的信息！";
		exit();
	}

	if (!get_magic_quotes_gpc()) {
		$isbn = addslashes($isbn);
		$author = addslashes($author);
		$title = addslashes($title);
		$price = doubleval($price);
	}

	@ $db = new mysqli('localhost', 'bookorama', 'bookorama123', 'books');

	if (mysqli_connect_errno()) {
		echo "错误：无法连接到数据库！";
		exit();
	}

	$query = "insert into books values('".$isbn."','".$author."', '".$title."', '".$price."')";

	$result = $db->query($query);

	if ($result) {
		echo $db->affected_rows."本书成功加入数据库！";
	} else {
		echo "发生错误，添加失败！";
	}

	$db->close();

?>
</body>
</html>
