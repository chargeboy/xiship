<?php
	//利用php面向对象的知识，创建一个简单的html页面。这是实现方法函数的页面！
class Page{
	//类页面的属性
	public $content;
	public $title = "页面标题";
	public $keywords = "book, games, well, play";
	public $buttons = array( '主页'=>"home.php",
														 '内容'=>"content.php",
														 '服务'=>"services.php",
														 '站点地图'=>"map.php"
		                    );
	//类页面的方法
	public function __set($name, $value){
		$this->$name = $value;
	}

	public function Display(){
		echo "<html>\n<head>\n";
		$this->DisplayTitle();
		$this->DisplayKeywords();
		$this->DisplayStyles();
		echo "</head>\n<body>\n";
		$this->DisplayHeader();
		$this->DisplayMenu($this->buttons);
		echo "$this->content";
		$this->DisplayFooter();
		echo "</head>\n</html>\n";
	}

	public function DisplayTitle(){
		echo "<title>".$this->title."</title>";
	}

	public function DisplayKeywords(){
		echo "<meta name=\"keywords\" content=\"".$this->keywords."\" />";
	}

	public function DisplayStyles(){
?>
	<style type="text/css">
	h1 { 
		color: white; font-size: 24pt; text-align: center; font-family: arial,sans-serif; 
	}
	.menu {
		color: white; font-size: 12pt; text-align: center; font-family: arial,sans-serif; font-weight: bold;
	}
	td {
		background: black;
	}
	p {
		color: black; font-size: 12pt; text-align: justify; font-family: arial,sans-serif;
	}
	p.foot {
		color: white; font-size: 9pt; text-align: center; font-family: arial,sans-serif; font-weight: bold;
	}
	</style>
<?php
	}
	public function DisplayHeader(){
?>
	<table width="100%" cellpadding="12" cellspacing="0" border="0">
	<tr bgcolor="white">
		<td align="left"><img src="logo.gif" /></td>
		<td><h1>页面头部</h1></td>
		<td align="right"><img src="logo.gif" /></td>
	</tr>
	</table>
<?php
	}
	public function DisplayMenu($buttons){/************************************************************************************************/
		//有点不明白这段的意思，这个while循环
		echo "<table width=\"100%\" bgcolor=\"white\" cellspacing=\"4\" cellpadding=\"4\">";
		echo "<tr>\n";
		$width = 100/count($buttons);
		// list — 把数组中的值赋给一些变量
		while (list($name, $url) = each($buttons)) {
			$this->DisplayButton($width, $name, $url, !$this->ISURLCurrentPage($url));
		}
		echo "</tr>\n";
		echo "</table>\n";
	}

	public function ISURLCurrentPage($url){
		//strpos(haystack, needle); 它返回的是目标关键字子字符串在被搜索字符串中的位置，但是在这里是什么回事呢？
		if (strpos($_SERVER['PHP_SELF'], $url) == false){
			return false;
		}else {
			return true;
		}
	}

	public function DisplayButton($width, $name, $url, $active = true){
		if ($active) {
			echo "<td width = \"".$width."%\">
			<a href=\"".$url."\">
			<img src=\"s-logo.gif\" alt=\"".$name."\" border=\"0\" /></a>
			<a href=\"".$url."\">
			<span class=\"menu\">".$name."</span></a></td>";
		} else {
			echo "<td width=\"".$width."%\">
			<img src=\"side-logo.gif\" />
			<span class=\"menu\">".$name."</span></td>";
		}
	}

	public function DisplayFooter(){
?>
	<table width="100%" bgcolor="white" cellpadding="12" border="0">
	<tr>
		<td>
			<p class="foot">&copy; 版权所有</p>
			<p class="foot"><a href = "">获取更多信息</a></p>
		</td>
	</tr>
	</table>
<?php
	}
}
?>