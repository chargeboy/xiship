<?php
	//熟悉并掌握面向对象的基本知识
	
/*类、属性和方法*/
	class classmate{

		//类的属性定义
		public $name = 'pan';
		public $number = '22';
		public $school = 'minda';

		//类的方法定义
		public function eat(){
			echo "eating\n";
		}
		public function play(){
			echo "playing\n";
		}
		public function sleep(){
			echo "sleeping";
		}
	}
/*使用类属性*/
	class classmate{
		public $attribute;
		function operation($param){
			$this->attribute = $param;
			echo "$this->attribute";
		}
	}
/*类的实例化*/
	$classmate = new classmate();
/*访问对象属性*/
	echo $classmate->name."<br />";
	echo $classmate->number."<br />";
/*使用对象方法*/
	$classmate->eat();
	$classmate->play();

/*构造函数*/
	/**
	 * 1. 构造函数在对象被实例化的时候自动调用
	 * 2. 构造函数通常用于初始化对象的属性值
	 * 3. $this是php里面的伪变量，表示对象自身
	 */
	function __construct($name, $number, $school){
		$this->name = $name;
		$this->number = $number;
		$this->school = $school;
		print $name.";".$number.";".$school."<br />";
	}
	$const = new classmate("pan", "22", "minda");
	echo $const->name."<br />";
/*析构函数*/
	/**
	 * 1. 析构函数在该对象不会再被使用的情况下自动调用
	 * 2. 对象变量被赋值为 NULL 的时候，调用析构函数
	 * 3. 用于清理程序中使用的系统资源
	 */
	function __destruct(){
		print "Destroying" .$this->name. "<br />";
	}
/*对象引用*/
	//引用操作符会产生一个新的引用
	$const = $const1;
	//使用& 的引用不会产生一个新的引用
	$const2 = &$const;

/*对象继承*/
	/*
	* 使用 extends 关键字说明
	* 1.定义人类
	* 2.People 继承人类
	* 3.PHP中类不允许同时继承多个父类，即单继承性
	* 4.子类中的对象可以访问父类的方法
	*/
	class Human{
		public $name;
		public $number;
		public $school;
		public function eat(){
			echo $this->name."'s eating ".$food."<br />";
		}
	}
	class People extends Human
	{
		
		function __construct($name, $number, $school)
		{
			$this->name = $name;
			$this->number = $number;
			$this->school = $school;
		}
	
	public function eat(){
		echo "string";
	}
	public function play(){
		echo "string";
	}
	}
	$human = new People("pan", "22", "minda");
	echo "$human->name.<br />";
	$human->eat();
	$human->play();
	$human->eat("rice");

/*访问控制*/
	/**	
	 * 1. public的类成员可以被自身、子类和其他类访问
	 * 2. protected的类成员只能被自身和子类访问
	 * 3. private的类成员只能被自身访问。不能被外部访问
	 */
	
/*Static(静态关键字)*/
	/**静态成员
	 * 1. 静态属性用于保存类的公有数据
	 * 2. 静态方法里面只能访问静态属性
	 * 3. 静态成员不需要实例化对象就可以访问
	 * 4. 类内部，可以通过 self 或者 static 关键字访问自身的静态成员
	 * 5. 可以通过 parent 关键字访问父类的静态成员
	 * 6. 可以通过类名称在外部访问类的静态成员
	 */
	
/*重写和Final关键字*/
	/**
	 * 1. 对于不想被子类重写的方法，可以在方法前加上 final 关键字
	 * 2. 子类中编写跟父类完全一致的方法可以完成对父类方法的重写
	 * 3. 对于不想被任何类继承的类可以在 class 之前加 final 关键字
	 */
/*数据访问*/
	/**
	 * 1. parent 关键字可以用于调用父类被重写的成员
	 * 2. self可以用于访问类自身的成员方法，也可以用于访问自身的静态成员和类常量; 不能用于访问类自身的属性; 访问常量时不用在常量名称前面加 $ 符号
	 * 3. static 关键字用于访问类自身定义的静态成员，访问静态属性时需要在属性名前面添加$符号
	 */
/*对象接口*/
	/**
	 * 1. 接口里面的方法没有具体的实现
	 * 2. 实现了某个接口的类必须实现接口定义中的所有的方法
	 * 3. 接口可以继承接口
	 * 4. 接口中定义的所有方法都必须是公有的
	 */
	interface Displayable{
		function display();
	}
	class webPage implements Displayable{
		function display(){
			//
		}
	}
/*抽象类*/
