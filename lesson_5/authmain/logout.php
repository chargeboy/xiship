<?php
	//该脚本注销会话变量并销毁会话
	session_start();

	//store to test if they were logged in
	$old_user = $_SESSION['valid_user'];
	unset($_SESSION['valid_user']);
	session_destroy();
?>
<html>
<body>
	<h1>Log out</h1>
<?php
	if (!empty($old_user)) {
		echo "Logged out.<br />";
	} else {
		//if they weren't logged in but come to this page somehow.
		echo "Your were not logged in, and so have not been logged out.<br />";
	}
?>

	<a href="authmain.php">Back to main page to  log in.</a>
</body>
</html>