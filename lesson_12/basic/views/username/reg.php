<?php
/**
 * Created by PhpStorm.
 * User: panchaozhi
 * Date: 2016/7/14
 * Time: 19:29
 */
use yii\helpers\Html;
use yii\widgets\ActiveForm;

//注意，在开始之前的表单中是没有任何输入的，所以事先是这个视图先被渲染
//提交之后，才渲染register视图

?>

<?php $form = ActiveForm::begin();?>

  <?=$form->field($model, 'username')->textInput(['autofocus'=>true])?>
  <?=$form->field($model, 'password')->passwordInput()?>
  <?=$form->field($model, 'email')?>

  <div class="form-group">
    <?=Html::submitButton('Submit', ['class'=>'btn btn-primary', 'name'=>'reg-button'])?>
  </div>

<?php $form = ActiveForm::end();?>
