<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Status */

$this->title = 'Create Status';
//Home/Statuses/Create Status这是头部上面的层级链接

$this->params['breadcrumbs'][] = ['label' => 'Statuses', 'url' => ['index']];

//在显示 $this->title即Create Status之前有一个链接Statuses指向index操作
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="status-create">

<!--    这里是输出一个内容为标题的h1-->
    <h1><?= Html::encode($this->title) ?></h1>

<!--    然后渲染 '_form' 视图-->
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
