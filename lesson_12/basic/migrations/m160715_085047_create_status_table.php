<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Handles the creation for table `status_table`.
 */
class m160715_085047_create_status_table extends Migration
{
    /**
     * @inheritdoc
     * up方法是我们后面执行 ./yii migrate/up 命令的时候触发的，这里一般负责创建一个表，我们可以将表的Schema写进这个方法里
     */
    public function up()
    {
      $tableOptions = null;
      if ($this->db->driverName === 'mysql') {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
      }
      $this->createTable('{{%status}}', [

          //我们的status表会有5个字段，其中id是主键，message和permissions就是我们上次输入的文本框和下拉列表的内容(不知道为啥我自己写创建不成功）

        'id' => Schema::TYPE_PK,
        'message' => Schema::TYPE_TEXT.' NOT NULL DEFAULT ""',
        'permissions' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 0',
        'created_at' => Schema::TYPE_INTEGER . ' NOT NULL',
        'updated_at' => Schema::TYPE_INTEGER . ' NOT NULL',
        ], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%status}}');
    }
}
