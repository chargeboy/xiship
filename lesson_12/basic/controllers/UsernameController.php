<?php
/**
 * Created by PhpStorm.
 * User: panchaozhi
 * Date: 2016/7/14
 * Time: 19:17
 */

namespace app\controllers;
use yii\web\Controller;
use app\models\Admin;

class UsernameController extends Controller{

  public function actionRegister(){
    $model = new Admin();
    if (\Yii::$app->request->post()) {

      //获取用户输入的值
      $model->username = \Yii::$app->request->post('username');
      $model->password = \Yii::$app->request->post('password');
      $model->email = \Yii::$app->request->post('email');

      //保存到数据库
      $model->save();
      //渲染视图
      return $this->render('register', ['model'=>$model]);
    } else {
      return $this->render('reg', ['model'=>$model]);
    }
  }
}