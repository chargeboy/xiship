<html>
<head>
	<title>uploading</title>
</head>
<body>
<h1>uploading file...</h1>
<?php
	//这是对 upload.html 页面的处理程序
	if ($_FILES['userfile']['error']>0) {
		echo "Problem:";
		switch ($_FILES['userfile']['error']) {
			case 1: echo "文件过大！";
							break;

			case 2: echo "文件超过了表单指定的最大值！";
							break;

			case 3: echo "文件只有部分被上传！";
							break;

			case 4: echo "没有任何文件上传！";
							break;
			case 6: echo "没有指定的临时目录！";							
							break;
			case 7: echo "文件写入磁盘失败！";
							break;
		}
		exit();
	}

	//does the file have the right MIMEtype?
	if ($_FILES['userfile']['type'] !='text/plain'){
		echo "Problem: file is not plain text";
		exit();
	}

	//put the file where we would like it
	$upfile = ''.$_FILES['userfile']['name'];

	//确保所处理的文件已经被上传，而且不是一个本地文件
	if (is_uploaded_file($_FILES['userfile']['tmp_name'])) {
		if (!move_uploaded_file($_FILES['userfile']['tmp_name'], $upfile)) {
			echo "Problem: Could not move file to destimation directiry";
			exit;
		}
	}
	else {
		echo "Problem: Possible file upload attack. Filename:";
		echo $_FILES['userfile']['name'];
		exit();
	}

	echo "file uploaded seccessfully<br><br>";

	//remove possible HTMl and PHP tags from the file's contents
	$contents = file_get_contents($upfile);

	$contents = strip_tags($contents);

	file_put_contents($_FILES['userfile']['name'], $contents);

	echo "<p>Preview of uploaded file contents:<br/><hr/>";
	echo nl2br($contents);
	echo "<br/><hr/>";
?>
</body>
</html>