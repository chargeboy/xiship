<?php
/**
 * 创建一个控制器
 * 控制器的请求处理 get, 和 post, ip, id, 
 * 控制器的响应处理 add, set, remove, location, redirect(), sendFile(), 
 * 控制器的 session 处理
 * 控制器的 cookie 处理
 *
 * 视图的创建
 * 视图的数据传递
 * 视图的数据安全
 * 视图的布局文件
 * 在视图中显示另外一个视图
 * 视图的数据块
 *
 *
 * 数据模型（表）
 * 数据库配置
 * 活动记录的创建
 * 单表查询
 * 单表删除
 * 单表添加数据
 * 单表数据修改
 * 关联查询
 * 关联查询性能问题
 * 总结
 * 
 * @authors Your Name (panchaozhigithub@github.com)
 * @date    2016-05-14 15:47:56
 * @version $Id$
 */
namespace app\controllers;
use yii\web\Controller;
use yii\web\Cookie;
use app\models\Test;

//E:\wamp\www\basic\vendor\yiisoft\yii2\web\Controller.php里的命名空间是 yii\web\Controller
class HelloController extends Controller {
    
    public function actionIndex(){//操作使用action来进行区分


    	// * 控制器的请求处理 get, 和 post, ip, id, 
    	// 
    	// $request = \YII::$app->request;*****************YII是全局变量要加个反斜杠 \
    	// echo $request->get('id', 'null');
    	// echo $request->post('name', 'null');
    	// if ($request->isGet) {
    	// 	  echo "THis is get method!";****************刷新的时候是get方法
    	// }
    	// echo $request->userIP;

    	//
    	//  控制器的响应处理 add, set, remove, location, redirect(), sendFile(), 
    	//  
    	// $res = \YII::$app->response;
    	// $res->statusCode = '404';
    	// $res->headers->add('pragma', 'no-cache');
    	// $res->headers->remove('pragma');
    	// $res->headers->add('location', 'http://www.baidu.com');
    	// $res->statusCode = '301';
      // echo "Hello, World!";
      // $this->redirect('http://www.baidu.com', 302);
      // $res->sendFile('./robots.txt');
      //
      //  
      //  * 控制器的 session 处理
      //  
   		// $session = \YII::$app->session;
   		// $session->open();
   		// if ($session->isActive) {
   		// 	echo "session is active!";
   		// }
   		// $session->set('user', 'panchaozhi');
   		// echo $session->get('user');
   		// $session->remove('user');
   		// $session['user'] = 'panchaozhiwtf';
   		// echo $session['user'];
   		// unset($session['user']);
   		// 查看session值在不同的浏览器是通过怎样的方式实现数据传输的。
   		// 即通过保存的文件 （在wamp/tmp里） 来实现的  		
   		// $session['user'] = 'panchaozhi good well';
   		// echo $session['user'];
   		// 
   		// 控制器的 cookie处理和使用
   		// $cookies = \YII::$app->response->cookies;
   		// $cookie_date = array('name'=>'uesr', 'value'=>'panchaozhiwtf');
   		// $cookies->add(new Cookie($cookie_date));
   		// $cookies->remove('uesr');
   		// 
   		// $cookies = \YII::$app->request->cookies;
   		// echo $cookies->getValue('uesr','不存在');
   		// 
   		// 
   		// echo "";
   		//
   		//
   		//
   		//
   		//
   		//
   		//
   		//
   		//
   		//
   		// return $this->renderPartial('index');
   		// 
   		// 
   		// 
   		//查询数据
   		//$id = '1 or 1=1';
   		// $sql = 'select * from test where id=:id';
   		// $results = Test::findBySql($sql, array(':id'=>'1 or 1=1'))->all(); 
   		// Test::find()->where(['id'=>1])->all();
   		// 
   		// // 查询结果转化为数组用 asArray();
   		// $results = Test::find()->where(['>','id',0])->asArray()->all();
   		// print_r($results);



   		//删除数据
   		//  $results = Test::find()->where(['>', 'id', 0])->all();
   		// $results[0]->delete();
   		// Test::deleteAll('id>:id', array(':id'=>0));
   		// 
   		// 
   		// 添加数据
      //
      //
      //
      //数据缓存**************************
      //获取缓存组件
      // $cache = \YII::$app->cache;
      //在缓存中写数据
      // echo $cache->add('key1', 'Hello, panchaozhi!');
      // echo $cache->add('key2', 'Hello, chaozhi!');
      //读缓存
      // 修改
      // $data = $cache->set('key1', 'Hello, World!');
      //删除
      // $cache->delete('key1');
      // 清空数据
      // $cache->flush('key1');
      // $data= $cache->get('key2');
      // 
      // 有效期设置
      // $cache->add('key1', 'Hello,World!', 15);
      // echo $cache->get('key1');
      // var_dump($data);
      // 
      // 缓存依赖关系
      // 文件依赖
      // 表达式依赖
      // 数据库依赖
      // 
      // 
      // 片段缓存***************************
      // return $this->renderPartial('index');
      // echo "4";
      // 
      // 
      // 使用gii 来新建模型和控制器
      $test = new Test();
      $test->id = 5;
      $test->title = 123123;
      $test->save();
      print_r($test->getErrors());
      
    }
    /*public function behaviors(){
      // echo "1";
      return[
        [
          'class'=>'yii\filters\PageCache',
          'duration'=>15,
          'dependency'=>[
              'class'=>'yii\caching\FileDependency',
              'filename'=>'tert.txt'
          ]
        ]
      ];
    }*/
}