
一、Cookie

Yii2的cookie主要通过yii\web\Request和yii\web\Response来操作的。
通过\YII::$app->response->getCookies()->add($cookie)来添加cookie
通过\YII::$app->request->cookies读取cookie

1. 添加一个cookie

方法一：
$cookie = new \yii\web\Cookie();
$cookie -> name = 'name';						//cookie名
$cookie -> value = 'value';					//cookie值
$cookie -> expire = time() * 3600;	//过期时间
$cookie -> httpOnly  = true;				//是否只读
\Yii::$app -> response->getCookies()->add($cookie);

方法二：
$cookie = new \Yii\web\Cookie([
				'name' => 'name',
				'value' => 'value',
				'expire' => time() +3600,
				'httpOnly' => true
]);
\Yii::$app->response->getCookies()->add($cookie);

2. 读取一个Cookie 
$cookie = \Yii::$app->request->cookies;
$cookie -> has('name');						//判断cookie是否存在
$cookie -> get('name');						//get()方法读取cookie
$cookie -> getValue('name');			//getValue9()方法读取cookie
$cookie -> count();								//获取cookie个数
$cookie -> getCount();

3. 删除一个cookie
$name = \Yii::$app->request->cookies->get('name');
\Yii::$app->response->getCookies()->remove($name);

4. 删除全部cookie
\Yii::$app->response->getCookies()->removeAll();


二、Session
yii2的session通过yii\web\Session实例的session应用组件来访问

$session = \Yii::$app->session;

1. 添加一个session
$session->set('name_string', 'value');
$session->set('name_array', [1,2,3]);

2. 读取一个session
$session->get('name_string');
$session->get('name_raaay');

3. 删除一个session
$session->remove('name_array');

4. 删除所有session
$session->removeAll();
