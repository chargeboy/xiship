
<!-- 2016年7月6日16:11:10 -->

二、Html助手和Request组件

1. Controller与Views关联的Html

	在@app\views\hello的index.php中：
	<?php
		use app\helpers\Html;
	?>

	表单：<?=Html::beginForm(提交地址，提交方法，属性数组);?>
	输入框：<?=Html::input(类型，name值，默认值，属性数组);?>
	表单类型Input：<?Html::表单类型Input(name值，默认值，属性数组);?>
	文本域：<?Html::textarea(name值，默认值，属性数组);?>
	按钮：<?Html::submitButton('提交按钮', ['class' => 'bn bn-primary']);?>
				<?=Html::endForm();?>

	在Hello控制器中注入视图：
	return $this->render('index');
	输出结果：url：localhost/web/index.php?hello/index
	参见views\hello\index.php

	更多如下：
	<?php //【一】表单：Html::beginForm(提交地址,提交方法,属性数组);?>

	<?=Html::beginForm('','post',['id'=>'form','class'=>'form','data'=>'myself']);?>

		<?php //【二】输入框：Html::input(类型,name值，默认值,属性数组);?>

		<?=Html::input('text','test','',['class' => 'form-control','placeholder'=>'hehe']);?>
		<?=Html::input('email','email','admin@admin.com',['class' => 'form-control']);?>
		<?=Html::input('password','pwd','',['class' => 'form-control']);?>
		<?=Html::input('hidden','hidden','',['class' => 'form-control']);?>

		<hr />
		<?php //Html::表单类型Input(name值，默认值，属性数组);?>

		<?=Html::textInput('test','hehe',['class' => 'form-control']);?>
		<?=Html::textInput('email','admin@admin.com',['class' => 'form-control']);?>
		<?=Html::passwordInput('pwd','',['class' => 'form-control']);?>
		<?=Html::hiddenInput('hidden','',['class' => 'form-control']);?>

		<hr />
		<?php //【三】文本域：Html::textarea(表单name，默认值，属性数组);?>
		<?=Html::textarea('area','',['class'=>'form-control','rows'=>'3']);?>

		<hr />
		<?php //【四】单选按钮：Html::radio(name值，是否选中，属性数组);?>
		<?=Html::radio('sex',true,['calss'=>'form-control']);?>
		<?php //单选按钮列表：Html:;radioList(name值，选中的值，键值对列表，属性数组); ?>
		<?=Html::radioList('height','1',['1'=>'160','2'=>'170','3'=>'180'],['class'=>'form-control']);?>

		<hr />
		<?php //【五】复选框：Html::checkbox(name值，是否选中，属性数组);?>
		<?=Html::checkbox('haha',true,['calss'=>'form-control']);?>
		<?php //单选按钮列表：Html:;checkboxList(name值，选中的值，键值对列表，属性数组); ?>
		<?=Html::checkboxList('xixi','1',['1'=>'160','2'=>'170','3'=>'180'],['class'=>'form-control']);?>

		<hr />
		<?php //【六】下拉列表：Html:;dropDownList(name值，选中的值，键值对列表，属性数组); ?>
		<?=Html::dropDownList('list','2',['1'=>'160','2'=>'170','3'=>'180'],['class'=>'form-control']);?>

		<hr />
		<?php //【七】控制标签Label：Html::label(显示内容，for值，属性数组); ?>
		<?=Html::label('显示的','test',['style'=>'color:#ff0000']);?>

		<hr />
		<?php //【八】上传控件：Html::fileInput(name值，默认值，属性数组); ?>
		<?=Html::fileInput('img',null,['class'=>'btn btn-default']);?>

		<hr />
		<?php //【九】按钮：; ?>
		<?=Html::button('普通按钮',['class'=>'btn btn-primary']);?>
		<?=Html::submitButton('提交按钮',['class'=>'btn btn-primary']);?>
		<?=Html::resetButton('重置按钮',['class'=>'btn btn-primary']);?>

	<?=Html::endForm();?>


2. 与Models关联的Html
	和生成普通的表单基本一样，只是需要在操作中对视图注入模型，视图中表单生成方式前面多了active

	在控制器Hello的index操作中注入视图和模型的数据：
	<?php
	namespace app\controllers;
	use yii\web\Controller;
	use app\models\Acticle;

	class HelloController extends Controller {
		public function actionIndex() {
			$model = Acticle::findOne(1);
			//model相当于views\hello\index.php下面的 $model,model的值为查询到的值 $model
			//render渲染会一起渲染 layouts
			return $this->render('article', ['model'] => $model);
		}
	}
	?>

	在@app\views\hello下创建article.php
	<?php
	use yii\helpers\Html;
	?>
	<?=Html::beginForm('', 'post', ['name' => 'article']);?>
		<?=Html::activeInput('text', $model, 'title', ['class' => 'form-control']);?>
		<?=Html::activeTextInput($model, 'title', ['class'] => 'form-control');?>
		<?=Html::activeTextArea($model, 'content', ['class'] => 'form-control');?>
		<?=Html::activeTextArea($model, 'desc', ['class'] => 'form-control');?>
		<?=Html::submitButton('提交按钮', ['class' => 'bn bn-primary']);
	<?=Html::endForm();?>

	使用submitButton提交按钮提交。

	在Hello控制器中判断打印一下看看是否接收到了参数：
	<?php
		if (\YII::$app->request->isPost) {
			Hearder('Content-Type:text/html; charset=utf-8');
			echo "<pre>";
			var_dump(\YII::$app->request->post());
			echo "</pre>";
			exit();
		}
	?>

	总结重要一点：
		(1). models目录下的 表名.php 文件应该是自动生成的，写代码的时候不需要用到这个，只要在controllers和views两个文件中进行操作.
		(2). views显示想要的视图，controllers写控制器操作。
		(3). 一个控制器在views下必有对应的文件名，如：HelloController在views下的文件名是hello，hello下写着要渲染的视图。
		(4). 关于URL：http://lcoalhost/web/index.php?r=控制器名/控制器操作。输出什么值主要看index操作里要渲染的是哪个视图。

3. Html的转义
	yii2提供的转义和反转义，使用\yii\helpers\Html::encode($html)和\yii\helpers\Html::decode($html);

4. 可以通过\yii\helpers\Html::error(模型，字段，属性数组);进行错误的输出。

5. 生成标签<?= Html::tag('p', Html::encode($user->name), ['class' => 'username']) ?>


三、Request组件

1. 一个就用请求是通过yii\web\Request对象来表示的，通过调用\YII::$app->requset来获取

2. Request对象的一些属性和方法：

	 1．request对象 			        				\Yii::$app->request
	 2．判断Ajax请求 		          				\Yii::$app->request->isAjax
	 3．判断POST请求    	        				\Yii::$app->request->isPost
	 4．获取浏览器信息		        				\Yii::$app->request->userAgent
	 5．获取客户端IP			 								\Yii::$app->request->userIp
	 6．读取所有get数据		        				\Yii::$app->request->get()
	 7．读取单个get数据           				\Yii::$app->request->get('r')
	 8．读取所有post数据		      				\Yii::$app->request->post()
	 9．读取单个post数据		      				\Yii::$app->request->get('post')
	 10．获取不包含host info的url部分			\yii::$app->request->url
	 11．获取整个URL			                \Yii::$app->request->absoluteUrl
	 12．获取host info部分								\Yii::$app->request->hostInfo;
	 13．入口脚本之后查询字符串之前				\Yii::$app->request->pathInfo
	 14．查询字符串			 									\Yii::$app->request->queryString
	 15．host info之后，入口脚本之前部分	\Yii::$app->request->baseUrl;