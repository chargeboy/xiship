
一、视图、模型、控制器

控制器

1. 控制器以[控制器名+Controller]命名,命名空间在app\controllers下。
2. 控制器操作是以[action+方法名]命名，操作id为say-hello 相当于操作名 actionSayHello.
3. 路由访问格式为[控制器名&操作id]; 控制器名为HelloController前的hello。
URL:Http://localhost/basic/web/index.php?r=hello/index,输出Hello World!

例子如下：
<?php
	namespace app\controllers;
	use yii\web\Controller;

	class HelloController extends Controller{
		public function actionIndex(){
			echo "Hello World!";
		}
	}
?>

4. 控制器的一些方法
	(1). 重定向 $this->redirect(['hello/index']);
	(2). 回到首页 $this->goHome();
	(3). 返回 $this->goBack();
	(4). 刷新当前页面 $this->refresh();
	(5). 渲染视图 $this->render(视图，注入视图及数组数据);
	(6). 渲染没有layout的视图 $this->renderPartial(视图，注入视图数组及数据);
	(7). 响应Ajax请求 $this->renderAjax(视图，注入视图数组及数据);

	例子如下：
	<?php
		namespace app\controllers;
		use yii\web\Controller;

		class HelloController extends Controller {
			function actionIndex() {
				echo "Hello World!";

				return $this->redirect(['hello/index']);

				return $this->goHome();

				return $this->goBack();

				return $this->refresh();
			}
		}

	?>



视图

1. 视图在views目录下，一个控制器id对应一个视图目录，HelloController的控制器id是hello，将对应@app\views\hello视图目录，视图由yii\web\View组件管理。
2. 在HelloController中调用return $this->render('index', ['data'=>[1,2,3]]);在@app\views\hello\index.php中获取Hello控制器注入的值。
3. index.php代码
	<?php
		foreach ($data as $values) {
			echo $values;
		}
	?>
	输出：1 2 3



模型

1. 模型在@app\models目录下。模型类名与模型文件名相同。Model一般继承两个yii2的类，一个是关联到数据库的yii\db\ActiveRecord,这个时候如果类名（文件名）不能与表直接对应，需要通过tableName()公共静态方法显示的指定表名，另一个是继承自yii\base\Model类。

2. 创建一个数据库yii2(创建数据库或表不成功以后直接用可视化方法来建)
	CREATE DATABASE yii2 DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;

	use yii2;

	创建数据表
	CREATE TABLE yii2(
		'id' INT UNSIGNED auto_increment primary key,
		'title' varchar(100) not null,
		'content' text not null,
		'desc' varchar(200)
	);

	插入数据
	insert into article('title', 'content', 'desc') values('...');
	insert into article('title', 'content', 'desc') values('...');

	配置数据库信息，在@app\config\db.php
	<?php
		return [
			'class' => 'yii\db\Connection',
			'dsn' => 'mysql:host=localhost; dbname=yii2',
			'username' => 'root',
			'password' => 'localhost',
			'charset' => 'utf8',
		];
	?>

	在@app\models下创建Article.php,类名继承自ActiveRecord,类名和表名一致，所以不用指定表名
	<?php
		namespace app\models;
		use yii\db\ActiveRecord;

		class Acticle extends ActiveRecord {

		}
	?>

	在@app\controller目录的Hello控制器下引入Article的命名空间
	use app\models\Article;

	在index操作中实例化Article:
	<?php
		...
		public function actionIndex() {
			$article = new Article();
			echo "<pre>";
			var_dump($article);
			echo "</pre>";
		}
	?>

	在Hello控制器中对article进行查询一条数据：
	<?php
		...
		public function actionIndex() {
			// $article = new Article();
			$article = Article::findOne(1);
			var_dump($article);
		}
	?>
	url访问：localhost/web/index.php?r=hello/index
	输出：
	object(app\models\Article)[62]
	  private '_attributes' (yii\db\BaseActiveRecord) => 
	    array (size=4)
	      'id' => string '1' (length=1)
	      'title' => string '测试标题1' (length=13)
	      'content' => string '测试内容1' (length=13)
	      'desc' => string '测试描述1' (length=13)
	  private '_oldAttributes' (yii\db\BaseActiveRecord) => 
	    array (size=4)
	      'id' => string '1' (length=1)
	      'title' => string '测试标题1' (length=13)
	      'content' => string '测试内容1' (length=13)
	      'desc' => string '测试描述1' (length=13)
	  private '_related' (yii\db\BaseActiveRecord) => 
	    array (size=0)
	      empty
	  private '_errors' (yii\base\Model) => null
	  private '_validators' (yii\base\Model) => null
	  private '_scenario' (yii\base\Model) => string 'default' (length=7)
	  private '_events' (yii\base\Component) => 
	    array (size=0)
	      empty
	  private '_behaviors' (yii\base\Component) => 
	    array (size=0)
	      empty

	<!-- 2016年7月5日23:08:33 -->
