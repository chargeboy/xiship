
一、配置缓存组件
在应用主体配置文件@app\config\web.php中的$config数组下的components数组中配置cache组件
'cache' => [
		'class' => 'yii\caching\FileCache',
]


二、缓存操作
$cache = \Yii::$app->cache;

1. 添加缓存
$cache->add('name', 'value');
$cache->madd(['nameone'=>'valueone', 'nametwo'=>'valuetwo']);

2. 读取缓存
$cache->get('name');
$cache->mget(['nameone', 'nametwo']);

3. 判断能在是否存在
$cache->exists('name');

4. 删除缓存
$cache->delete('name');
$cache->flush();