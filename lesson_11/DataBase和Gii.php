
2016年7月16日14:00:27

YII系列教程：DataBase And Gii

一、创建一个数据库
	1. 可以用phpMyAdmin或者Navicat for MySQL来创建，或者直接点使用命令行：CREATE DATABASE yii;
	2. 有了数据库之后，将我们的YII与数据库进行连接。Yii2的数据库配置文件在 basic/config/db.php中，打开它进行如下配置：
		<?php
			return [
				'class' => 'yii\db\Connection',
				'dsn' => 'mysql:host=localhost; dbname=yii',
				'username' => 'root',
				'password' => 'password',
				'charset' => 'utf8',
			];
		?>

二、创建Migration
	创建一个status表。打开Termimal命令控制台(phpstorm在左下角，或者快捷键Alt+F12)，执行如下命令：

	cd basic/(进入你项目的根目录)
	./yii migrate/create create_status_table

	过程中会有询问是否创建migrate,选择yes。完成之后，提示 New migration created successful
	这时会在目录在创建一个migrations/的目录，里面就有刚刚我们创建的migration文件，名字大概是这样的：m160715_085047_create_status_table.php

	我们打开这个文件，看看它长什么样：
	<?php
		use yii\db\Migration;

		class m160715_085047_create_status_table extends Migration{
			public function up() {
				//...(里面的东西好像我们用不到)
			}

			public function down() {
				echo "m160715_085047_create_status_table cannot be reverted.\n";
				return false;
			}
		}
		?>

	文件内容大概就是这样的，up()方法是我们后面执行./yii migrate/up 命令的时候触发的，这里一般负责创建一个表，我们可以将表的Schema写在这个方法里面。


三、创建status表

	有了上面的migrations之后，我们可以直接在up()方法写上我们的一些字段和配置
		<?php
			use yii\db\Migration;
			use yii\db\Schema; //记得一定要引用这个

			public function up() {
				$tableOptions = null;
				if ($this->db->driverName==='mysql') {
					$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
				}

				$this->createTable('{{%status}}', [
				              'id' => Schema::TYPE_PK,
				              'message' => Schema::TYPE_TEXT.' NOT NULL DEFAULT ""',
				              'permissions' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 0',
				              'created_at' => Schema::TYPE_INTEGER . ' NOT NULL',
				              'updated_at' => Schema::TYPE_INTEGER . ' NOT NULL',
				          ], $tableOptions);
			}

			public function down() {
				$this->dropTable('{{%status}}');
			}
		?>
	这里我们的status表会有5个字段，id为主主键，message和permissions就是上一次我们的文本输入框的内容，这里我们还加了两个字段，create_at和update_at
	down()方法和up()相对应，用来删除数据表的。

	有了表的schema之后，我们就可以来执行我们的migrate命令了，命令行执行：
	./yii migrate/up
	过程中还是选择yes。然后我们到数据库去看我们的表了，新建的表有migrations和status表。


四、使用Gii
	通过gii来造成我们的Model和Controller等文件
	访问gii：localhost/basic/web/index.php?r=gii
	选择Model Generator，在Table Name输入框输入status
	点击正文的Preview按钮， 如果已经存在同名的文件，选择overwrite，然后点击Generate按钮

	这时候打开models/Status.php,你会看到Yii会根据我们的数据表生成的验证规则和表单属性：
		<?php
		namespace app\models;
		use Yii;
		/**
		 * This is the model class for table "status".
		 *
		 * @property integer $id
		 * @property string $message
		 * @property integer $permissions
		 * @property integer $created_at
		 * @property integer $updated_at
		 */
		class Status extends \yii\db\ActiveRecord{
			 /**
			     * @inheritdoc
			     */
			    public static function tableName()
			    {
			        return 'status';
			    }

			    /**
			     * @inheritdoc
			     */
			    public function rules()
			    {
			        return [
			            [['message', 'created_at', 'updated_at'], 'required'],
			            [['message'], 'string'],
			            [['permissions', 'created_at', 'updated_at'], 'integer']
			        ];
			    }

			    /**
			     * @inheritdoc
			     */
			    public function attributeLabels()
			    {
			        return [
			            'id' => 'ID',
			            'message' => 'Message',
			            'permissions' => 'Permissions',
			            'created_at' => 'Created At',
			            'updated_at' => 'Updated At',
			        ];
			    }
			}
		}
		?>

	Status模型造成好之后，该为它造成相应的视图的控制器了，这时候需要使用到的是Gii的CRUD Generator了，访问地址：localhost/basic/web/index.php?r=gii/crud

	在这个页面上，分别填上相应的数据
	Model Class: app\models\Status
	Search Model Class: app\models\StatusSearch
	Controller Class: app\controllers\StatusController
	View Path: 可以直接留空，默认就是app\views\ControllerID

	