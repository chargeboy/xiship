
四、URL助手

1. 一些方法

在Hello控制器下新建一个actionUrl操作

	//不带域名根目录
	//echo Url::base();

	//带域名的根目录
	//echo Url::base(true);

	//不带域名的首页
	//echo Url::home();

	//带域名的首页
	//echo Url::home(true);

	//当前url
	//echo Url::current();



五、分页组件

1. 我们往 article表中插入更多的数据
INSERT  INTO `yii2`.`article`(`title`,`desc`,`content`) VALUES('测试标题3','测试描述3','测试内容3');
INSERT  INTO `yii2`.`article`(`title`,`desc`,`content`) VALUES('测试标题4','测试描述4','测试内容4');
INSERT  INTO `yii2`.`article`(`title`,`desc`,`content`) VALUES('测试标题5','测试描述5','测试内容5');
INSERT  INTO `yii2`.`article`(`title`,`desc`,`content`) VALUES('测试标题6','测试描述6','测试内容6');
INSERT  INTO `yii2`.`article`(`title`,`desc`,`content`) VALUES('测试标题7','测试描述7','测试内容7');
INSERT  INTO `yii2`.`article`(`title`,`desc`,`content`) VALUES('测试标题8','测试描述8','测试内容8');
INSERT  INTO `yii2`.`article`(`title`,`desc`,`content`) VALUES('测试标题9','测试描述9','测试内容9');
INSERT  INTO `yii2`.`article`(`title`,`desc`,`content`) VALUES('测试标题10','测试描述10','测试内容10');
INSERT  INTO `yii2`.`article`(`title`,`desc`,`content`) VALUES('测试标题11','测试描述11','测试内容11');
INSERT  INTO `yii2`.`article`(`title`,`desc`,`content`) VALUES('测试标题12','测试描述12','测试内容12');
INSERT  INTO `yii2`.`article`(`title`,`desc`,`content`) VALUES('测试标题13','测试描述13','测试内容13');
INSERT  INTO `yii2`.`article`(`title`,`desc`,`content`) VALUES('测试标题14','测试描述14','测试内容14');
INSERT  INTO `yii2`.`article`(`title`,`desc`,`content`) VALUES('测试标题15','测试描述15','测试内容15');


2. 在actionUrl操作中通过db查询和\yii\data\Pagination组织分页信息
//AR构建db查询

	controller action
	<?php
		use \YII\data\Pagination;
		$article = Article::find();
		$articleCount = clone $article;
		$pageSize = 4;
		$pages = new Pagination(
				[
					'totalCount' => $articleCount->count(),
					'pageSize' => $pageSize
		    ]		
		  );

		$models = $article -> offset($pages -> offset)
							-> limit($pages -> limit)
							-> all();

		return $this->render('url', 
				[
					'models' => $models,
					'pages' => $pages
				]
			);
	?>

	在@app\views\hello下新建一个url.php

	View
	<?php
		use \yii\widgets\LinkPager;
		//循环展示数据
		foreach ($models as $model) {
			//...
			echo $model -> title;
		}
		//显示分页页码
		echo LinkPager::widgets(
				[
					'pagination' => $pages,
					'nextPageLabel' => '下一页',
					'prevPageLabel' => '上一页',
					//......等等，选自己喜欢的样式就行
				]
			);
	?>

3. 定义分页的属性

	自带的分页类都可以定义哪些属性

	首先我们说说LinkPager组件

	pagination参数必填，这个是我们Pagination类的实例
	默认分页类是下面这个样子的:
	（<<1 2 3 4 5 6 7 8 9 10>>）

	上下页按钮以及10个按钮
	首先，我们把上下页的按钮修改成中文
	<?= LinkPager::widget([ 
	    'pagination' => $pages, 
	    'nextPageLabel' => '下一页', 
	    'prevPageLabel' => '上一页', 
	]); ?>
	如果你不想要显示上下页，可以将prevPageLabel和nextPageLabel设置为false
	<?= LinkPager::widget([ 
	    'pagination' => $pages, 
	    'nextPageLabel' => false, 
	    'prevPageLabel' => false, 
	]); ?>
	默认不显示首页也尾页，如果你需要，可以这样设置
	<?= LinkPager::widget([ 
	    'pagination' => $pages, 
	    'firstPageLabel' => '首页', 
	    'lastPageLabel' => '尾页', 
	]); ?>
	如果你的数据过少，不够2页，默认不显示分页，如果你需要，设置hideOnSinglePage=false即可
	<?= LinkPager::widget([ 
	    'pagination' => $pages, 
	    'hideOnSinglePage' => false, 
	]); ?>
	默认显示的页码为10页，可以设置maxButtonCount为你想要展示的页数
	<?= LinkPager::widget([ 
	    'pagination' => $pages, 
	    'maxButtonCount' => 5, 
	]); ?>
	有些人不喜欢默认的样式，想要分页带上自己的样式，可以设置options，不要忘了自行实现pre,next,disabled等样式
	<?= LinkPager::widget([ 
	    'pagination' => $pages, 
	    'options' => ['class' => 'm-pagination'], 
	]); ?>
	接下来我们谈谈Pagination组件

	默认的分页路由是下面这样子的，我们看看能做点什么

	r=controller/action?page=2&per-page=20
	(这里的per-page=其实就是前面设定的$pageSize)

	首先，我们是必须要指定总条数totalCount的，没这个参数，分页也是没办法实现的
	$pages = new Pagination([ 
	    'totalCount' => $totalCount, 
	]);
	默认分页的数量是20，你可以设置pageSize为你想要的
	$pages = new Pagination([ 
	    'totalCount' => $totalCount, 
	    'pageSize' => 5, 
	]);
	从上面的分页路由我们可以看到，默认带的有每页的数量per-page 如果你不想显示该参数，设置pageSizeParam=false就好
	$pages = new Pagination([ 
	    'totalCount' => $totalCount, 
	    'pageSizeParam' => false, 
	]);
	我们也可以看到，默认的页面取决于参数page,如果你想改变该参数为p,设置pageParam=p就好
	$pages = new Pagination([ 
	    'totalCount' => $totalCount, 
	    'pageParam' => 'p', 
	]);
	如果你的分页存在于首页，相信你肯定想要/?p=1而不是/site/index?p=1，我们看看怎么隐藏掉路由
	$pages = new Pagination([ 
	    'totalCount' => $totalCount, 
	    'route' => false, 
	]);
	可能你会发现分页类Pagination有一个bug,假如我们只有1页的数据，但是手动更改地址栏的page=20的时候，也会显示page=1的数据？当然，这在大部分接口API中就很让人厌烦。但是，这并非bug,而是一种友好的验证。设置validatePage=false即可避免掉该问题
	$pages = new Pagination([ 
	    'totalCount' => $totalCount, 
	    'validatePage' => false, 
	]);

