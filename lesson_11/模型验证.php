
模型验证

1. 模型的load方法
	模型对象的load方法为模型加载数据，一般地，模型尝试从$_POST搜集用户提交的数据，由Yii的 yii\web\Request::post()方法负责搜集。

	另外load加载国的字段必须在模型的rules方法里，不然也不能赋值。

2. 模型rules常用的验证规则
	
	1.【safe     不验证规则】
	['字段','safe']
	[['字段1','字段2',……],'safe']

	2.【required 不能为空，必须验证】
	['字段','required','message' => '提示信息']
	[['字段1','字段2',……],'required','message' => '提示信息']

	3.【compare  对比验证】
	['字段','compare','compareValue'=>'对比的值','message' => '提示信息']
	['字段','compare','compareAttribute'=>'对比的字段','message' => '提示信息']

	4.【double   双精度数字验证】
	['字段','double','min'=>'最小值','max' => '最大值','tooSmall'=>'小于最小值提示','tooBig'=>'大于最大值提示','message'=>'提示信息']

	5.【email    邮箱规则验证】
	['字段','email','message' => '提示信息']

	6.【in       范围验证】
	['字段','in','range'=>['1','2','3',……],'message' => '提示信息']

	7.【integer  整型数字验证】
	['字段','integer','message' => '提示信息']

	8.【match    正则验证】
	['字段','match','parttern'=>'#正则#','message' => '提示信息']

	9.【string   字符串验证】
	['字段','string','min'=>'最小长度','max' => '最大长度','tooShort'=>'小于最小长度提示','tooLong'=>'大于最大长度提示','message'=>'提示信息']

	10.【unique  唯一验证】
	['字段','unique','message' => '提示信息']

	11.【captcha 验证码验证】
	['字段','captcha','captchaAction',=>'login/captcha','message' => '提示信息']

	12.自定义验证
	['字段','自定义方法']
	可通过在自定义方法里调用addError()来定义错误
	例:
	['username',function($attribute,$params){
	    if(满足条件){
	        $this->addError($attribute,'提示信息');
	    }    
	},'params'=>['message'=>'dd']]


3. 在@app\models下创建一个Hello命名的模型文件
	<?php
		namespace app\models;
		use yii\base\Model;

		class Hello extends Model {
			public $username;
			public $password;

			public function rules() {
				return 
					[
						['username' 'required', 'message' => '用户名不能为空'],
						['password', 'required', 'message' => '密码不能为空']
					];
			}
		}

	?>

	在Hello控制器下新建一个actionValidate操作来测试，通过模型的validate()方法在幕后执行验证操作:
	<?php
		public function actionValidate() {
			//模拟数据，数组的一维必须是相关模型名
			//手动固定值用于测试数据
			$data = [
				'Hello' => [
					'username' => '12345',
					'password' => 'asdggg'
				]
			];

			$hello = new \app\models\Hello();
			//load方法为模型加载数据，从$_POST那里获取数据。hello对象去调用这个数据
			$hello -> load($data);
			if ($data && $hello -> validate()) {
				echo "ok!";
			} else {
				var_dump($hello -> errors;
			}
		}
	?>

