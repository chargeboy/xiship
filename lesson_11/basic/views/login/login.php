<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = '登录';
?>

<h1><?=Html::encode($this->title)?></h1>
<h3>请填写登录信息：</h3>
<?php $form = ActiveForm::begin();?>

  <?=$form->field($model, 'username');?>
  <?=$form->field($model, 'password')->passwordInput();?>
<?=Html::submitButton('登录')?>
<?php ActiveForm::end();?>