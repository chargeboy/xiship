<?php
/**
 * Created by PhpStorm.
 * User: panchaozhi
 * Date: 2016/7/12
 * Time: 12:38
 */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title='注册';
$this->params['breadcrumbs'][] = $this->title;
?>

<h1><?=Html::encode($this->title)?></h1>
<p>请填写以下信息完成注册:</p>

<?php $form = ActiveForm::begin([
  'id' => 'form-signup',
  'enableAjaxValidation' => true,
  'enableClientValidation' => true
]);?>

  <?= $form->field($model, 'username')?>
  <?= $form->field($model, 'email')?>
  <?= $form->field($model, 'password')->passwordInput()?>
  <?= $form->field($model, 'password_compare')->passwordInput()?>

  <?=Html::submitButton('注册', ['class' => 'btn btn-primary', 'name' => 'signup-btn'])?>


<?php $form = ActiveForm::end();?>
