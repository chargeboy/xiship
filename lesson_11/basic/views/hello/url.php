<?php
//这是实现分页的视图文件，引用组件 LinkPager和Pagination
use yii\widgets\LinkPager;
?>


<?php
  foreach ($models as $model) {
    echo $model -> title;
    echo "<br />";
    }
?>

<?=LinkPager::widget(
  [
    'pagination' => $pages,
    'nextPageLabel' => '下一页',
    'prevPageLabel' => '上一页',
    'firstPageLabel' => '首页',
    'lastPageLabel' => '尾页',
//    'maxButtonCount' => 2,
  ]
);?>
