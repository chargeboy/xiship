<?php
use \yii\helpers\Html;
?>

<?=Html::beginForm('#', 'post')?>

  <?=\yii\captcha\Captcha::widget([
  'model' => $model,
  'attribute' => 'code',
  'captchaAction' => 'code/captcha',
  'template' => '{input}{image}',
  'options' => [
    'class' => 'form-control',
    'id' => 'verify'
  ],
  'imageOptions' => [
    'class' => 'image-code',
    'image' => '点击图片刷新'
  ]
]);
?>
<?=Html::submitButton('验证', ['class' => 'btn btn-primary'])?>
<?=Html::endForm()?>
