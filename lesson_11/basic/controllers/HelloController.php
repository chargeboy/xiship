<?php

//设定一个命令空间
namespace app\controllers;

//Controller引用来自 Controller.php里的Controller类，该类的命令空间是yii\web
use app\models\Article;
use yii\base\Model;
use yii\db\Query;
use yii\web\Controller;
use yii\data\Pagination;
use app\models\Hello;

//URL访问地址：hostname/web/index.php?控制器名&操作(action开头)
class HelloController extends Controller{
  public function actionIndex(){

//    echo "Hello World!";

//    $request = \YII::$app->request;
//    echo $request->get('id', 20);
    /*if ($request->isGet) {
      echo "this is get method!";
    }*/
//    echo $request->userIP;

//    $response = \YII::$app->response;
//    $response->statusCode=404;

//    $cache = \YII::$app->cache;
//    $cache->add("key1", "Hello, World!");
//    $res = $cache->get('key1');
//    echo $res;

//    return $this->render('index', ['data' => [1,2,3]]);

//    $article = new Article();
//    echo "<pre>";
//    var_dump($article);
//    echo "</pre>";

//    $article = new Article();
//    $article = Article::findOne(1);
//    var_dump($article);

//    return $this->render('index');


//    if (\YII::$app->request->isPost) {
//      echo "<pre>";
//      var_dump(\YII::$app->request->post());
//      echo "</pre>";
//      exit();
//    }
//    $model = Article::findOne(1);
    //这个model就是views里面的$model,model的值为$model
    //render可以渲染views下的不同视图，并且会渲染layouts视图
//    return $this->render('article', ['model' => $model]);

//    return $this->render('say-index');
  }
  
  public function actionUrl() {
    //实现分页的操作
    
    $article = Article::find();
    $totalCount = clone $article;
    $pageSize = 3;
    $pages = new Pagination(
      [
        'totalCount' => $totalCount->count(),
        'pageSize' => $pageSize
      ]
    );
    
    $models = $article->offset($pages->offset)
            ->limit($pages->limit)
            ->all();
    
    return $this->render('url', 
      [
        'models' => $models,
        'pages' => $pages
      ]
    );
  }

  function actionValidate() {
    $data = ['Hello' =>
      [
      'username' => 'ssg',
      'password' => 'qwetstgga'
      ]
    ];

    $hello = new Hello();
    $hello->load($data);
    if ($data && $hello->validate()) {
      echo "ok！";
    } else {
      var_dump($hello->errors);
    }
  }

  function actionDb() {

    //查询状态为1的所有条数
//    $models = Article::findAll(['status' => 1]);
//    echo count($models);

    //id为1的
//    $model = Article::find()->where(['>', 'id', '5'])->asArray()->All();
//    print_r($model);

    //查询状态为1的所有数据
//    $model = Article::find(['status' => 1])->asArray()->All();
//    print_r($model);

    //查询状态等于1的数据并且根据 id 排序
//    $model = Article::find()->where(['status'=>1])->orderBy('id DESC')->asArray()->all();
//    print_r($model);

    //查询状态等于1的数据并根据 id排序，从第十行开始，取4条数据
//    $model = Article::find()->where(['status' => 1])->orderBy('id ASC')->offset(10)->limit(4)->asArray()->all();
//    print_r($model);

    //save()方法的第一个参数布尔值表示插入或更新时是否开启验证，默认是true
//    $article = Article::findOne(1);
//    $article -> title = '更改测试标题1';
//    $article -> save();

    //更新指定
//    $article = Article::updateAll(['title' => '测试标题1指定的跟新'], ['id' => 1]);
//    print_r($article);

    //添加一条数据
//    $article = new Article();
//    $article -> title = '添加一条标题';
//    $article -> content = '添加一个内容';
//    $article -> desc = '添加一个描述';
//    $article -> save();

    //删除一条数据
//    Article::findOne(16)->delete();

    //删除指定数据
//    Article::deleteAll(['id' => 15]);
    
    //查询一条id为2的数据
//    $db = new \yii\db\Query();
//    $model = $db->select('id, title, content, desc')->from('article')->where('id = :id', [':id' => 2]) -> one();
//    print_r($model);

    //查询多条语句
    //注意select不能写成select('id', 'title', 'content')这样只会显示出id的值
//    $db = new Query();
//    $models = $db -> select('id, title, content') -> from('article') -> where(['status' => 1]) -> all();
//    print_r($models);

    //根据id排序，从第10行开始，取4条数据
//    $db = new Query();
//    $models = $db -> select('id, title, content') -> from('article') -> orderBy('id DESC') -> offset(10) -> limit(4) -> all();
//    print_r($models);

    //统计查询
//    $db = new Query();
//    $models = $db -> select('title') -> from('article') -> count();
//    print_r($models);

    //查询一条数据,id为1的数据
//    $db = \YII::$app -> db;
//    $model = $db -> createCommand('SELECT * FROM  `article`') ->queryOne();
//    print_r($model);

    //绑定单个防SQL注入参数
//    $db = \YII::$app -> db;
//    $model = $db -> createCommand('SELECT * FROM `article` where id=:id') -> bindValue(':id', 2) -> queryOne();
//    print_r($model);

    //绑定防多个SQL注入参数
//    $db = \YII::$app -> db;
//    $models = $db -> createCommand('SELECT * FROM `article` where id=:id AND status=:status') -> bindValues(['id' => 3, 'status' => 1]) -> queryOne();
//    print_r($models);

    //查询多条语句
//    $db = \YII::$app -> db;
//    $models = $db -> createCommand('SELECT * FROM `article`') -> queryAll();
//    print_r($models);

    //统计查询
//    $db = \YII::$app ->db;
//    $model = $db -> createCommand('SELECT COUNT("id") FROM  `article`')->queryScalar();
//    print_r($model);

    //更新数据
//    $db = \YII::$app->db;
//    $model=$db->createCommand()->update('article', ['status'=>0], 'id=:id', ['id'=>9])->execute();
//    print_r($model);

    //插入语句
//    $db = \YII::$app->db;
//    $db -> createCommand() -> insert('article', ['title'=>'ceshiyicia', 'content'=>'ceshineirong', 'desc'=>'ceshimiaoshu']) -> execute();

    //一次插入多行
//    $db = \YII::$app->db;
//    $db->createCommand()->batchInsert('article', ['title', 'content', 'desc'], [
//      ['测试16','neiron16', 'maioshu16'],
//      ['测试17','neiron18', 'maioshu17'],
//      ['测试18','neiron18', 'maioshu18']
//    ])->execute();

    //删除数据
//    $db = \YII::$app->db;
//    $db->createCommand()->delete('article', 'status= 0')->execute();




  }
}