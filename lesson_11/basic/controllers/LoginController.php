<?php
namespace app\controllers;

use app\models\LoginForm;
use app\models\User;
use yii\web\Controller;

class LoginController extends Controller {
  
  public function actionLogin() {
    
    $model = new User();
    //$model怎么来？
    return $this->render('login', ['model' => $model]);
  }
  
  public function actionSignup() {
    $model = new SignupForm();
    $model->load($_POST);

    if (\Yii::$app->request->isAjax) {
      \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
      return \yii\bootstrap\ActiveForm::validate($model);
    }

    if ($model->load(\Yii::$app->request->post())) {
      if ($user = $model->signup()) {
        if (\Yii::$app->getUser()->login($user)) {
          return $this->goHome();
        }
      }
    }

    return $this->render('signup', ['model' => $model]);
  }
}