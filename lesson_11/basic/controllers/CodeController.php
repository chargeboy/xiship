<?php
//生成验证码的控制器

namespace app\controllers;
use app\models\Code;
use yii\web\Controller;

class CodeController extends Controller {
  
  public function actions() {
    return [
      'captcha' => [
        'class' => 'yii\captcha\captchaAction',
        'maxLength' => 4,
        'minLength' => 4,
        'width' => 80,
        'height' => 40
      ] 
    ];
  }
  
  public function actionVerifyCode() {
    $model = new Code();
    if (\Yii::$app->request->isPost && $model->load(\Yii::$app->request->post())) {
      if ($model->validate()) {
        echo "验证成功!";
      } else {
        var_dump($model->getErrors());
      }
    }
    return $this->render('verifycode', ['model' => $model]);
  }
}