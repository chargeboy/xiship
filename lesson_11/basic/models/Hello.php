<?php
namespace app\models;
use yii\base\Model;

class Hello extends Model {
  public $username;
  public $password;

  public function rules() {
    return [
      ['username', 'required', 'message' => '用户名不能为空'],
      ['password', 'required', 'message' => '密码不能为空']
    ];
  }
}