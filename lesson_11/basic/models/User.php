<?php

namespace app\models;

class User extends /*\yii\base\Object*/ \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    public $id;
    public $username;
    public $password;
    public $authKey;
    public $accessToken;

    /*private static $users = [
        '100' => [
            'id' => '100',
            'username' => 'admin',
            'password' => 'admin',
            'authKey' => 'test100key',
            'accessToken' => '100-token',
        ],
        '101' => [
            'id' => '101',
            'username' => 'demo',
            'password' => 'demo',
            'authKey' => 'test101key',
            'accessToken' => '101-token',
        ],


    ];*/

    /**
     * @inheritdoc
     * @return string
     */
    public static function tableName() {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules(){
        return [
            [['username', 'password'], 'required'],
            [['username'], 'string', 'max'=>50],
            [['password'], 'string', 'max'=>50],
            [['authKey'], 'sting', 'max'=>100],
            [['accessToken'], 'string', 'max'=>100]
        ];
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function attributeLabels() {
        return [
            'id'=>'ID',
            'username'=>'Username',
            'password'=>'Password',
            'authKey'=>'AuthKey',
            'accessToken'=>'AccessToken'
        ];
    }

    /**
     * @inheritdoc
     * @describe 根据id返回对应的用户信息
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
        /*return isset(self::$users[$id]) ? new static(self::$users[$id]) : null;*/
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['access_token'=>$token]);
        /*foreach (self::$users as $user) {
            if ($user['accessToken'] === $token) {
                return new static($user);
            }
        }

        return null;*/
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        $user = User::find()->where(['username'=>$username])->asArray()->one();
        if ($user) {
            return new static($user);
        }
        return null;
        /*foreach (self::$users as $user) {
            if (strcasecmp($user['username'], $username) === 0) {
                return new static($user);
            }
        }

        return null;*/
    }

    /**
     * @inheritdoc
     * @describe 返回用户的id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * @inheritdoc
     * @describe 当两次登陆时，从cookie中获取AuthKey传递给validateAuthKey,验证通过 ，登录成功
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return $this->password === $password;
    }
}
