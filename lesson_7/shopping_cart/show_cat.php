<?php
//列出一个目录中的所有图书
//该脚本显示特定目录中包含的所有图书
include('book_sc_fns.php');
//the shopping cart needs sessions, so start one
session_start();

//利用目录的id 转化为目录的名字name
$catid = $_GET['catid'];
$name = get_category_name($catid);

do_html_header($name);

//从数据库中获取书籍信息，而不是目录信息，与前面的不同
$book_array = get_books($catid);
display_books($book_array);

//如果是管理员登录，显示以下内容

do_html_footer();