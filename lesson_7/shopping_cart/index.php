<?php

//这是整个购物车的主页面，从这里入口，这里显示的是用户想要浏览的一个图书的分类
//用户必须选择一个分类才能够进入所要浏览的图书
  include ('book_sc_fns.php');
  // The shopping cart needs sessions, so start one
  session_start();
  
  //显示页面的标题
  do_html_header("Welcome to Book-O-Rama");

  echo "<p>Please choose a category:</p>";

  // get categories out of database
  //从数据库中得到图书的分类信息，保存在数组变量$cat_array中，这只是一个标识符
  $cat_array = get_categories();

  // display as links to cat pages
  //显示图书的分类的函数
  display_categories($cat_array);

  // if logged in as admin, show add, delete, edit cat links
  //如果是管理登录，显示这个按钮
  if(isset($_SESSION['admin_user'])) {
    display_button("admin.php", "admin-menu", "Admin Menu");
  }
  
  //页脚
  do_html_footer();