<?php
//该脚本控制购物车
include('book_sc_fns.php');
//The shopping cart needs sessions, so strat one
session_start();

@$new = $_GET['new'];

if($new) {
	//new item selected
	//如果用户在之前没有任何物品，即没有一个购物车那么我们需要为其创建一个购物车
	if(!isset($_SESSION['cart'])) {
		$_SESSION['cart'] = array();
		$_SESSION['items'] = 0;
		$_SESSION['total_price'] = '0.00';
	}

	//检查某物品是否已经在购物车中，
	//如果是，我们将该物品的数量增加1
	//否则，添加新的物品到购物车
	if(isset($_SESSION['cart'][$new])) {
		$_SESSION['cart'][$new]++;
	} else {
		$_SESSION['cart'][$new] = 1;
	}

	//计算购物车中所有物品的总价和数量。
	$_SESSION['tatal_price'] = calculate_price($_SESSION['cart']);
	$_SESSION['items'] = calculate_items($_SESSION['cart']);
}


	//点击Save Change按钮的时候，进入show_cart.php脚本，即刷新了页面
if (isset($_POST['save'])) {
	foreach ($_SESSION['cart'] as $isbn => $qty) {
		//如果一个域设置为零，使用unset函数将购物车中该物品完全删除。否则更新购物车，使之与该表单匹配
		if ($_POST[$isbn] == '0') {
			unset($_SESSION['cart'][$isbn]);
		} else {
			$_SESSION['cart'][$isbn] = $_POST[$isbn];
		}
	}

	//保存之后，再一次刷新页面，重新计算
	$_SESSION['total_price'] = calculate_price($_SESSION['cart']);
	$_SESSION['items'] = calculate_items($_SESSION['cart']);
}

do_html_header('Your shopping cart');

//当用户点击View Cart这个按钮后，系统只执行以下代码
//如果购物车有物品，调用display_cart()函数
//该函数格式化并打印购物车的内容
//array_count_values统计数组中所有的值出现的次数, 这是php自带的函数
if (($_SESSION['cart']) && array_count_values($_SESSION['cart'])) {
	display_cart($_SESSION['cart']);
} else {
	echo "<p>There are no items in your cart.</p><hr />";
}

do_html_footer();