<?php
//该脚本显示具体的图书信息
//
//这段脚本与前面的两个脚本非常的相似
//$book = get_book_details($isbn);
//display_book_details($book);
// 在output_fns.php中以HTML形式输出数据
// 
//
//该脚本实现的逻辑：
// 根据提供的 catid，在books表中找到对应的 isbn，然后输出这本书的详细信息
//
//
include('book_sc_fns.php');
//The shopping cart needs session, so start one
session_start();

$isbn = $_GET['isbn'];

//get this book out of database
$book = get_book_details($isbn);
do_html_header($book['title']);
display_book_details($book);

//这里多显示一个continue shopping的按钮
//默认的$target的链接为index.php
//如果传进来 catid，则显示如下链接
$target = "index.php";
if ($book['catid']) {
	$target = "show_cat.php?catid=".$book['isbn'];
}


//if logged in as admin, show edit book links
if (check_admin_user()) {
	display_button("edit_book_form.php?isbn=".$isbn, "edit-item", "Edit Item");
	display_button("admin.php", "admin-menu", "Admin Menu");
	display_button($target, "continue", "Continue");
} else {
	display_button("show_cart.php?new=".$isbn, "add-to-cart", "Add".$book['title']."To My Shopping Cart");
	display_button($target, "continue-shopping", "Continue Shopping");
}
do_html_footer();