<?php
//这是连接数据库的函数

function db_connect() {
	//连接数据库
	$result = new mysqli('localhost', 'book_sc', 'password', 'book_sc');
	if (!$result) {
		return false;
	}
	$result->autocommit(TRUE);
	return $result;
}

function db_result_to_array($result) {
	//该函数将一个Mysql结果标识符转换为结果数组
	$res_array = array();

	//mysql_fetch_assoc()返回每一行并以数组显示它
	for ($count = 0; $row = $result->fetch_assoc(); $count++) {
		$res_array[$count] = $row;
	}

	return $res_array;
}