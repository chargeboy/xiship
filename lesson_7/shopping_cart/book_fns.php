<?php
//用以保存和获取图书数据的函数

function get_categories() {
	//该函数比从数据库中取回一个目录列表
	//query database for a list of categories

	$con = db_connect();

	//从表categories中取出catid和catname
	$query = "select catid, catname from categories";
	$result = @$con->query($query);

	if (!$result) {
		return false;
	}

	//mysql_num_rows()返回所选记录的行数
	$num_cats = @$result->num_rows;
	if ($num_cats == 0) {
		return false;
	}

	//将一个MySQL结果标识符转换为结果数组，返回一组数字索引的行
	$result = db_result_to_array($result);
	return $result;
}

function get_category_name($catid) {
	//query database for the name for a category id
	$con = db_connect();
	$query = "select catname from categories where catid = '".$catid."'";
	$result = @$con->query($query);

	if (!$result) {
		return false;
	}
	$num_cats = @$result->num_rows;
	if ($num_cats == 0) {
		return false;
	}
	$row = $result->fetch_object();
	//返回结果对象里的catname字段
	return $row->catname;
}

function get_books($catid) {
	//该函数从数据库里取回一本图书的详细信息
	
	$con = db_connect();
	$query = "select * from books where catid='".$catid."'";
	$result = @$con->query($query);

	if (!$result) {
		return false;
	}

	$num_cats = @$result->num_rows;
	if ($num_cats == 0) {
		return false;
	}

	$result = db_result_to_array($result);
	return $result;
}

function get_book_details($isbn) {
	//query database for all details for a particular book
	if ((!$isbn) || (!$isbn=='')) {
		return false;
	}
	$con = db_connect();
	$query = "select * from books where isbn='".$isbn."'";
	$result = @$con->query($query);
	if (!$result) {
		return false;
	}
	$result = @$result->fetch_assoc();
	return $result;
}