<?php
//这是写一些输出HTML的函数

function do_html_header($title = '') {
	//print an HTML header
	//declare the session variables we want access to inside the function
	if (!$_SESSION['items']) {
		$_SESSION['items'] = '0';
	}
	if (!$_SESSION['total_price']) {
		$_SESSION['total_price'] = '0.00';
	}
?>
	<html>
	<head>
		<title><?php echo $title; ?></title>
		<style>
		h2 { font-family: Arial, Helvetica, sans-serif; font-size: 30px; color: red; margin: 6px }
		body { font-family: Arial, Helvetica, sans-serif; font-size: 25px }
		li, td { font-family: Arial, Helvetica, sans-serif; font-size: 25px }
		hr { color: #FF0000; width=70%; text-align=center }
		a { color: #000000 }
		</style>
	</head>
	<body>
	<table width="100%" border="0" cellspacing="0" bgcolor="#cccccc">
	<tr>

	<!-- 显示boo-o-rama，且可以点击 -->
	<td rowspan="2">
	<a href="index.php"><img src="images/Book-O-Rama.gif" alt="Bookorama" border="0" align="left" valign="bottom" height="55" width="325" /></a>
	</td>

	<!-- 显示 Total Items = 0这个内容，bottom下对齐 -->
	<td align="right" valign="bottom">
	<?php
	if (isset($_SESSION['admin_user'])) {
		echo "&nbsp;";
	} else {
		echo "Total Items = ".$_SESSION['items'];
	}
	?>

	<!-- 点击购物车的时候要显示的页面,跨两行 -->
	<td align="right" rowspan="2" width="135">
	<?php
	if (isset($_SESSION['admin_user'])) {
		display_button('logout.php', 'log-out', 'Log Out');
	} else {
		display_button('show_cart.php', 'view-cart', 'View Your Shopping Cart');
	}
	?>
	</tr>

	<!-- 显示的是Total Price = $0.00 这个内容,top上对齐-->
	<tr>
	<td align="right" valign="top">
	<?php
	if (isset($_SESSION['admin_user'])) {
		echo "&nbsp;";
	} else {
		echo "Total Price = $".number_format($_SESSION['total_price'], 2);
	}
	?>
	</td>
	</tr>
	</table>
	<?php
	if ($title) {
		do_html_heading($title);
	}
}

function do_html_heading($heading) {
	//print heading
?>
	<h2><?php echo $heading; ?></h2>
<?php
}

function do_html_footer() {
	//print an HTML footer
?>
	</body>
	</html>
<?php
}

function do_html_url($url, $name) {
	//使用 do_html_url的时候使用到这个函数
?>
	<a href="<?php echo $url; ?>"><?php echo $name; ?></a><br />

<?php
}

function display_categories($cat_array) {
	//该函数以一列指向目录链接的形式显示一组目录
	if (!is_array($cat_array)) {
		echo "<p>No categories currently available</p>";
		return;
	}
	echo "<ul>";
	foreach ($cat_array as $row) {
		$url = "show_cat.php?catid=".$row['catid'];
		$title = $row['catname'];
		echo "<li>";
		do_html_url($url, $title);
		echo "</li>";
	}
	echo "</ul>";
	echo "<hr />";
}

function display_books($book_array) {
	//显示图书目录的详细信息
	if (!is_array($book_array)) {
		echo "<p>No books currently available in this categories</p>";
	} else {
		//create table
		echo "<table width=\"100%\" border=\"0\">";

		//create a table row for each book
		foreach ($book_array as $row ) {

			//显示图片可点的链接
			$url = "show_book.php?isbn=".$row['isbn'];
			echo "<tr><td>";
			if (@file_exists("images/".$row['isbn'].".jpg")) {
			$title = "<img src=\"images/".$row['isbn'].".jpg\" style=\"border:1px solid black\" />";
			do_html_url($url, $title);
		} else {
			echo "&nbsp;";
		}

		//显示文字可点的链接
		echo "</td><td>";
		$title = $row['title']." by ".$row['author'];
		do_html_url($url, $title);
		echo "</td></tr>";
		}
		echo "</table>";
	}
	echo "<hr />";
}

function display_book_details($book) {
	//display all details about this book
	if (is_array($book)) {
		echo "<table><tr>";
		//display the picture if there is one
		if (@file_exists("images/".$book['isbn'].".jpg")) {
			$size = getimagesize("images/".$book['isbn'].".jpg");
			if (($size[0] > 0) && ($size[1] > 0)) {
				echo "<td><img src=\"images/".$book['isbn'].".jpg\" style=\"border:1px solid black\"/></td>";
			}
		}
		echo "<td><ul>";
		echo "<li><strong>Author:</strong> ";
		echo $book['author'];
		echo "</li><li><strong>ISBN:</strong> ";
		echo $book['isbn'];
		echo "</li><li><strong>Our Price:</strong> ";
		echo number_format($book['price'], 2);
		echo "</li><li><strong>Description:</strong> ";
		echo $book['description'];
		echo "</li></ul></td></tr></table>";
	} else {
		echo "<p>The details of this book cannot be displayed at this time.</p>";
	}
	echo "<hr />";
}

function display_button($target, $image, $alt) {
	echo "<div align=\"center\"><a href=\"".$target."\">
	<img src=\"images/".$image.".gif\" alt=\"".$alt."\" border=\"0\" height=\"50\" width=\"135\" /></a></div>";
}

function display_cart($cart, $change = true, $images = 1) {
	//该函数显示购物车的页面内容
	//optionally allow changes(true or false)
	//optionally include images(1 - yes, 0 - no)
	echo "<table border=\"0\" width=\"100%\" cellspacing=\"0\">
		<form action=\"show_cart.php\" method=\"post\">
		 		<tr><th colspan=\"".(1+
		 			$images)."\" bgcolor=\"#cccccc\">Item</th>
		 		<th bgcolor=\"#cccccc\">Price</th>
		 		<th bgcolor=\"#cccccc\">Quantity</th>
		 		<th bgcolor=\"#cccccc\">Total</th>
		 		</tr>";

	//display each item as a table row
	foreach ($cart as $isbn => $qty) {
		$book = get_book_details($isbn);
		echo "<tr>";
		if ($images == true) {
			echo "<td align=\"left\">";
			if (file_exists("images/".$isbn.".jpg")) {
				$size = getimagesize("images/".$isbn.".jpg");
				if (($size[0] > 0) && ($size[1] >0)) {
					echo "<img src=\"images/".$isbn.".jpg\" style=\"border: 1px solid black\" width=\"".($size[0]/3)."\" height=\"".($size[1]/3)."\"/>";
				}
			} else {
				echo "&nbsp";
			}
			echo "</td>";
		}
		echo "<td align=\"left\">
		      <a href=\"show_book.php?isbn=".$isbn."\">".$book['title']."</a> by ".$book['author']."</td>
		      <td align=\"center\">\$".number_format($book['price'], 2)."</td>
		      <td align=\"center\">";

		//if we allow changes, quantities are in text toxes
		if ($change == true) {
			echo "<input type=\"text\" name=\"".$isbn."\" value=\"".$qty."\" size=\"3\">";
		} else {
			echo $qty;
		}
		echo "</td><td align=\"center\">\$".number_format($book['price']*$qty, 2)."</td></tr>\n";
	}

	//display total row
	echo "<tr>
	      <th colspan=\"".(2+$images)."\" bgcolor=\"#cccccc\">".$_SESSION['items']."</th>
	      <th align=\"center\" bgcolor=\"#cccccc\">".$_SESSION['items']."</th>
	      <th align=\"center\" bgcolor=\"#cccccc\">\$".number_format($_SESSION['total_price'], 2)."</th></tr>";

	//display save change button
	if ($change == true) {
		echo "<tr>
		      <td colspan=\"".(2+$images)."\">&nbsp;</td>
		      <td align=\"center\">
		      <input type=\"hidden\" name=\"save\" value=\"true\" />
		      <input type=\"image\" src=\"images/save-changes.gif\" border=\"0\" alt=\"Save Changes\" />
		      </td>
		      <td>&nbsp;</td>
		      </tr>";
	}
	echo "</form></table>";
}