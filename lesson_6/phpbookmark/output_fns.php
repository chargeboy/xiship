<?php
	//这个函数是用户输出页面中涉及到的输出函数
	
function do_html_header($title) {
		//print an Html header
?>
	<html>
	<head>
		<title><?php echo "$title";?></title>
		<style>
			body {
				font-family: Arial, Helvetica, sans-serif; font-size: 13px
			}
			li, td {
				font-family: Arial, Helvetica, sans-serif; font-size: 13px
			}
			hr {
				color: #3333cc; width=300; text-align=left;
			}
			a {
				color: #000000
			}
		</style>
	</head>
	<body>
	<img src="bookmark.gif" alt="phpbookmark logo" border="0" align="left" valign="botton" height="55" width="57" />
	<h1>PHPbookmark</h1>
	<hr />
<?php
	if ($title) {
		do_html_heading($title);
	}
}

function do_html_heading($heading) {
	//print heading
?>
	<h2><?php echo "$heading";?></h2>
<?php
}

function do_html_url($url, $name) {
	//output url as link and br
?>
	<br /><a href="<?php echo $url;?>"><?php echo $name;?></a><br />
<?php
}

function do_html_footer() {
	//print an HTML footer
?>
	<!--<table>
	<tr>
		<td><p>&Copy; 版权所有</p>
		<p><a href="">获取更多信息</a></p></td>
	</tr>
	</table>
	</body>
	</html>-->
<?php
}

function display_site_info() {
	//display some marketing info文本
?>
	<ul>
	<li>Store your bookmarks online with us!</li>
	<li>See what other users use!</li>
	<li>Share your favorite links with others!</li>
	</ul>
<?php
}

function display_login_form() {
	//display the all login form
?>
	<p><a href="register_form.php">Not a member?</a></p>
	<form method="post" action="member.php">
	<table bgcolor="#cccccc">
	<tr>
		<td colspan="2">Members log in here:</td>
		<tr>
		<td>Username:</td>
		<td><input type="text" name="username" /></td></tr>
		<tr>
		<td>Password:</td>
		<td><input type="password" name="passwd" /></td></tr>
		<tr>
		<td colspan="2" align="center" value="Log in" /></td></tr>
		<tr>
		<td colspan="2" align="center"><input type="submit" name="submit" value="LogIn" /></td></tr>
		<tr>
		<td colspan="2"><a href="forgot_form.php">Forgot your password?</a></td></tr>
		</tr>
	</table></form>
<?php
}

function display_register_form() {
	//display the register form
?>
	<form action="register_new.php" method="post">
	<table bgcolor="#cccccc">
	<tr>
		<td>Email address:</td>
		<td><input type="text" name="email" size="30" maxlength="100" /></td></tr>
	<tr>
		<td>Preferred username<br />(max 16chars):</td>
		<td valign="top"><input type="text" name="username" size="16" maxlength="16" /></td></tr>
	<tr>
		<td>Password<br />(between 6 and 16chars):</td>
		<td valign="top"><input type="password" name="passwd" size="16" maxlength="16" /></td></tr>
	<tr>
		<td>Confirm password:</td>
		<td><input type="password" name="passwd2" size="16" maxlength="16" /></td></tr>
	<tr>
		<td colspan="2" align="center"><input type="submit" value="Register" /></td></tr>
	</table></form>
<?php
}

function display_user_urls($url_array) {
	//display the table of URLs if get url
	//
	//set global variable, so we can test later if this is on the page
	global $bm_table;
	$bm_table = true;
?>
	<br />
	<form name="bm_table" action="delete_bms.php" method="post">
	<table width="300" cellpadding="2" cellspacing="0">
	<?php
	$color = "#cccccc";
	echo "<tr bgcolor=\"".$color."\"><td><strong>Bookmark</strong></td>";
	echo "<td><strong>Delete?</strong></td></tr>";

	if ((is_array($url_array)) && (count($url_array) > 0)) {
		foreach ($url_array as $url) {
			if ($color == "#cccccc") {
				$color = "#ffffff";
			} else {
				$color = "#cccccc";
			}
			//remember to call htmlspecialchars() when we are displaying user data
			echo "<tr bgcolor=\"".$color."\"><td><a href=\"".$url."\">".htmlspecialchars($url)."</a></td>
			    <td><input type=\"checkbox\" name=\"del_me[]\" value=\"".$url."\" /></td></tr>";
		}
	} else {
			echo "<tr><td>No bookmarks on record.</td></tr>";
	}
?>
	</table></form>
<?php
}

function display_user_menu() {
	//display the menu options on this page
?>
	<hr />
	<a href="member.php">Home</a> &nbsp;|&nbsp;
	<a href="add_bm_form.php">Add BM</a> &nbsp;|&nbsp;
<?php
	//only offet the delete option if baookmark table is on this page
	global $bm_table;
	if ($bm_table == true) {
		echo "<a href=\"#\" onClick=\"bm_table.submit();\">Delete BM</a>&nbsp;|&nbsp;";
	} else {
		echo "<span style=\"color: #cccccc\">Delete BM</span> &nbsp;|&nbsp;";
	}
?>
	<a href="change_passwd_form.php">Change password</a>
	<br />
	<a href="recommend.php">Recommend URLs to me</a> &nbsp;|&nbsp;
	<a href="logout.php">Logout</a>
	<hr />
<?php
}

function display_add_bm_form() {
	//display the form for people to ener a new bookmark in
?>
	<form name="bm_table" action="add_bms.php" method="post">
	<table width="120" cellpadding="2" cellspacing="0" bgcolor="#cccccc">
	<tr><td>New BM:</td>
	<td><input type="text" name="new_url" value="http://" size="30" maxlength="255" /></td></tr>
	<tr><td colspan="2" align="center"><input type="submit" value="Add bookmark" /></td></tr>
	</table></form>
<?php
}