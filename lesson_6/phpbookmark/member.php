<?php
	//this is the page when user have login successfully
	require_once('bookmark_fns.php');

	session_start();

	//create short variable names
	@$username = $_POST['username'];
	@$passwd = $_POST['passwd'];

	if ($username && $passwd) {
		//they have just tried logging in
		try {
			login($username, $passwd);
			//if they are in the databse,register the user id
			$_SESSION['valid_user'] = $username;
		} catch(Exception $e) {
			//unsuccessful login
			do_html_header('Problem:');
			echo "Youa could not be logged in. You must be logged in to view this page.";
			do_html_url('login.php', 'To Login');
			do_html_footer();
			exit();
		}
	}

	do_html_header('Home');
	check_valid_user();

	//get the bookmarks this user has saved
	if ($url_array = get_user_urls($_SESSION['valid_user'])) {
		//if get urls, display it
		display_user_urls($url_array);
	}

	//give menu of options
	display_user_menu();

	do_html_footer();