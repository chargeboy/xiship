<?php
	//processor the file register.php
	//怎么处理 register 传送过来的数据呢？
	require_once('bookmark_fns.php');
	
	//create short variable names
	$email = $_POST['email'];
	$username = $_POST['username'];
	$passwd = $_POST['passwd'];
	$passwd2 = $_POST['passwd2'];

	//start session which may be needed later
	//
	session_start();

	try {
		//check forms filled in
		if (!filled_out($_POST)) {
			throw new Exception("Your have not filled the form out correctly - please go back and try again.");
		}

		//check email address not valid
		if (!valid_email($email)) {
			throw new Exception("That is not a valid email address. Please go back and try again.");			
		}

		//check passwords not the same
		if ($passwd != $passwd2) {
			throw new Exception("The passwords you entered do not metch - please go back and try again.");
		}

		//check passwords length is ok
		if ((strlen($passwd)<6) || (strlen($passwd>16))) {
			throw new Exception("Your passwords nust be between 6 and 16 characters - please go back and try again.");
		}

		//attempt to register
		//this function can also throw an exception
		register($email, $username, $passwd);
		//register session variable
		$_SESSION['valid_user'] = $username;

		//provide link to members page
		do_html_header("Registration successful");
		echo "Your registration was successful. Go to the members page to start setting up your bookmarks!";

		do_html_url('member.php', "Go to mambers page");

		//end page
		do_html_footer();
	}
	catch (Exception $e) {
		do_html_header('Problem:');
		echo $e->getMessage();
		do_html_footer();
		exit();
	}