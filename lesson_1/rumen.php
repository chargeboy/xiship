<?php
	//对入门知识的一次总结，把自己记忆不深的知识点罗列出来。掌握的就不用写了
	
一、PHP入门

	$tireqty = $_POST['tireqty']; //通过 POST 方式将表单中的数据传递给变量 $tireqty
	define('TIR', 100); 					//定义常量。声明的常量全局可用

	常用的超级全局变量:
	$_SERVER; 	//服务器环境变量数组
	$_GET; 			//通过get方法传递给该脚本的变量数组 
	$_POST; 		//通过 post 方法传递给该脚本的变量数组
	$_COOKIE; 	//cookie变量数组
	$_REQUEST; 	//所有用户输入的变量数组，包括 $_GET, $_POST, $_COOKIE所包含的输入内容
	$_SESSION; 	//会话数组变量

	三元运算符：condition ? value if true : value if false;
	
	常用的几个可变函数：
	gettype(var);
	settype(var, type);
	is_array(var);
	is_string(var);
	isset(var);
	empty(var);

	html中几个常用到的标签：
	<select name=""><option value=""></option></select>


二、数据的存储和检索

	存储数据的两种方法：普通文件、数据库

	打开文件->fopen，写文件->fwrite，关闭文件->fclose, 读完文件->feof(), 检查文件->file_exists(filename), 文件大小->filesize(filename),	删除文件->unlink(filename);

	打开文件：fopen( "$_SERVER['DOCUMENT_ROOT']/../..", 'w');
	//如果没有指明文件路径，文件将会在脚本自身所在的目录下创建
	//fopen的第二个参数是文件模式，r->只读， w->只写，a->追加，b->二进制模式
	
	示例：
	$outputstring = $data."\t".$tireqty."tires \t".$oilqty."oil \t"."\n";
	$fp = fopen(filename, mode);
	flock(handle, operation);
	fwrite(handle, string);
	flock(handle, operation);
	fclose(handle);


三、使用数组

	索引数组、关联数组

	初始化索引数组：
	$var = array('', '', '');
	$var = array('' => , '' => , '' => );

	访问数组内容：
	var[0], var[1], var[2], ........
	$var[''], $var[''], $var[''], .......

	循环访问数组：
	for($i=0, $i<=3, $i++){
		echo $var[$i]." ";
	}
	foreach ($variable as $key => $value) {
		echo "根据情况进行组合";
	}

	多维数组：
	$var = array( array('', '', ''),
								array('', '', ''),
								array('', '', '')
		     );
	使用shuffle()函数对数组的各个元素进行随机排序
	使用file()函数将整个文件载入一个数组中
	使用conut()函数来统计数组中的元素个数
	使用explode()函数来分割每行----------explode(',' string string);
	使用intval()函数可以将一个字符串转化成一个整数


四、字符串操作与正则表达式

	字符串的格式化
		trim(str); 函数可以除去字符串开始位置和结束 位置的空格并将结果字符串返回
		ltrim(str); 除去左边的空格， rtrim(str) 除去右边的空格
		strtoupper(string); 将字符串转化为大写
		strtolower(str); 将字符串转化为小写
		/ 转义字符。在前面加上反斜杠
		使用 addslashes(str)函数对字符串进行格式化。也就是在它们的前面加上反斜杠。
		使用 stripcslashes(str)将这些反斜杠移除掉。
		如果魔术方法 get_magic_quotes_gpc(oid)函数是启用的，要使用 stripcslashes(str),否则会有反斜杠

	字符串的连接和分割
		explode(delimiter, string);
		substr(string, start); 允许我们访问一个字符串给定起点和终点的子字符串。如制作验证码时的字符串

	字符串的比较
		“==”比较是否相等
		strlen(string); 函数测试字符串的长度

	字符串函数匹配和替换字符串
		strstr(haystack, needle); 它可以在一个较长的字符串中查找匹配的字符串或字符
		strpos(haystack, needle); 它返回的是目标关键字子字符串在被搜索字符串中的位置
			如：$test = "Hello, World"; echo "strpos($test, "o")"; 在浏览器中输出的是4
		str_replace(search, replace, subject);
			如：$feedback = str_replace($offcolor, '@!%*', $feedback); $offcolor是数组
		substr_replace(string, replacement, start); 用来在给定的位置中查找和替换字符串中特定的子字符串
			如：$test = substr_replace($test, 'X', -1); 用“X”替换$test中的最后一个字符

	正则表达式
		
		开始结束：^[a-zA-Z0-9]$
		转义字符：\
		匹配除换行符(\n)之外的字符:.
		或：|
		子模式的开始和结束：()
		重复一次或多次：+

	用正则表达式对字符串进行查找、替换和分割。
		$pattern是要搜索的模式或字符串类型，$subject是被匹配的字符串，$matches是保存在数组中的数据
		preg_match($pattern, $subject); preg_match_all($pattern, $subject, $matches); 
			preg_match()返回$pattern的匹配次数。它的值将是0次（不匹配）或1次，因为 preg_match() 在第一次匹配后 将会停止搜索。 
			preg_match_all()不同于此，它会一直搜索$subject直到到达结尾。如果发生错误, preg_match()返回 FALSE
			<?php
			 $subject = "abcdef";
			 $pattern = '/^def/';
			 preg_match($pattern, substr($subject, 3), $matches, PREG_OFFSET_CAPTURE);
			 print_r($matches);
			 
		preg_replace($pattern, $replacement, $subject);
			搜索subject中匹配pattern的部分，以replacement进行替换
			<?php
			 $string = 'April 15, 2003';
			 $$pattern = '/(\w+) (\d+), (\d+)/i';
			 $replacement = '${1}1,$3';
			 echo preg_replace($pattern, $replacement, $string);
				 
		preg_split($pattern, $subject);
			<?php
			 //使用逗号或空格(包含" ", \r, \t, \n, \f)分隔短语
			 $keywords = preg_split("/[\s,]+/", "hypertext language, programming");
			 print_r($keywords);
				  
		preg_quote(str); 转义正则表达式字符
?>